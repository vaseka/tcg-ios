var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Fractal;
(function (Fractal) {
    Fractal.MAIN_URL = 'http://api.fractalgames.com';
    Fractal.LOGIN_URL = '/api/user/login/';
    Fractal.SHORT_ACCOUNT_INFO_URL = '/api/user/shortaccountinfo/';
    Fractal.FACEBOOK_URL = '/api/user/facebook/';
    Fractal.GOOGLE_URL = '/api/user/google/';
    Fractal.TWITTER_LOGIN_URL = '/api/user/twitter/';
    Fractal.TWITTER_REGISTER_URL = '/api/user/twitterregister/';
    Fractal.POST_SCORE = "/api/game/postscore/";
    Fractal.POST_HIGH_SCORE = "/api/game/posthighscore/";
    Fractal.GET_HIGH_SCORES = "/api/game/gethighscores/";
    Fractal.GLOBAL_HIGH_SCORES = "/api/game/specificglobalsleaderboard/";
    Fractal.FRIENDS_HIGH_SCORES = "/api/game/specificfriendsleaderboard/";
    Fractal.USER_INFO = '/api/user/userinfo/';
    Fractal.CHANGE_INFO = '/api/user/changeinfo/';
    Fractal.CHECK_USERNAME_URL = '/api/user/checkusername/';
    Fractal.CHECK_EMAIL_URL = '/api/user/checkemail/';
    Fractal.REGISTRATION_URL = '/api/user/registration/';
    Fractal.FORGOTTEN_PASSWORD_URL = '/api/user/forgotten-password/';
    Fractal.CHECK_GAME_BOUGHT_URL = '/api/user/checkgamebought/';
    Fractal.BUY_GAME_URL = '/api/user/buypro/';
    Fractal.waitTime = 500;
    Fractal.cubeTransitionTime = 1600;
    Fractal.fractalEndTransitionTime = 600;
    Fractal.logoTransitionTime = 1300;
    Fractal.PRODUCTS = [
        {
            id: "com.fractalgames.thecolourgame.noads.extramodes",
            alias: "noadsextramodes",
            type: "non consumable"
        }
    ];
})(Fractal || (Fractal = {}));
var TheColourGame;
(function (TheColourGame) {
    (function (ORIENTATIONS) {
        ORIENTATIONS[ORIENTATIONS["PORTRAIT"] = 0] = "PORTRAIT";
        ORIENTATIONS[ORIENTATIONS["LANDSCAPE"] = 1] = "LANDSCAPE";
    })(TheColourGame.ORIENTATIONS || (TheColourGame.ORIENTATIONS = {}));
    var ORIENTATIONS = TheColourGame.ORIENTATIONS;
    TheColourGame.BOARD_STARTING_TILES = 2;
    TheColourGame.BOARD_PORTRAIT_HEIGHT_MULTIPLIER = 0.6;
    TheColourGame.BOARD_PORTRAIT_LINE_WIDTH_MULTIPLIER = 20 / 1080;
    TheColourGame.MIN_RANDOM_COLOR = 20;
    TheColourGame.MAX_RANDOM_COLOR = 230;
    TheColourGame.HIGHEST_LEVEL_INDEX = 6;
    TheColourGame.LEVELS_TO_REPEAT = [3, 4, 5];
    TheColourGame.CHOSEN_TILE_MIN_ALPHA = 60;
    TheColourGame.CHOSEN_TILE_MAX_ALPHA = 82;
    TheColourGame.UI_PORTRAIT_NUMBERS_FONT_SIZE_MULTIPLIER = 0.06;
    TheColourGame.UI_LANDSCAPE_NUMBERS_FONT_SIZE_MULTIPLIER = 0.09;
    TheColourGame.UI_ELEMENTS_VERTICAL_OFFSET_MULTIPLIER = 1.5;
    TheColourGame.TABLET_RESOLUTION_RATIO_THRESHOLD = 0.65;
    TheColourGame.UI_PHONE_NUMBERS_ADDITIONAL_OFFSET_MULTIPLIER = 1.1;
    TheColourGame.UI_QUIT_REFRESH_Y_OFFSET_MULTIPLIER = 0.125;
    TheColourGame.UI_LANDSCAPE_RIGHT_ELEMENTS_VOFFSET_MULTIPLIER = 0.15;
    TheColourGame.UI_LANDSCAPE_TABLET_LOGO_REDUCTION_MULTIPLIER = 0.7;
    TheColourGame.UI_LANDSCAPE_TABLET_MODE_TEXT_REDUCTION_MULTIPLIER = 0.5;
    TheColourGame.UI_SURVIVAL_TABLET_TIME_OFFSET_MULTIPLIER = 0.02;
    TheColourGame.UI_DECIMAL_TIME_FONT_SIZE_REDUCTION_MULTIPLIER = 0.5;
    TheColourGame.ORIENTATION_CHANGE_MINIMAL_INTERVAL = 100;
    TheColourGame.COUNTDOWN_STARTING_TIMER = 60;
    TheColourGame.SHOOTDOWN_STARTING_TIMER = 5;
    TheColourGame.SHOOTDOWN_DEFAULT_TARGET = 40;
    TheColourGame.SHOOTDOWN_STORAGE_TARGET_KEY = "shootdownTarget";
    TheColourGame.TARGET_DEFAULT_TARGET = 40;
    TheColourGame.TARGET_STORAGE_TARGET_KEY = "targetTarget";
    TheColourGame.TARGET_TIME_RATIO_TO_TARGET = 1.35;
    TheColourGame.SURVIVAL_DEFAULT_TIMER = 10;
    TheColourGame.SURVIVAL_BONUS_PENALTY_TWEENS_DURATION = 250;
    TheColourGame.SURVIVAL_DECIMAL_NUMBER_VOFFSET_MULTIPLIER = 0.4;
    TheColourGame.SURVIVAL_BONUS_PENALTY_FONT_SIZE_MULTIPLIER = 0.5;
    TheColourGame.SURVIVAL_PORTRAIT_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER = 0.04;
    TheColourGame.SURVIVAL_LANDSCAPE_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER = 0.06;
    TheColourGame.SURVIVAL_LANDSCAPE_TABLET_FLOATING_NUMBERS_TWEEN_DISTANCE_REDUCTION_MULTIPLIER = 0.02;
    TheColourGame.PACKAGE_NAME = "com.fractalgames.thecolourgame";
    TheColourGame.GAME_IDENTIFIER = "the-colour-game";
})(TheColourGame || (TheColourGame = {}));
var TheColourGame;
(function (TheColourGame) {
    var Board = (function () {
        function Board(game) {
            this.game = game;
            this.tilesPerSide = TheColourGame.BOARD_STARTING_TILES;
            this.calculateBoardElements();
            this.alphasCollection = [];
            for (var i = TheColourGame.CHOSEN_TILE_MIN_ALPHA; i <= TheColourGame.CHOSEN_TILE_MAX_ALPHA; i++) {
                this.alphasCollection.push(i / 100);
            }
            this.currentAlphaIndex = 0;
            this.drawBackGroundSprite();
            this.calculateTileSideSize();
            this.drawTiles(false, false);
        }
        Board.prototype.resize = function (newSize) {
            this.tilesPerSide = newSize;
            this.calculateTileSideSize();
        };
        ;
        Board.prototype.redrawWithNewOrientation = function () {
            this.drawBackGroundSprite();
            this.calculateBoardElements();
            this.drawTiles(false, true);
        };
        ;
        Board.prototype.drawTiles = function (shouldUpdateChosenTileAlpha, drawingOnOrientationChange) {
            if (this.boardImage) {
                this.boardImage.destroy();
                this.drawBMD.clear();
                this.drawBMD.destroy();
            }
            if (shouldUpdateChosenTileAlpha && this.currentAlphaIndex < this.alphasCollection.length - 1) {
                this.currentAlphaIndex++;
            }
            if (!drawingOnOrientationChange) {
                this.chooseRandomColor(TheColourGame.MIN_RANDOM_COLOR, TheColourGame.MAX_RANDOM_COLOR);
                this.chosenTileIndex = Math.round(Math.random() * (this.tilesPerSide * this.tilesPerSide - 1));
            }
            this.drawBMD = this.game.add.bitmapData(10, 10, "tile", true);
            this.drawBMD.ctx.rect(0, 0, this.drawBMD.width, this.drawBMD.height);
            this.drawBMD.ctx.fillStyle = this.currentColor;
            this.drawBMD.ctx.fill();
            var tileSprite = this.game.make.sprite(0, 0, this.game.cache.getBitmapData("tile"));
            tileSprite.width = this.tileSideLength;
            tileSprite.height = this.tileSideLength;
            tileSprite.anchor.set(0.5);
            var chosenTileSprite = this.game.make.sprite(0, 0, this.game.cache.getBitmapData("tile"));
            chosenTileSprite.width = this.tileSideLength;
            chosenTileSprite.height = this.tileSideLength;
            chosenTileSprite.anchor.set(0.5);
            chosenTileSprite.alpha = this.alphasCollection[this.currentAlphaIndex];
            this.drawBMD = this.game.add.bitmapData(this.sideLength, this.sideLength, "allTiles", true);
            var counter = -1;
            for (var i = 0; i < this.tilesPerSide; i++) {
                for (var j = 0; j < this.tilesPerSide; j++) {
                    counter++;
                    if (counter !== this.chosenTileIndex) {
                        this.drawBMD.draw(tileSprite, this.lineWidth + this.tileSideLength * 0.5 + j * (this.tileSideLength + this.lineWidth), this.lineWidth + this.tileSideLength * 0.5 + i * (this.tileSideLength + this.lineWidth));
                    }
                    else {
                        var xPosition = this.lineWidth + this.tileSideLength * 0.5 + j * (this.tileSideLength + this.lineWidth);
                        var yPosition = this.lineWidth + this.tileSideLength * 0.5 + i * (this.tileSideLength + this.lineWidth);
                        this.drawBMD.draw(chosenTileSprite, xPosition, yPosition);
                        this.chosenTilePosition = new Phaser.Point(xPosition, yPosition);
                    }
                }
            }
            if (window.innerHeight > window.innerWidth) {
                this.boardImage = this.game.add.sprite(this.game.width * 0.5, this.game.height * TheColourGame.BOARD_PORTRAIT_HEIGHT_MULTIPLIER, this.game.cache.getBitmapData("allTiles"));
                if (this.game.width / this.game.height > TheColourGame.TABLET_RESOLUTION_RATIO_THRESHOLD) {
                    this.boardImage.y = this.game.height - this.boardImage.height * 0.5;
                    this.backGroundSprite.y = this.game.height - this.boardImage.height * 0.5;
                }
                this.chosenTilePosition.y += this.boardImage.y - this.boardImage.height * 0.5;
            }
            else {
                this.boardImage = this.game.add.sprite(this.game.width * 0.5, this.game.height * 0.5, this.game.cache.getBitmapData("allTiles"));
                this.chosenTilePosition.x += this.game.width * 0.5 - this.boardImage.width * 0.5;
            }
            this.boardImage.anchor.set(0.5);
            tileSprite.destroy();
            tileSprite = null;
            chosenTileSprite.destroy();
            chosenTileSprite = null;
        };
        Board.prototype.destroy = function () {
            this.sideLength = null;
            this.lineWidth = null;
            this.backGroundSprite.destroy();
            this.backGroundSprite = null;
            this.drawBMD.destroy();
            this.drawBMD = null;
            this.currentColor = null;
            this.alphasCollection = null;
            this.currentAlphaIndex = null;
            this.chosenTileIndex = null;
            this.boardImage.destroy();
            this.boardImage = null;
            this.tilesPerSide = null;
            this.chosenTilePosition = null;
            this.tileSideLength = null;
        };
        ;
        Board.prototype.calculateBoardElements = function () {
            if (window.innerHeight > window.innerWidth) {
                this.sideLength = this.game.width;
                this.lineWidth = this.game.width * TheColourGame.BOARD_PORTRAIT_LINE_WIDTH_MULTIPLIER;
            }
            else {
                this.sideLength = this.game.height;
                this.lineWidth = this.game.height * TheColourGame.BOARD_PORTRAIT_LINE_WIDTH_MULTIPLIER;
            }
        };
        ;
        Board.prototype.chooseRandomColor = function (min, max) {
            var red = min + Math.round(Math.random() * (max - min));
            var green = min + Math.round(Math.random() * (max - min));
            var blue = min + Math.round(Math.random() * (max - min));
            var color = Phaser.Color.createColor(red, green, blue, 1);
            this.currentColor = Phaser.Color.RGBtoString(parseInt(color.r), parseInt(color.g), parseInt(color.b), parseInt(color.a));
        };
        ;
        Board.prototype.drawBackGroundSprite = function () {
            if (this.backGroundSprite) {
                this.backGroundSprite.destroy();
            }
            else {
                this.drawBMD = this.game.add.bitmapData(5, 5, "backgroundSprite", true);
                this.drawBMD.ctx.rect(0, 0, this.drawBMD.width, this.drawBMD.height);
                this.drawBMD.ctx.fillStyle = "#ffffff";
                this.drawBMD.ctx.fill();
            }
            if (window.innerHeight > window.innerWidth) {
                this.backGroundSprite = this.game.add.sprite(this.game.width * 0.5, this.game.height * TheColourGame.BOARD_PORTRAIT_HEIGHT_MULTIPLIER, this.game.cache.getBitmapData("backgroundSprite"));
            }
            else {
                this.backGroundSprite = this.game.add.sprite(this.game.width * 0.5, this.game.height * 0.5, this.game.cache.getBitmapData("backgroundSprite"));
            }
            this.backGroundSprite.width = this.sideLength;
            this.backGroundSprite.height = this.sideLength;
            this.backGroundSprite.anchor.set(0.5);
        };
        ;
        Board.prototype.calculateTileSideSize = function () {
            this.tileSideLength = (this.sideLength - (this.tilesPerSide + 1) * this.lineWidth) / this.tilesPerSide;
        };
        ;
        return Board;
    }());
    TheColourGame.Board = Board;
})(TheColourGame || (TheColourGame = {}));
var TheColourGame;
(function (TheColourGame) {
    var UI = (function () {
        function UI(game, time, score, boardImageWidth, gameMode, playState) {
            this.game = game;
            if (window.innerHeight > window.innerWidth) {
                this.drawPortraitUI(time, score, gameMode, playState);
            }
            else {
                this.drawLandscapeUI(time, score, boardImageWidth, gameMode, playState);
            }
        }
        UI.prototype.destroy = function () {
            this.refreshButton.destroy();
            this.refreshButton = null;
            this.modeText.destroy();
            this.modeText = null;
            this.exitButton.destroy();
            this.exitButton = null;
            if (this.shadowSprite) {
                this.shadowSprite.destroy();
                this.shadowSprite = null;
            }
            this.pointsTrackerSprite.destroy();
            this.pointsTrackerSprite = null;
            this.pointsText.destroy();
            this.pointsText = null;
            this.timeSprite.destroy();
            this.timeSprite = null;
            this.timeText.destroy();
            this.timeText = null;
            if (this.gameLogo) {
                this.gameLogo.destroy();
                this.gameLogo = null;
            }
            this.numbersStyle = null;
            if (this.timeTextDecimal) {
                this.timeTextDecimal.destroy();
                this.timeTextDecimal = null;
            }
            if (this.survivalModeBonusTime) {
                this.survivalModeBonusTime.destroy();
                this.survivalModeBonusTime = null;
                this.survivalModePenaltyPenaltyTime.destroy();
                this.survivalModePenaltyPenaltyTime = null;
                this.bonusTween = null;
                this.penaltyTween = null;
            }
        };
        ;
        UI.prototype.updateTimerText = function (time, mode) {
            if (mode !== "survival") {
                this.timeText.setText(time.toString());
            }
            else {
                this.timeText.setText(Math.floor(time).toFixed(0).toString());
                this.timeTextDecimal.setText((time - Math.floor(time)).toFixed(1).toString().substr(1, 2));
                if (this.timeTextDecimal.x - this.timeText.x !== this.timeText.width * 0.5) {
                    this.timeTextDecimal.x = this.timeText.x + this.timeText.width * 0.5;
                }
            }
        };
        ;
        UI.prototype.updatePointsTracker = function (scoreValue) {
            this.pointsText.setText(scoreValue.toString());
        };
        ;
        UI.prototype.redrawWithNewOrientation = function (time, score, boardImageWidth, gameMode, playState) {
            this.destroy();
            if (window.innerHeight > window.innerWidth) {
                this.drawPortraitUI(time, score, gameMode, playState);
            }
            else {
                this.drawLandscapeUI(time, score, boardImageWidth, gameMode, playState);
            }
        };
        ;
        UI.prototype.playAddBonusAnimation = function (timeToAdd) {
            if (this.bonusTween && this.bonusTween.isRunning) {
                this.bonusTween.stop();
                this.survivalModeBonusTime.alpha = 0;
                this.survivalModeBonusTime.x = this.timeSprite.x;
                this.survivalModeBonusTime.y = this.timeSprite.y;
            }
            var targetPositionX = 0;
            var targetPositionY = 0;
            if (window.innerHeight > window.innerWidth) {
                targetPositionX = this.survivalModeBonusTime.x + this.game.height * TheColourGame.SURVIVAL_PORTRAIT_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
                targetPositionY = this.survivalModeBonusTime.y - this.game.height * TheColourGame.SURVIVAL_PORTRAIT_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
            }
            else {
                targetPositionX = this.survivalModeBonusTime.x + this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
                targetPositionY = this.survivalModeBonusTime.y - this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
                if (this.checkIfDisplayedOnTablet) {
                    targetPositionX -= this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_TABLET_FLOATING_NUMBERS_TWEEN_DISTANCE_REDUCTION_MULTIPLIER;
                    targetPositionY -= this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_TABLET_FLOATING_NUMBERS_TWEEN_DISTANCE_REDUCTION_MULTIPLIER;
                }
            }
            this.survivalModeBonusTime.setText("+" + timeToAdd.toString());
            this.bonusTween = this.game.add.tween(this.survivalModeBonusTime).to({
                alpha: 1,
                x: targetPositionX,
                y: targetPositionY,
            }, TheColourGame.SURVIVAL_BONUS_PENALTY_TWEENS_DURATION, Phaser.Easing.Default, true, 0, 0, true);
        };
        ;
        UI.prototype.playPenaltyAnimation = function () {
            if (this.penaltyTween && this.penaltyTween.isRunning) {
                this.penaltyTween.stop();
                this.survivalModePenaltyPenaltyTime.alpha = 0;
                this.survivalModePenaltyPenaltyTime.x = this.timeSprite.x;
                this.survivalModePenaltyPenaltyTime.y = this.timeSprite.y;
            }
            var targetPositionX = 0;
            var targetPositionY = 0;
            if (window.innerHeight > window.innerWidth) {
                targetPositionX = this.survivalModePenaltyPenaltyTime.x + this.game.height * TheColourGame.SURVIVAL_PORTRAIT_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
                targetPositionY = this.survivalModePenaltyPenaltyTime.y + this.game.height * TheColourGame.SURVIVAL_PORTRAIT_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
            }
            else {
                targetPositionX = this.survivalModePenaltyPenaltyTime.x + this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
                targetPositionY = this.survivalModePenaltyPenaltyTime.y + this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
                if (this.checkIfDisplayedOnTablet) {
                    targetPositionX -= this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_TABLET_FLOATING_NUMBERS_TWEEN_DISTANCE_REDUCTION_MULTIPLIER * 0.5;
                    targetPositionY -= this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_TABLET_FLOATING_NUMBERS_TWEEN_DISTANCE_REDUCTION_MULTIPLIER * 0.5;
                }
            }
            this.penaltyTween = this.game.add.tween(this.survivalModePenaltyPenaltyTime).to({
                alpha: 1,
                x: targetPositionX,
                y: targetPositionY
            }, TheColourGame.SURVIVAL_BONUS_PENALTY_TWEENS_DURATION, Phaser.Easing.Default, true, 0, 0, true);
        };
        ;
        UI.prototype.drawPortraitUI = function (time, score, gameMode, playState) {
            this.numbersStyle = {
                font: 'Oswald-Regular',
                fontSize: this.game.height * TheColourGame.UI_PORTRAIT_NUMBERS_FONT_SIZE_MULTIPLIER,
                fill: "#ffffff"
            };
            var shadowY = this.game.height * (200 / 1920);
            var shadowHeight = this.game.height * (60 / 1920);
            this.shadowSprite = this.game.add.sprite(0, shadowY, "shadowSprite");
            this.shadowSprite.width = this.game.width;
            this.shadowSprite.height = shadowHeight;
            var upperElementsY = this.shadowSprite.y * 0.5;
            var upperElementsOffsetFromEnd = this.game.width * (90 / 1080);
            var upperElementsSideLength = this.game.height * (120 / 1920);
            this.refreshButton = this.game.add.button(upperElementsOffsetFromEnd, upperElementsY, "refreshSprite", function () {
                playState.restartGame();
            });
            this.refreshButton.anchor.set(0.5);
            this.refreshButton.width = upperElementsSideLength;
            this.refreshButton.scale.y = this.refreshButton.scale.x;
            this.refreshButton.y += this.refreshButton.height * TheColourGame.UI_QUIT_REFRESH_Y_OFFSET_MULTIPLIER;
            this.modeText = this.game.add.text(this.game.width * 0.5, upperElementsY, gameMode.toUpperCase(), {
                font: "Oswald-Light",
                fontSize: this.game.height * TheColourGame.UI_PORTRAIT_NUMBERS_FONT_SIZE_MULTIPLIER,
                fill: "#ffffff"
            });
            this.modeText.anchor.set(0.5, 0.5);
            this.exitButton = this.game.add.button(this.game.width - upperElementsOffsetFromEnd, upperElementsY, "xSprite", function () {
                playState.quitGame();
            });
            this.exitButton.anchor.set(0.5);
            this.exitButton.width = upperElementsSideLength;
            this.exitButton.scale.y = this.refreshButton.scale.x;
            this.exitButton.y += this.exitButton.height * TheColourGame.UI_QUIT_REFRESH_Y_OFFSET_MULTIPLIER;
            var lowerElementsY = this.game.height * (290 / 1920);
            var lowerElementsOffsetFromMiddle = this.game.width * (220 / 1080);
            var lowerElementsSideLength = this.game.height * (75 / 1920);
            this.timeSprite = this.game.add.sprite(this.game.width * 0.5 + lowerElementsOffsetFromMiddle, lowerElementsY, "timeSprite");
            this.timeSprite.width = lowerElementsSideLength;
            this.timeSprite.height = lowerElementsSideLength;
            this.timeSprite.anchor.set(0.5);
            var numbersYPosition = this.timeSprite.y + this.timeSprite.height * TheColourGame.UI_ELEMENTS_VERTICAL_OFFSET_MULTIPLIER;
            if (!this.checkIfDisplayedOnTablet()) {
                numbersYPosition *= TheColourGame.UI_PHONE_NUMBERS_ADDITIONAL_OFFSET_MULTIPLIER;
            }
            this.timeText = this.game.add.text(this.timeSprite.x, numbersYPosition, time.toFixed(0).toString(), this.numbersStyle);
            this.timeText.anchor.set(0.5);
            if (gameMode === "survival") {
                this.addSurvivalSpecificObjects(time);
            }
            this.pointsTrackerSprite = this.game.add.sprite(this.game.width * 0.5 - lowerElementsOffsetFromMiddle, lowerElementsY, "tickSprite");
            this.pointsTrackerSprite.width = lowerElementsSideLength;
            this.pointsTrackerSprite.height = lowerElementsSideLength;
            this.pointsTrackerSprite.anchor.set(0.5);
            this.pointsText = this.game.add.text(this.pointsTrackerSprite.x, numbersYPosition, score.toString(), this.numbersStyle);
            this.pointsText.anchor.set(0.5);
        };
        ;
        UI.prototype.drawLandscapeUI = function (time, score, boardImageWidth, gameMode, playState) {
            this.numbersStyle = {
                font: 'Oswald-Regular',
                fontSize: this.game.width * TheColourGame.UI_LANDSCAPE_NUMBERS_FONT_SIZE_MULTIPLIER,
                fill: "#ffffff"
            };
            var logoX = (this.game.width - boardImageWidth) * 0.25;
            var logoY = this.game.height * (75 / 375);
            this.gameLogo = this.game.add.sprite(logoX, logoY, "gameLogoSprite");
            this.gameLogo.anchor.set(0.5);
            this.gameLogo.width = this.game.width * (110 / 670);
            this.gameLogo.scale.y = this.gameLogo.scale.x;
            this.modeText = this.game.add.text(this.gameLogo.x, this.gameLogo.y + this.gameLogo.height * TheColourGame.UI_ELEMENTS_VERTICAL_OFFSET_MULTIPLIER, gameMode.toUpperCase(), {
                font: "Oswald-Light",
                fontSize: this.game.height * TheColourGame.UI_PORTRAIT_NUMBERS_FONT_SIZE_MULTIPLIER,
                fill: "#ffffff"
            });
            this.modeText.anchor.set(0.5);
            if (this.checkIfDisplayedOnTablet()) {
                this.gameLogo.width *= TheColourGame.UI_LANDSCAPE_TABLET_LOGO_REDUCTION_MULTIPLIER;
                this.gameLogo.scale.y = this.gameLogo.scale.x;
                this.numbersStyle.fontSize *= 0.8;
                this.modeText.fontSize *= TheColourGame.UI_LANDSCAPE_TABLET_MODE_TEXT_REDUCTION_MULTIPLIER;
            }
            var leftSideElementsWidth = this.game.width * (50 / 735);
            var refreshButtonY = this.game.height * (275 / 415);
            this.refreshButton = this.game.add.button(this.gameLogo.x, refreshButtonY, "refreshSprite", function () {
                playState.restartGame();
            });
            this.refreshButton.anchor.set(0.5);
            this.refreshButton.width = leftSideElementsWidth;
            this.refreshButton.scale.y = this.refreshButton.scale.x;
            this.exitButton = this.game.add.button(this.gameLogo.x, this.refreshButton.y + this.refreshButton.height * 1.25, "xSprite", function () {
                playState.quitGame();
            });
            this.exitButton.anchor.set(0.5);
            this.exitButton.width = leftSideElementsWidth;
            this.exitButton.scale.y = this.exitButton.scale.x;
            var rightSideElementsWidth = this.game.width * (42 / 735);
            var pointsTrackerSpriteX = this.game.width - logoX;
            var pointsTrackerSpriteY = this.game.height * TheColourGame.UI_LANDSCAPE_RIGHT_ELEMENTS_VOFFSET_MULTIPLIER;
            this.pointsTrackerSprite = this.game.add.sprite(pointsTrackerSpriteX, pointsTrackerSpriteY, "tickSprite");
            this.pointsTrackerSprite.anchor.set(0.5);
            this.pointsTrackerSprite.width = rightSideElementsWidth;
            this.pointsTrackerSprite.scale.y = this.pointsTrackerSprite.scale.x;
            this.pointsText = this.game.add.text(this.pointsTrackerSprite.x, this.pointsTrackerSprite.y + this.pointsTrackerSprite.height * TheColourGame.UI_ELEMENTS_VERTICAL_OFFSET_MULTIPLIER, score.toString(), this.numbersStyle);
            this.pointsText.anchor.set(0.5);
            var timeSpriteY = this.game.height * (0.5 + TheColourGame.UI_LANDSCAPE_RIGHT_ELEMENTS_VOFFSET_MULTIPLIER);
            this.timeSprite = this.game.add.sprite(this.pointsTrackerSprite.x, timeSpriteY, "timeSprite");
            this.timeSprite.anchor.set(0.5);
            this.timeSprite.width = rightSideElementsWidth;
            this.timeSprite.scale.y = this.timeSprite.scale.x;
            this.timeText = this.game.add.text(this.timeSprite.x, this.timeSprite.y + this.timeSprite.height * TheColourGame.UI_ELEMENTS_VERTICAL_OFFSET_MULTIPLIER, time.toFixed(0).toString(), this.numbersStyle);
            this.timeText.anchor.set(0.5);
            if (gameMode === "survival") {
                if (this.checkIfDisplayedOnTablet()) {
                    this.timeText.x -= this.game.width * TheColourGame.UI_SURVIVAL_TABLET_TIME_OFFSET_MULTIPLIER;
                }
                this.addSurvivalSpecificObjects(time);
            }
        };
        ;
        UI.prototype.addSurvivalSpecificObjects = function (time) {
            this.timeTextDecimal = this.game.add.text(this.timeText.x + this.timeText.width * 0.5, this.timeText.y - this.timeText.height * 0.4, (time - Math.floor(time)).toFixed(1).toString().substr(1, 2), this.numbersStyle);
            this.timeTextDecimal.fontSize *= TheColourGame.UI_DECIMAL_TIME_FONT_SIZE_REDUCTION_MULTIPLIER;
            this.survivalModeBonusTime = this.game.add.text(this.timeSprite.x, this.timeSprite.y, "5", {
                font: "Oswald",
                fill: "#00FF00",
                fontSize: this.numbersStyle.fontSize * TheColourGame.SURVIVAL_BONUS_PENALTY_FONT_SIZE_MULTIPLIER
            });
            this.survivalModeBonusTime.alpha = 0;
            this.survivalModeBonusTime.anchor.set(0.5);
            this.survivalModePenaltyPenaltyTime = this.game.add.text(this.timeSprite.x, this.timeSprite.y, "-2", {
                font: "Arial",
                fill: "#FF3333",
                fontSize: this.numbersStyle.fontSize * TheColourGame.SURVIVAL_BONUS_PENALTY_FONT_SIZE_MULTIPLIER
            });
            this.survivalModePenaltyPenaltyTime.alpha = 0;
            this.survivalModePenaltyPenaltyTime.anchor.set(0.5);
        };
        ;
        UI.prototype.checkIfDisplayedOnTablet = function () {
            if (window.innerHeight > window.innerWidth) {
                return (this.game.width / this.game.height) > TheColourGame.TABLET_RESOLUTION_RATIO_THRESHOLD;
            }
            else {
                return (this.game.height / this.game.width) > TheColourGame.TABLET_RESOLUTION_RATIO_THRESHOLD;
            }
        };
        ;
        return UI;
    }());
    TheColourGame.UI = UI;
})(TheColourGame || (TheColourGame = {}));
var Fractal;
(function (Fractal) {
    var Helpers = (function () {
        function Helpers() {
        }
        Helpers.getQueryParams = function (qs) {
            qs = qs.split('+').join(' ');
            var params = {};
            var tokens;
            var re = /[?&]?([^=]+)=([^&]*)/g;
            while (tokens = re.exec(qs)) {
                params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
            }
            return params;
        };
        Helpers.rotateUntilCallback = function (callback) {
            Helpers.$helperContainer.addClass('show-loader');
            callback();
        };
        Helpers.removeLoader = function () {
            Helpers.$helperContainer.removeClass('show-loader');
        };
        Helpers.disableClicking = function () {
            $('.helpers').addClass('prevent-clicking');
        };
        Helpers.enableClicking = function () {
            $('.helpers').removeClass('prevent-clicking');
        };
        Helpers.disallowScrolling = function () {
            $('html').addClass('noscroll');
        };
        Helpers.enableScrolling = function () {
            $('html').removeClass('noscroll');
        };
        Helpers.prototype.mobileCheck = function () {
            var check = false;
            (function (a) {
                if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))
                    check = true;
            })(navigator.userAgent || navigator.vendor || window.opera);
            return check;
        };
        Helpers.abbreviateNumber = function (value) {
            var newValue = value;
            if (value >= 9999) {
                var suffixes = ['', 'K', 'M', 'B', 'T'];
                var suffixNum = Math.floor(('' + value).length / 3);
                var shortValue = '';
                for (var precision = 2; precision >= 1; precision--) {
                    shortValue = parseFloat((suffixNum !== 0 ? (value / Math.pow(1000, suffixNum)) : value).toPrecision(precision)).toString();
                    var dotLessShortValue = (shortValue + '').replace(/[^a-zA-Z 0-9]+/g, '');
                    if (dotLessShortValue.length <= 2) {
                        break;
                    }
                }
                if (shortValue % 1 !== 0) {
                    var shortNum = shortValue.toFixed(1);
                }
                newValue = shortValue + suffixes[suffixNum];
            }
            return newValue;
        };
        Helpers.convertToSlug = function (Text) {
            return Text
                .toLowerCase()
                .replace(/[^\w ]+/g, '')
                .replace(/ +/g, '-');
        };
        Helpers.iq_box_progress = function (iq, level) {
            var progress;
            progress = (iq / (level / 87) + 3);
            $('.bar-iq-box').css('left', progress + '%');
            $('.under-box-centered-iq').html(Helpers.abbreviateNumber(iq));
        };
        Helpers.iq_box_progress_account = function (iq, level) {
            var progress;
            progress = (iq / (level / 95));
            $('.bar-iq-box').css('left', progress + '%');
            $('.under-box-centered-iq').html(Helpers.abbreviateNumber(iq));
        };
        Helpers.delayExecution = function (time, callback) {
            return setTimeout(function () {
                callback();
            }, time);
        };
        Helpers.showLoadingButton = function ($button, loading) {
            if (loading) {
                $button.find('.loading-rect').addClass('show-loading');
            }
            else {
                $button.find('.loading-rect').removeClass('show-loading');
            }
        };
        Helpers.preventDefault = function (e) {
            e = e || window.event;
            if (e.preventDefault)
                e.preventDefault();
            e.returnValue = false;
        };
        Helpers.preventDefaultForScrollKeys = function (e) {
            if (Helpers.keys[e.keyCode]) {
                Helpers.preventDefault(e);
                return false;
            }
        };
        Helpers.disableScroll = function () {
            if (window.addEventListener)
                window.addEventListener('DOMMouseScroll', Helpers.preventDefault, false);
            window.onwheel = Helpers.preventDefault;
            window.onmousewheel = document.onmousewheel = Helpers.preventDefault;
            window.ontouchmove = Helpers.preventDefault;
            document.onkeydown = Helpers.preventDefaultForScrollKeys;
        };
        Helpers.enableScroll = function () {
            if (window.removeEventListener)
                window.removeEventListener('DOMMouseScroll', Helpers.preventDefault, false);
            window.onmousewheel = document.onmousewheel = null;
            window.onwheel = null;
            window.ontouchmove = null;
            document.onkeydown = null;
        };
        Helpers.validateEmail = function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@']+(\.[^<>()[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        };
        Helpers.hasWhiteSpace = function (string) {
            return string.indexOf(' ') >= 0;
        };
        Helpers.displayLoaderDots = function ($parentContainer) {
            var loader = $parentContainer.find('.loading-footer');
            loader.addClass('show-loader');
        };
        Helpers.hideLoaderDots = function ($parentContainer) {
            var loader = $parentContainer.find('.loading-footer');
            loader.removeClass('show-loader');
        };
        Helpers.detectIE = function () {
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf('MSIE ');
            if (msie > 0) {
                return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            }
            var trident = ua.indexOf('Trident/');
            if (trident > 0) {
                var rv = ua.indexOf('rv:');
                return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
            }
            var edge = ua.indexOf('Edge/');
            if (edge > 0) {
                return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
            }
            return 0;
        };
        Helpers.isInt = function (value) {
            return !isNaN(value) &&
                parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
        };
        Helpers.transitionEndEventName = function () {
            var i, undefined, el = document.createElement('div'), transitions = {
                'transition': 'transitionend',
                'OTransition': 'otransitionend',
                'MozTransition': 'transitionend',
                'WebkitTransition': 'webkitTransitionEnd'
            };
            for (i in transitions) {
                if (transitions.hasOwnProperty(i) && el.style[i] !== undefined) {
                    return transitions[i];
                }
            }
        };
        Helpers.showPopup = function (text) {
            navigator.notification.alert(text, null, 'Fractal Games', 'OK');
        };
        Helpers.$helperContainer = $('.helpers');
        Helpers.keys = { 37: 1, 38: 1, 39: 1, 40: 1 };
        return Helpers;
    }());
    Fractal.Helpers = Helpers;
})(Fractal || (Fractal = {}));
var TheColourGame;
(function (TheColourGame) {
    TheColourGame.moreInfo = $('.moreInfo');
    TheColourGame.moreInfoButton = $('.menuMoreInfoButton');
    TheColourGame.mainMenuScreen = $('.mainMenu');
    TheColourGame.backButton = $('.backInfoButton');
    TheColourGame.highSurvival = $('.survivalHighScore');
    TheColourGame.survivalPlay = $('.survival').find('.button-highscore');
    TheColourGame.highTarget = $('.targetHighScore');
    TheColourGame.targetPlay = $('.target').find('.button-highscore');
    TheColourGame.highCountdown = $('.countdownHighScore');
    TheColourGame.countdownPlay = $('.countdown').find('.button-highscore');
    TheColourGame.highShootdown = $('.shootdownHighScore');
    TheColourGame.shootdownPlay = $('.shootdown').find('.button-highscore');
    TheColourGame.$loginOverlay = $('.login-overlay');
    TheColourGame.$leaderboardOverlay = $('.leaderboard-overlay');
    TheColourGame.$clickPreventor = $('.clickPreventor');
    function hideOverlays() {
        TheColourGame.$loginOverlay.addClass('fade-in-overlay');
        TheColourGame.$leaderboardOverlay.addClass('fade-in-overlay');
        setTimeout(function () {
            TheColourGame.$loginOverlay.addClass('hidden');
            TheColourGame.$leaderboardOverlay.addClass('hidden');
        }, 700);
    }
    TheColourGame.hideOverlays = hideOverlays;
    function showOverlays() {
        TheColourGame.$loginOverlay.removeClass('hidden');
        TheColourGame.$leaderboardOverlay.removeClass('hidden');
        setTimeout(function () {
            TheColourGame.$loginOverlay.removeClass('fade-in-overlay');
            TheColourGame.$leaderboardOverlay.removeClass('fade-in-overlay');
        }, 150);
    }
    TheColourGame.showOverlays = showOverlays;
    TheColourGame.moreInfoButton.on("click", function () {
        TheColourGame.hideOverlays();
        setHighScoresInfo();
        TheColourGame.$clickPreventor.addClass("preventOn");
        TheColourGame.moreInfo.addClass("blocked");
        setTimeout(function () {
            TheColourGame.moreInfo.addClass("slideToRight");
            TheColourGame.mainMenuScreen.addClass("moveToRight");
            TheColourGame.$clickPreventor.removeClass("preventOn");
        }, 200);
    });
    TheColourGame.backButton.on("click", function () {
        TheColourGame.showOverlays();
        TheColourGame.$clickPreventor.addClass("preventOn");
        TheColourGame.moreInfo.removeClass("slideToRight");
        TheColourGame.mainMenuScreen.removeClass("moveToRight");
        setTimeout(function () {
            TheColourGame.moreInfo.removeClass("blocked");
            TheColourGame.$clickPreventor.removeClass("preventOn");
        }, 1000);
    });
    function setHighScoresInfo() {
        if (localStorage.getItem("highScoreSurvival") !== null) {
            TheColourGame.highSurvival.html(localStorage.getItem("highScoreSurvival"));
        }
        else {
            TheColourGame.highSurvival.html("0");
        }
        if (localStorage.getItem("highScoreTarget") !== null) {
            TheColourGame.highTarget.html(localStorage.getItem("highScoreTarget"));
        }
        else {
            TheColourGame.highTarget.html("0");
        }
        if (localStorage.getItem("highScoreCountdown") !== null) {
            TheColourGame.highCountdown.html(localStorage.getItem("highScoreCountdown"));
        }
        else {
            TheColourGame.highCountdown.html("0");
        }
        if (localStorage.getItem("highScoreShootdown") !== null) {
            TheColourGame.highShootdown.html(localStorage.getItem("highScoreShootdown"));
        }
        else {
            TheColourGame.highShootdown.html("0");
        }
    }
    TheColourGame.setHighScoresInfo = setHighScoresInfo;
    function setHighScoresPlay() {
        if (localStorage.getItem("highScoreSurvival") !== null) {
            TheColourGame.survivalPlay.html(localStorage.getItem("highScoreSurvival"));
        }
        else {
            TheColourGame.survivalPlay.html("0");
        }
        if (localStorage.getItem("highScoreTarget") !== null) {
            TheColourGame.targetPlay.html(localStorage.getItem("highScoreTarget"));
        }
        else {
            TheColourGame.targetPlay.html("0");
        }
        if (localStorage.getItem("highScoreCountdown") !== null) {
            TheColourGame.countdownPlay.html(localStorage.getItem("highScoreCountdown"));
        }
        else {
            TheColourGame.countdownPlay.html("0");
        }
        if (localStorage.getItem("highScoreShootdown") !== null) {
            TheColourGame.shootdownPlay.html(localStorage.getItem("highScoreShootdown"));
        }
        else {
            TheColourGame.shootdownPlay.html("0");
        }
    }
    TheColourGame.setHighScoresPlay = setHighScoresPlay;
})(TheColourGame || (TheColourGame = {}));
var TheColourGame;
(function (TheColourGame) {
    TheColourGame.mainMenu = $('.mainMenu');
    TheColourGame.buttonOne = $('.survival');
    TheColourGame.buttonTwo = $('.countdown');
    TheColourGame.buttonThree = $('.shootdown');
    TheColourGame.buttonFour = $('.target');
    TheColourGame.logoScreen = $('.logoScreen');
    TheColourGame.mql = window.matchMedia("screen and (orientation: landscape)");
    function startMainMenu(callback) {
        setTimeout(function () {
            TheColourGame.mainMenu.addClass("blocked");
            TheColourGame.logoScreen.removeClass("blocked");
            callback();
        }, Fractal.logoTransitionTime);
    }
    TheColourGame.startMainMenu = startMainMenu;
    TheColourGame.playButton = $('.menuPlayButton');
    TheColourGame.chooseMode = $('.chooseMode');
    TheColourGame.playButton.on("click", function () {
        if (TheColourGame.storeCG.products[0].checkBought() && navigator.connection.type === 'none') {
            $('.mode-buttons').addClass('unlocked');
        }
        TheColourGame.setHighScoresPlay();
        TheColourGame.$clickPreventor.addClass("preventOn");
        TheColourGame.mainMenu.addClass("moveToLeft");
        setTimeout(function () {
            TheColourGame.mainMenu.removeClass("blocked");
            TheColourGame.$clickPreventor.removeClass("preventOn");
        }, 1200);
        TheColourGame.chooseMode.addClass("slideToLeft");
        if (TheColourGame.mql.matches === true) {
            setTimeout(function () {
                TheColourGame.buttonFour.addClass("slideDown");
                TheColourGame.buttonThree.addClass("slideDown");
                TheColourGame.buttonTwo.addClass("slideDown");
                TheColourGame.buttonOne.addClass("slideDown");
            }, 800);
        }
        else {
            setTimeout(function () {
                TheColourGame.buttonFour.addClass("slideDown");
                TheColourGame.buttonThree.addClass("slideDown");
                TheColourGame.buttonTwo.addClass("slideDown");
                TheColourGame.buttonOne.addClass("slideDown");
            }, 800);
        }
    });
})(TheColourGame || (TheColourGame = {}));
var Fractal;
(function (Fractal) {
    function setAccountTransformations($item, transform) {
        var cubeWidth;
        cubeWidth = $item.find('.cube-size').width() / 2;
        switch (transform) {
            case 'initial':
                $($item).find('.profile').css({
                    WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                });
                $($item).find('.cropping-section').css({
                    WebkitTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                    MozTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                    msTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                    OTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                    transform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)'
                });
                break;
            case 'right':
                $($item).find('.profile').css({
                    WebkitTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                    MozTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                    OTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                    msTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                    transform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)'
                });
                $($item).find('.cropping-section').css({
                    WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                });
                break;
            default:
                console.error('Forgotten transform keyword');
                return 0;
        }
        $item.find('.half-account-cube-sides').css({
            WebkitTransform: 'translateZ(-' + (cubeWidth) + 'px)',
            MozTransform: 'translateZ(-' + (cubeWidth) + 'px)',
            OTransform: 'translateZ(-' + (cubeWidth) + 'px)',
            msTransform: 'translateZ(-' + (cubeWidth) + 'px)',
            transform: 'translateZ(-' + (cubeWidth) + 'px)'
        });
    }
    Fractal.setAccountTransformations = setAccountTransformations;
})(Fractal || (Fractal = {}));
var Fractal;
(function (Fractal) {
    var TemplateCompilerController = (function () {
        function TemplateCompilerController(container, source) {
            this.source = Handlebars.compile(source.html());
            this.container = container;
            this.container.html('');
        }
        TemplateCompilerController.prototype.compileData = function (data) {
            var dataSource = this.source(data);
            this.container.html(dataSource);
        };
        TemplateCompilerController.registerHelpers = function () {
            Handlebars.registerHelper('incPlace', function (selector, options) {
                if (selector === "friends") {
                    TemplateCompilerController.friendsCurrentPlace++;
                }
                else {
                    TemplateCompilerController.globalCurrentPlace++;
                }
                return "";
            });
            Handlebars.registerHelper('ifCond', function (v1, v2, options) {
                if (v1 === v2) {
                    return options.fn(this);
                }
                return options.inverse(this);
            });
            Handlebars.registerHelper('inc', function (selector, options) {
                if (selector === "friends") {
                    return TemplateCompilerController.friendsCurrentPlace;
                }
                else {
                    return TemplateCompilerController.globalCurrentPlace;
                }
            });
            Handlebars.registerHelper('checkMode', function (object, options) {
                if (object) {
                    return object;
                }
                else {
                    return 0;
                }
            });
            Handlebars.registerHelper('checkUser', function (user, options) {
                if (user.user_info.username == TheColourGame.accountHandler.user.username) {
                    return "background:rgba(85, 85, 85, 0.2);";
                }
                return "";
            });
            Handlebars.registerHelper('checkPlace', function (selector, options) {
                if (selector === "friends") {
                    switch (TemplateCompilerController.friendsCurrentPlace) {
                        case 1:
                            return "gold";
                        case 2:
                            return "silver";
                        case 3:
                            return "bronze";
                        default:
                            return "red";
                    }
                }
                else {
                    switch (TemplateCompilerController.globalCurrentPlace) {
                        case 1:
                            return "gold";
                        case 2:
                            return "silver";
                        case 3:
                            return "bronze";
                        default:
                            return "red";
                    }
                }
            });
        };
        TemplateCompilerController.globalCurrentPlace = 1;
        TemplateCompilerController.friendsCurrentPlace = 1;
        return TemplateCompilerController;
    }());
    Fractal.TemplateCompilerController = TemplateCompilerController;
})(Fractal || (Fractal = {}));
var TheColourGame;
(function (TheColourGame) {
    function setLoginTransformations($item, transform) {
        var cubeWidth;
        cubeWidth = $item.find('section').width() / 2;
        if (Fractal.Helpers.isInt(Fractal.Helpers.detectIE()) && Fractal.Helpers.detectIE() < 12) {
            switch (transform) {
                case 'initial':
                    $($item).find('.log-in').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.sign-up').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    break;
                case 'left':
                    $($item).find('.log-in').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        transform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)'
                    });
                    $($item).find('.sign-up').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    break;
                case 'right':
                    $($item).find('.log-in').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        transform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)'
                    });
                    $($item).find('.sign-up').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    break;
                case 'back':
                    $($item).find('.log-in').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        transform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)'
                    });
                    $($item).find('.sign-up').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    break;
                default:
                    return 0;
            }
            $($item).find('.half-cube-sides').css({
                WebkitTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                MozTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                OTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                msTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                transform: 'translateZ(-' + (cubeWidth) + 'px)'
            });
        }
        else {
            switch (transform) {
                case 'initial':
                    $($item).find('.log-in').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.sign-up').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    break;
                case 'left':
                    $($item).find('.log-in').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        transform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)'
                    });
                    $($item).find('.sign-up').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    break;
                case 'right':
                    $($item).find('.log-in').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        transform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)'
                    });
                    $($item).find('.sign-up').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    break;
                case 'back':
                    $($item).find('.log-in').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        transform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)'
                    });
                    $($item).find('.sign-up').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    break;
                default:
                    return 0;
            }
            $($item).find('.half-cube-sides').css({
                WebkitTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                MozTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                OTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                msTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                transform: 'translateZ(-' + (cubeWidth) + 'px)'
            });
        }
    }
    TheColourGame.setLoginTransformations = setLoginTransformations;
    function sinusoidalRotation($cube, transform, x_times, y_times) {
        var cubeWidth;
        cubeWidth = $cube.width() / 2;
        switch (transform) {
            case "first":
                $($cube).find(".left").css({
                    WebkitTransform: "rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".front").css({
                    WebkitTransform: "rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".right").css({
                    WebkitTransform: "rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".top").css({
                    WebkitTransform: "rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".bottom").css({
                    WebkitTransform: "rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".back").css({
                    WebkitTransform: "rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                break;
            case "second":
                $($cube).find(".left").css({
                    WebkitTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (-46 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (-46 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (-46 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (-46 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (-46 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".front").css({
                    WebkitTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (44 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (44 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (44 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (44 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (44 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".right").css({
                    WebkitTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (134 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (134 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (134 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (134 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (134 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".top").css({
                    WebkitTransform: "rotateX(" + (54 + (360 * x_times)) + "deg) rotateY(0deg) rotateZ(" + (46 - (y_times * 360)) + "deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(" + (54 + (360 * x_times)) + "deg) rotateY(0deg) rotateZ(" + (46 - (y_times * 360)) + "deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(" + (54 + (360 * x_times)) + "deg) rotateY(0deg) rotateZ(" + (46 - (y_times * 360)) + "deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(" + (54 + (360 * x_times)) + "deg) rotateY(0deg) rotateZ(" + (46 - (y_times * 360)) + "deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(" + (54 + (360 * x_times)) + "deg) rotateY(0deg) rotateZ(" + (46 - (y_times * 360)) + "deg) translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".bottom").css({
                    WebkitTransform: "rotateX(" + (234 + (360 * x_times)) + "deg) rotateY(0deg)  rotateZ(" + (44 + (y_times * 360)) + "deg)  translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(" + (234 + (360 * x_times)) + "deg) rotateY(0deg)  rotateZ(" + (44 + (y_times * 360)) + "deg)  translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(" + (234 + (360 * x_times)) + "deg) rotateY(0deg)  rotateZ(" + (44 + (y_times * 360)) + "deg)  translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(" + (234 + (360 * x_times)) + "deg) rotateY(0deg)  rotateZ(" + (44 + (y_times * 360)) + "deg)  translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(" + (234 + (360 * x_times)) + "deg) rotateY(0deg)  rotateZ(" + (44 + (y_times * 360)) + "deg)  translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".back").css({
                    WebkitTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (224 + (y_times * 360)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (224 + (y_times * 360)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (224 + (y_times * 360)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (224 + (y_times * 360)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (224 + (y_times * 360)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                break;
            default:
                console.log('Forgotten transform keyword');
                return 0;
        }
    }
    TheColourGame.sinusoidalRotation = sinusoidalRotation;
    function initializeCube() {
        $('.splashscreen-cube-wrapper').addClass('animate');
    }
    TheColourGame.initializeCube = initializeCube;
    function splashScreen(callback) {
        var fractalGamesSplashscreen = $(".fractal-games-splashscreen");
        initializeCube();
        setTimeout(function () {
            fractalGamesSplashscreen.addClass("fractal-games-splashscreen-end");
            setTimeout(function () {
                fractalGamesSplashscreen.addClass("hidden");
                callback();
            }, Fractal.fractalEndTransitionTime + Fractal.waitTime);
        }, Fractal.waitTime * 2 + Fractal.cubeTransitionTime);
    }
    TheColourGame.splashScreen = splashScreen;
    var logoScreen = $('.logoScreen');
    function startLogo(callback) {
        logoScreen.addClass("blocked");
        callback();
    }
    TheColourGame.startLogo = startLogo;
})(TheColourGame || (TheColourGame = {}));
var Fractal;
(function (Fractal) {
    var User = (function () {
        function User(anonymous, serviceHandler, loginCompiler, accountCompiler, username, user_avatar, iq, level, prevLevelIq, prevLevelName, nextLevelIq, nextLevelName) {
            this.anonymous = anonymous;
            this.serviceHandler = serviceHandler;
            this.loginCompiler = loginCompiler;
            this.accountCompiler = accountCompiler;
            this.username = username;
            this.user_avatar = user_avatar;
            this.iq = iq;
            this.level = level;
            this.prevLevelIq = prevLevelIq;
            this.prevLevelName = prevLevelName;
            this.nextLevelIq = nextLevelIq;
            this.nextLevelName = nextLevelName;
        }
        User.prototype.updateUser = function (data) {
            if (data) {
                this.anonymous = false;
                this.username = data.username;
                this.user_avatar = data.user_avatar_url;
                this.iq = data.total_iq;
                this.level = data.level;
                this.prevLevelIq = data.prev_level_iq;
                this.prevLevelName = data.prev_level_name;
                this.nextLevelIq = data.next_level_iq;
                this.nextLevelName = data.next_level_name;
                this.saveUserToLocalStorage();
            }
            else {
                this.anonymous = true;
                this.username = null;
                this.user_avatar = null;
                this.iq = 0;
                this.level = 'Kindergarten';
                this.prevLevelIq = 0;
                this.prevLevelName = '';
                this.nextLevelIq = 1000;
                this.nextLevelName = 'Pre-school';
            }
        };
        User.prototype.userInit = function () {
            this.loginCompiler.compileData(this);
            this.loginOverlayFunctions();
            this.leaderboardOverlayFunctions();
            this.accountCompiler.compileData(this);
            if (TheColourGame.storeReady) {
                TheColourGame.storeCG.products[0].handleProServiceCheck();
            }
            if (!this.anonymous) {
                this.saveUserToLocalStorage();
            }
        };
        User.prototype.clearHighScores = function () {
            localStorage.removeItem('highScoreSurvival');
            localStorage.removeItem('highScoreTarget');
            localStorage.removeItem('highScoreShootdown');
            localStorage.removeItem('highScoreCountdown');
        };
        User.prototype.getHighScores = function () {
            var self = this;
            this.serviceHandler.getHighScores(function (r) {
                var mode_scores = r.modes_scores;
                for (var key in mode_scores) {
                    if (mode_scores.hasOwnProperty(key)) {
                        var value = mode_scores[key];
                        var capitalisedKey = key.replace(/\w/, key.match(/\w/)[0].toUpperCase());
                        var currentScore = parseInt(localStorage.getItem("highScore" + capitalisedKey));
                        if (isNaN(currentScore) || currentScore <= value) {
                            localStorage.setItem("highScore" + capitalisedKey, value);
                        }
                        else {
                            TheColourGame.accountHandler.serviceHandler.sendHighScore(currentScore.toString(), key, function (r) {
                            }, function (r) {
                                console.error(r.responseJSON.detail);
                            });
                        }
                    }
                }
                TheColourGame.setHighScoresInfo();
                TheColourGame.setHighScoresPlay();
            }, function (r) {
                TheColourGame.setHighScoresInfo();
                TheColourGame.setHighScoresPlay();
            });
        };
        User.prototype.updateIQ = function (iq) {
            this.iq += iq;
            this.userInit();
        };
        User.prototype.showLoginOverlay = function () {
            $('.login-overlay').removeClass('disappear').removeClass('fade-in-overlay');
        };
        User.prototype.showLeaderboardOverlay = function () {
            $('.leaderboard-overlay').removeClass('disappear').removeClass('fade-in-overlay');
        };
        User.prototype.loginOverlayFunctions = function () {
            var _$loginOverlay = $('.login-overlay');
            var self = this;
            _$loginOverlay.off('click').on('click', function () {
                $('.login-button-wrapper').addClass('active');
                _$loginOverlay.find('.login-overlay-wrapper').addClass('show');
                _$loginOverlay.addClass('show');
                if (self.anonymous) {
                    self.loginModalFunctions(function () {
                    });
                    $('.login-wrapper-modal').removeClass('not-visible');
                    $('body').addClass('modal-open');
                }
                else {
                    self.accountModalFunction();
                    setTimeout(function () {
                        var modal = $('.account');
                        $('.account-wrapper-modal').removeClass('not-visible');
                        $('body').addClass('modal-open');
                    }, 500);
                }
            });
        };
        User.prototype.leaderboardModalFunctions = function () {
            var self = this;
            var $_leaderboardWrapperModal = $('.leaderboard-wrapper-modal');
            $_leaderboardWrapperModal.find('.close-modal').off('click').on('click', function (e) {
                $_leaderboardWrapperModal.find('.leaderboard-button').removeClass('active');
                $_leaderboardWrapperModal.find('.global-button-wrapper').addClass('active');
                self.closeLeaderboard();
            });
            var $friendsButton = $('.friends-button-wrapper');
            var $globalsButton = $('.global-button-wrapper');
            var $leaderboardModalWrapper = $('.leaderboard-modal-wrapper-sides');
            $friendsButton.off('click').on('click', function (e) {
                $(e.currentTarget).addClass('active');
                $globalsButton.removeClass('active');
                $leaderboardModalWrapper.addClass('effect-on').removeClass('show-right').addClass('show-front');
            });
            $globalsButton.off('click').on('click', function (e) {
                $(e.currentTarget).addClass('active');
                $friendsButton.removeClass('active');
                $leaderboardModalWrapper.addClass('effect-on').removeClass('show-front').addClass('show-right');
            });
            var globalsPagination = 1;
            var canAskForMoreGlobals = true;
            var globalTimeout;
            $('.leaderboard-modal-globals-users-wrapper').off('scroll').on('scroll', function (e) {
                if (canAskForMoreGlobals) {
                    clearTimeout(globalTimeout);
                    globalTimeout = setTimeout(function () {
                        if ($(e.currentTarget).scrollTop() + $(e.currentTarget).height() > $(e.currentTarget).height() - 100) {
                            globalsPagination++;
                            TheColourGame.accountHandler.serviceHandler.getGlobalHighScoreLeaderboard(globalsPagination, function (r) {
                                if (r.users.length !== 0) {
                                    var data = TheColourGame.accountHandler.leaderboardGlobalCompiler.source(r);
                                    var htmlData = $(data);
                                    var usersToAppend = htmlData.find('div.leaderboard-modal-globals-inner-wrapper');
                                    TheColourGame.accountHandler.leaderboardGlobalCompiler.container
                                        .find('div.leaderboard-modal-globals-inner-wrapper')
                                        .append(usersToAppend.html());
                                }
                                else {
                                    canAskForMoreGlobals = false;
                                }
                            }, function (r) {
                                console.error(r.responseJSON.detail);
                            });
                        }
                    }, 1000);
                }
            });
            var friendsPagination = 1;
            var canAskForMoreFriends = true;
            var friendsTimeout;
            $('.leaderboard-modal-friends-users-wrapper').off('scroll').on('scroll', function (e) {
                if (canAskForMoreFriends) {
                    clearTimeout(friendsTimeout);
                    friendsTimeout = setTimeout(function () {
                        if ($(e.currentTarget).scrollTop() + $(e.currentTarget).height() > $(e.currentTarget).height() - 100) {
                            if (navigator.connection.type !== 'none') {
                                window.facebookConnectPlugin.getLoginStatus(function (r) {
                                    if (r.status == 'connected') {
                                        window.facebookConnectPlugin.api('/me/friends', '', function (r) {
                                            friendsPagination++;
                                            TheColourGame.accountHandler.serviceHandler.getFriendsHighScoreLeaderboard(friendsPagination, r.data, function (r) {
                                                if (r.users.length !== 0) {
                                                    var data = TheColourGame.accountHandler.leaderboardFriendsCompiler.source(r);
                                                    var htmlData = $(data);
                                                    var usersToAppend = htmlData.find('div.leaderboard-modal-friends-inner-wrapper');
                                                    TheColourGame.accountHandler.leaderboardFriendsCompiler.container
                                                        .find('div.leaderboard-modal-friends-inner-wrapper')
                                                        .append(usersToAppend.html());
                                                }
                                                else {
                                                    canAskForMoreFriends = false;
                                                }
                                            }, function (r) {
                                                console.error(r.responseJSON);
                                            });
                                        });
                                    }
                                });
                            }
                            else {
                                Fractal.Helpers.showPopup("There is no internet connection. Connect to the internet to load the leaderboard.");
                            }
                        }
                    }, 1000);
                }
            });
        };
        User.prototype.socialFacebookLogin = function ($elem) {
            var self = this;
            $elem.off('click').on('click', function () {
                if (navigator.connection.type !== 'none') {
                    window.facebookConnectPlugin.login(["email", "public_profile", "user_friends"], function (response) {
                        var accessToken = response.authResponse["accessToken"];
                        Fractal.Helpers.rotateUntilCallback(function () {
                            self.serviceHandler.facebookLogin(accessToken, function (r) {
                                self.logUserIn(r);
                                Fractal.Helpers.removeLoader();
                            }, function (response) {
                                console.warn(response);
                                Fractal.Helpers.removeLoader();
                                Fractal.Helpers.showPopup(response.responseJSON.detail);
                            });
                        });
                    }, function (response) {
                        console.warn(response);
                        Fractal.Helpers.removeLoader();
                        Fractal.Helpers.showPopup(response.errorMessage);
                    });
                }
                else {
                    Fractal.Helpers.removeLoader();
                    Fractal.Helpers.showPopup("Please check your internet connection.");
                }
            });
        };
        User.prototype.leaderboardModalRequest = function () {
            var self = this;
            var _$leaderboardModal = $('.leaderboard-wrapper-modal');
            var _$friendsButton = $('.friends-button-wrapper');
            if (navigator.connection.type !== 'none') {
                Fractal.Helpers.rotateUntilCallback(function () {
                });
                _$friendsButton.addClass('active');
                Fractal.TemplateCompilerController.globalCurrentPlace = 1;
                Fractal.TemplateCompilerController.friendsCurrentPlace = 1;
                TheColourGame.accountHandler.serviceHandler.getGlobalHighScoreLeaderboard(1, function (r) {
                    TheColourGame.accountHandler.leaderboardGlobalCompiler.compileData(r);
                    self.leaderboardModalFunctions();
                    Fractal.Helpers.removeLoader();
                    _$leaderboardModal.removeClass('not-visible');
                }, function (r) {
                    Fractal.Helpers.removeLoader();
                });
                window.facebookConnectPlugin.getLoginStatus(function (r) {
                    if (r.status == 'connected') {
                        window.facebookConnectPlugin.api('/me/friends', '', function (r) {
                            TheColourGame.accountHandler.serviceHandler.getFriendsHighScoreLeaderboard(1, r.data, function (r) {
                                TheColourGame.accountHandler.leaderboardFriendsCompiler.compileData(r);
                                _$leaderboardModal.find('.leaderboard-modal-friends-users-wrapper').removeClass('no-facebook-connect').addClass('facebook-connect');
                            }, function (r) {
                                TheColourGame.accountHandler.leaderboardFriendsCompiler.compileData('');
                                _$leaderboardModal.find('.leaderboard-modal-friends-users-wrapper').removeClass('facebook-connect').addClass('no-facebook-connect');
                                self.socialFacebookLogin(_$leaderboardModal.find('.socialaccount_provider_facebook_button'));
                            });
                        }, function (r) {
                            Fractal.Helpers.removeLoader();
                            TheColourGame.accountHandler.leaderboardFriendsCompiler.compileData('');
                            console.log(r);
                        });
                    }
                    else {
                        Fractal.Helpers.removeLoader();
                        TheColourGame.accountHandler.leaderboardFriendsCompiler.compileData('');
                        _$leaderboardModal.find('.leaderboard-modal-friends-users-wrapper').removeClass('facebook-connect').addClass('no-facebook-connect');
                        self.socialFacebookLogin(_$leaderboardModal.find('.socialaccount_provider_facebook_button'));
                    }
                }, function (r) {
                    Fractal.Helpers.removeLoader();
                    console.log(r);
                });
            }
            else {
                Fractal.Helpers.showPopup("There is no internet connection. Please connect to the internet to view the leaderboard.");
            }
        };
        User.prototype.leaderboardOverlayFunctions = function () {
            var self = this;
            var _$leaderboardOverlay = $('.leaderboard-overlay');
            var _$leaderboardModal = $('.leaderboard-wrapper-modal');
            var _$buttons = $('.leaderboard-button');
            var _$friendsButton = $('.friends-button-wrapper');
            _$buttons.removeClass('active');
            _$leaderboardOverlay.off('click').on('click', function () {
                self.leaderboardModalRequest();
            });
        };
        User.prototype.loginModalFunctions = function (callback) {
            var $loginSignupTabs = $('#login-signup-tabs');
            var currentClearIntervalUsername;
            var currentClearIntervalEmail;
            var currentClearIntervalPassword;
            var self = this;
            var currentLoginState = 'initial';
            $('.login-overlay-popup-message-button,.login-overlay-popup-message-header-close').off('click').on('click', function () {
                $('.login-overlay-message').removeClass('show-popup');
            });
            $('#forgotten-email-form').find('#email').off('input').on('input', function (elT) {
                var el = $(elT.currentTarget);
                var val = el.val();
                el.removeClass('valid');
                clearTimeout(currentClearIntervalEmail);
                el.parent().find('span').css('display', 'none');
                el.parent().find('div').css('display', 'block');
                if (val !== '') {
                    if (!Fractal.Helpers.hasWhiteSpace(val) && Fractal.Helpers.validateEmail(val)) {
                        currentClearIntervalEmail = setTimeout(function () {
                            self.serviceHandler.checkEmail(val, function (r) {
                                if (r.free) {
                                    el.parent().find('span').html('&#x2716;');
                                    el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                                    el.parent().find('div').css('display', 'none');
                                }
                                else {
                                    el.addClass('valid');
                                    el.parent().find('span').html('&#10004;');
                                    el.parent().find('span').css({ 'display': 'block', 'color': '#0b0' });
                                    el.parent().find('div').css('display', 'none');
                                }
                            });
                        }, 2000);
                    }
                    else {
                        el.parent().find('span').html('&#x2716;');
                        el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                        el.parent().find('div').css('display', 'none');
                    }
                }
                else {
                    el.parent().find('div').css('display', 'none');
                }
            });
            $('.email-signup').off('input').on('input', function (elT) {
                var el = $(elT.currentTarget);
                var val = el.val();
                el.removeClass('valid');
                clearTimeout(currentClearIntervalEmail);
                el.parent().find('span').css('display', 'none');
                el.parent().find('div').css('display', 'block');
                if (val !== '') {
                    if (!Fractal.Helpers.hasWhiteSpace(val) && Fractal.Helpers.validateEmail(val)) {
                        currentClearIntervalEmail = setTimeout(function () {
                            self.serviceHandler.checkEmail(val, function (r) {
                                if (r.free) {
                                    el.addClass('valid');
                                    el.parent().find('span').html('&#10004;');
                                    el.parent().find('span').css({ 'display': 'block', 'color': '#0b0' });
                                    el.parent().find('div').css('display', 'none');
                                }
                                else {
                                    el.parent().find('span').html('&#x2716;');
                                    el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                                    el.parent().find('div').css('display', 'none');
                                }
                            });
                        }, 2000);
                    }
                    else {
                        el.parent().find('span').html('&#x2716;');
                        el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                        el.parent().find('div').css('display', 'none');
                    }
                }
                else {
                    el.parent().find('div').css('display', 'none');
                }
            });
            $('.username-signup').off('input').on('input', function (e) {
                var el = $(e.currentTarget);
                var val = el.val();
                el.removeClass('valid');
                clearTimeout(currentClearIntervalUsername);
                el.parent().find('span').css('display', 'none');
                el.parent().find('div').css('display', 'block');
                if (val !== '') {
                    if (!Fractal.Helpers.hasWhiteSpace(val)) {
                        currentClearIntervalUsername = setTimeout(function () {
                            self.serviceHandler.checkUsername(val, function (r) {
                                if (r.free) {
                                    el.addClass('valid');
                                    el.parent().find('span').html('&#10004;');
                                    el.parent().find('span').css({ 'display': 'block', 'color': '#0b0' });
                                    el.parent().find('div').css('display', 'none');
                                }
                                else {
                                    el.parent().find('span').html('&#x2716;');
                                    el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                                    el.parent().find('div').css('display', 'none');
                                }
                            });
                        }, 2000);
                    }
                    else {
                        el.parent().find('span').html('&#x2716;');
                        el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                        el.parent().find('div').css('display', 'none');
                    }
                }
                else {
                    el.parent().find('div').css('display', 'none');
                }
            });
            $('.password-signup').off('input').on('input', function (e) {
                var el = $(e.currentTarget);
                var val = el.val();
                el.removeClass('valid');
                clearTimeout(currentClearIntervalPassword);
                el.parent().find('span').css('display', 'none');
                el.parent().find('div').css('display', 'block');
                if (val !== '') {
                    if (!Fractal.Helpers.hasWhiteSpace(val)) {
                        currentClearIntervalPassword = setTimeout(function () {
                            if (val.length >= 6) {
                                el.addClass('valid');
                                el.parent().find('span').html('&#10004;');
                                el.parent().find('span').css({ 'display': 'block', 'color': '#0b0' });
                                el.parent().find('div').css('display', 'none');
                            }
                            else {
                                el.parent().find('span').html('&#x2716;');
                                el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                                el.parent().find('div').css('display', 'none');
                            }
                        }, 2000);
                    }
                    else {
                        el.parent().find('span').html('&#x2716;');
                        el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                        el.parent().find('div').css('display', 'none');
                    }
                }
                else {
                    el.parent().find('div').css('display', 'none');
                }
            });
            var sendForgottenPassword = true;
            $('.send-button').off('click').on('click', function () {
                if (navigator.connection.type !== 'none') {
                    if (sendForgottenPassword) {
                        self.serviceHandler.forgottenPassword($('.email').val(), function (r) {
                            if (r !== '') {
                                Fractal.Helpers.showPopup('Please check if the entered email is valid.');
                            }
                            else {
                                sendForgottenPassword = false;
                                setTimeout(function () {
                                    sendForgottenPassword = true;
                                }, 15000);
                                Fractal.Helpers.showPopup('Please check your email.');
                            }
                        }, function () {
                        });
                    }
                    else {
                        Fractal.Helpers.showPopup('Please wait few seconds and try again :)');
                    }
                }
                else {
                    Fractal.Helpers.showPopup('Please check your internet connection.');
                }
            });
            $('.login-button').off('click').on('click', function () {
                if (navigator.connection.type !== 'none') {
                    Fractal.Helpers.rotateUntilCallback(function () {
                        var username = $('.username').val();
                        var password = $('.password').val();
                        self.serviceHandler.insertToken();
                        self.serviceHandler.login(username, password, function (response) {
                            self.logUserIn(response);
                            Fractal.Helpers.removeLoader();
                        }, function () {
                            Fractal.Helpers.showPopup('The username or password were incorrect.');
                            Fractal.Helpers.removeLoader();
                        });
                    });
                }
                else {
                    Fractal.Helpers.showPopup('Please check your internet connection.');
                }
            });
            $('.signup-button').off('click').on('click', function () {
                if (navigator.connection.type !== 'none') {
                    var pwdVal = $('.password-signup');
                    var usrVal = $('.username-signup');
                    var emailVal = $('.email-signup');
                    var sendFlag = true;
                    var errors = '';
                    if (emailVal.val() === '') {
                        errors += 'Please enter your email.';
                        sendFlag = false;
                    }
                    else {
                        if (!Fractal.Helpers.validateEmail(emailVal.val())) {
                            errors += 'Email is not valid.';
                            sendFlag = false;
                        }
                    }
                    if (usrVal.val() === '') {
                        errors += 'Please enter your username.';
                        sendFlag = false;
                    }
                    else {
                        if (!usrVal.hasClass('valid')) {
                            errors += 'Username is already taken.';
                            sendFlag = false;
                        }
                    }
                    if (pwdVal.val() === '') {
                        errors += 'Please enter your password.';
                        sendFlag = false;
                    }
                    else {
                        if (!pwdVal.hasClass('valid')) {
                            errors += 'Password must be at least 6 characters.';
                            sendFlag = false;
                        }
                    }
                    if (sendFlag) {
                        self.serviceHandler.registration(emailVal.val(), usrVal.val(), pwdVal.val(), function (response) {
                            if (response.status !== 200 && !response.token) {
                            }
                            else {
                                self.saveUser(response);
                            }
                        }, function () {
                        });
                    }
                    else {
                        Fractal.Helpers.showPopup(errors);
                    }
                }
                else {
                    Fractal.Helpers.showPopup('Please check your internet connection.');
                }
            });
            if ($loginSignupTabs.length > 0) {
                $loginSignupTabs.removeClass('transition-toggle');
                TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);
            }
            $('.login-button-wrapper').off('click').on('click', function (e) {
                currentLoginState = 'initial';
                $('.signup-button-wrapper').removeClass('active');
                $loginSignupTabs.addClass('transition-toggle');
                $(e.currentTarget).addClass('active');
                TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);
            });
            $('.signup-button-wrapper').off('click').on('click', function (e) {
                currentLoginState = 'left';
                $('.login-button-wrapper').removeClass('active');
                $loginSignupTabs.addClass('transition-toggle');
                $(e.currentTarget).addClass('active');
                TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);
            });
            $('.forgotten-password-button').off('click').on('click', function (e) {
                currentLoginState = 'right';
                $loginSignupTabs.addClass('transition-toggle');
                TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);
            });
            self.socialFacebookLogin($('.socialaccount_provider_facebook'));
            $(".socialaccount_provider_google").off("click").on("click", function () {
                if (navigator.connection.type !== 'none') {
                    window.plugins.googleplus.login({}, function (obj) {
                        Fractal.Helpers.rotateUntilCallback(function () {
                            self.serviceHandler.googleLogin(obj, function (r) {
                                self.logUserIn(r);
                                Fractal.Helpers.removeLoader();
                            }, function (r) {
                                Fractal.Helpers.showPopup("Social account's email is already registered.");
                                Fractal.Helpers.removeLoader();
                            });
                        });
                    }, function (msg) {
                        Fractal.Helpers.showPopup("Something went wrong :( Please try again.");
                    });
                }
                else {
                    Fractal.Helpers.showPopup("Please check your internet connection.");
                }
            });
            $('.login-modal').find('.close-modal').off('click').on('click', function () {
                self.closeLogin();
            });
            TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);
            callback();
        };
        User.prototype.logUserIn = function (response) {
            var self = this;
            self.saveUser(response);
        };
        User.prototype.convertImgToBase64URL = function (url, callback, outputFormat) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function () {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                var dataURL;
                canvas.height = img.height;
                canvas.width = img.width;
                ctx.drawImage(img, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL);
                canvas = null;
                img = null;
            };
            img.src = url;
        };
        User.prototype.saveUserToLocalStorage = function () {
            var userToString = {
                username: this.username,
                total_iq: this.iq,
                level: this.level,
                next_level_iq: this.nextLevelIq,
                next_level_name: this.nextLevelName,
                prev_level_iq: this.prevLevelIq,
                prev_level_name: this.prevLevelName,
                user_avatar_url: this.user_avatar,
            };
            this.convertImgToBase64URL(userToString.user_avatar_url, function (base64Img) {
                userToString.user_avatar_url = base64Img;
                var stringifyResponse = JSON.stringify(userToString);
                localStorage.setItem('userInfo', stringifyResponse);
            }, 'png');
        };
        User.prototype.saveUser = function (user) {
            var _this = this;
            var self = this;
            var objToSave = {
                token: user.token
            };
            var stringifyResponse = JSON.stringify(objToSave);
            localStorage.setItem('userCred', stringifyResponse);
            self.serviceHandler.initalize();
            self.serviceHandler.shortAccountInfo(function (r) {
                self.updateUser(r.user);
                self.saveUserToLocalStorage();
                _this.getHighScores();
                _this.convertImgToBase64URL(self.user_avatar, function (base64Img) {
                    self.user_avatar = base64Img;
                    self.userInit();
                    self.closeLogin();
                    self.closeLeaderboard();
                }, 'png');
            }, function (r) {
            });
        };
        User.prototype.accountModalFunction = function () {
            var permitted = false;
            window.requestPermissions.requestCamera(function (r) {
                console.log("%c" + r, "color:green");
                permitted = true;
            }, function (r) {
                console.log("%c" + r, "color:red");
                permitted = false;
            }, "checkCameraPermission");
            $("#file").off('click').on('click', function (e) {
                if (!permitted) {
                    window.requestPermissions.requestCamera(function (r) {
                        permitted = true;
                        return false;
                    }, function (r) {
                        window.requestPermissions.requestCamera(function (r) {
                            permitted = true;
                            console.log("%c" + r, "color:green");
                            return false;
                        }, function (r) {
                            console.log("%c" + r, "color:red");
                            Fractal.Helpers.showPopup("You have not granted permission for photos. If you want to change your avatar you should allow it.");
                            return false;
                        }, "requestCameraPermission");
                        return false;
                    }, "checkCameraPermission");
                    if (!permitted) {
                        e.preventDefault();
                    }
                }
            });
            var self = this;
            var currentState = 'initial';
            $('.profile-edit-save').off('click').on('click', function (e) {
                if (navigator.connection.type !== 'none') {
                    var currentAvatar = $('.current-avatar');
                    var accountUsername = $('#username-edit');
                    var imageB64String = '';
                    var container_1 = $(e.currentTarget);
                    if (currentAvatar.attr('changed') == '' || accountUsername.val() != '') {
                        container_1.find('.loading-rect').addClass('show-loading-g');
                        if (currentAvatar.attr('changed') == '') {
                            imageB64String = currentAvatar.attr('src');
                            imageB64String = imageB64String.toString();
                            imageB64String = imageB64String.replace('data:image/png;base64,', '');
                        }
                        var obj_1 = {
                            username: accountUsername.val(),
                            image_base64: imageB64String
                        };
                        Fractal.Helpers.rotateUntilCallback(function (cubeInterval) {
                            self.serviceHandler.changeInfo(obj_1, function (r) {
                                var newImg = new Image();
                                newImg.src = r.user.user_avatar_url;
                                newImg.onload = function () {
                                    self.updateUser(r.user);
                                    self.convertImgToBase64URL(self.user_avatar, function (base64Img) {
                                        self.user_avatar = base64Img;
                                        self.closeAccount();
                                        self.userInit();
                                        Fractal.Helpers.removeLoader();
                                        container_1.find('.loading-rect').removeClass('show-loading-g');
                                        newImg = null;
                                    }, 'png');
                                };
                            }, function (r) {
                                var msg = r.responseText === '' ? "Please check your internet connection." : r.responseText;
                                self.closeAccount();
                                Fractal.Helpers.showPopup(msg);
                                Fractal.Helpers.removeLoader();
                                container_1.find('.loading-rect').removeClass('show-loading-g');
                            });
                        });
                    }
                    else {
                        if (currentAvatar.attr('changed') != '') {
                            currentAvatar.addClass('no-red-required');
                        }
                        if (accountUsername.val() == '') {
                            accountUsername.addClass('required');
                        }
                    }
                }
                else {
                    Fractal.Helpers.showPopup("Please check your internet connection.");
                }
            });
            $('.profile-logout').off('click').on('click', function () {
                localStorage.removeItem('userInfo');
                localStorage.removeItem('userCred');
                localStorage.removeItem('highScoreTarget');
                localStorage.removeItem('highScoreSurvival');
                localStorage.removeItem('highScoreCountdown');
                localStorage.removeItem('highScoreShootdown');
                if (TheColourGame.storeReady) {
                    localStorage.setItem('productBought', window.store.get('com.fractalgames.thecolourgame.noads.extramodes').owned);
                }
                else {
                    localStorage.removeItem('productBought');
                }
                self.updateUser();
                self.userInit();
                self.serviceHandler.removeToken();
                self.closeAccount();
                $('.login-overlay-wrapper').addClass('show');
                TheColourGame.setHighScoresInfo();
                TheColourGame.setHighScoresPlay();
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var $resizeImageSelector = $('.resize-image');
                        $('.current-avatar').removeClass('no-red-required');
                        currentState = 'right';
                        $('.half-account-cube-sides').addClass('transition-toggle');
                        Fractal.setAccountTransformations($('#account-sections'), currentState);
                        $resizeImageSelector.attr('src', e.target.result);
                        resizeableImage($resizeImageSelector);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            ;
            $('#file').change(function () {
                readURL(this);
            });
            $(window).resize(function () {
                $('.half-account-cube-sides').removeClass('transition-toggle');
                Fractal.setAccountTransformations($('#account-sections'), currentState);
            });
            Fractal.setAccountTransformations($('#account-sections'), currentState);
            Fractal.Helpers.iq_box_progress_account(self.iq, self.nextLevelIq);
            var resizeableImage = function (imageTarget) {
                var $container;
                var orig_src = new Image();
                var image_target = $(imageTarget).get(0);
                var resize_canvas = document.createElement('canvas');
                var oldPosition = {
                    x: 0,
                    y: 0
                };
                var transform = {
                    translate: { x: 0, y: 0 },
                    scale: 1,
                    angle: 0,
                    rx: 0,
                    ry: 0,
                    rz: 0
                };
                var lastX = 0, lastY = 0, lastScale = 1, lastRotation = 0;
                imageTarget = null;
                var deltaRotation = 0;
                var hamContainer;
                var updateElementTransform = function (el) {
                    var value = [
                        'translate3d(' + transform.translate.x + 'px, ' + transform.translate.y + 'px, 0)',
                        'scale(' + transform.scale + ', ' + transform.scale + ')',
                        'rotate3d(' + transform.rx + ',' + transform.ry + ',' + transform.rz + ',' + transform.angle + 'deg)'
                    ];
                    value = value.join(" ");
                    el.style.webkitTransform = value;
                    el.style.mozTransform = value;
                    el.style.transform = value;
                };
                var init = function () {
                    var image_div = $('.resize-image');
                    $('.cropping').append(image_div);
                    $('.resize-container').remove();
                    orig_src.src = image_target.src;
                    $(image_target).wrap('<div class="resize-container" style="visibility:hidden"></div>')
                        .before('<span class="resize-handle resize-handle-nw"></span>')
                        .before('<span class="resize-handle resize-handle-ne"></span>')
                        .after('<span class="resize-handle resize-handle-se"></span>')
                        .after('<span class="resize-handle resize-handle-sw"></span>');
                    $container = $(image_target).parent('.resize-container');
                    updateElementTransform($container[0]);
                    hamContainer = new window.Hammer($container[0], {});
                    hamContainer.get('rotate').set({ enable: true });
                    hamContainer.get('pan').set({ direction: window.Hammer.DIRECTION_ALL });
                    hamContainer.get('pinch').set({ enable: true });
                    hamContainer.on('pan', function (ev) {
                        transform.translate = {
                            x: ev.deltaX + lastX,
                            y: ev.deltaY + lastY
                        };
                        updateElementTransform($container[0]);
                    });
                    hamContainer.on('panend', function (ev) {
                        lastX = ev.deltaX;
                        lastY = ev.deltaY;
                    });
                    hamContainer.on('pinch', function (ev) {
                        transform.scale = lastScale * ev.scale;
                        updateElementTransform($container[0]);
                    });
                    hamContainer.on('pinchend', function (ev) {
                        lastScale = transform.scale;
                    });
                    hamContainer.on('rotate', function (ev) {
                        transform.rz = 1;
                        deltaRotation = deltaRotation - ev.rotation;
                        transform.angle -= deltaRotation;
                        updateElementTransform($container[0]);
                        deltaRotation = ev.rotation;
                    });
                    hamContainer.on('rotatestart', function (ev) {
                        deltaRotation = ev.rotation;
                    });
                    $('.js-crop').on('click', crop);
                    setTimeout(function () {
                        var $overlaySelector = $('.overlay');
                        var size = $overlaySelector.width();
                        resizeImage(size, size / orig_src.width * orig_src.height);
                        var overlay = $overlaySelector;
                        var imageSize = $('.resize-container');
                        setTimeout(function () {
                            imageSize.offset({
                                'left': (overlay.offset().left - ((imageSize.width() - overlay.width()) / 2)),
                                'top': (overlay.offset().top - ((imageSize.height() - overlay.height()) / 2))
                            });
                            $('.resize-container').css('visibility', 'visible');
                            oldPosition.x = $container.offset().left;
                            oldPosition.y = $container.offset().top;
                        }, 100);
                    }, 400);
                };
                var getMatrix = function (obj) {
                    return obj.css("-webkit-transform") ||
                        obj.css("-moz-transform") ||
                        obj.css("-ms-transform") ||
                        obj.css("-o-transform") ||
                        obj.css("transform");
                };
                var parseMatrix = function (_str) {
                    return _str.replace(/^matrix(3d)?\((.*)\)$/, '$2').split(/, /);
                };
                var getRotateDegrees = function (obj) {
                    var matrix = parseMatrix(getMatrix(obj)), rotationAngle = 0;
                    if (matrix[0] !== 'none') {
                        var a = matrix[0], b = matrix[1], c = matrix[2], d = matrix[3];
                        rotationAngle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
                    }
                    return rotationAngle;
                };
                var getScaleDegrees = function (obj) {
                    var matrix = parseMatrix(getMatrix(obj)), scale = 1;
                    if (matrix[0] !== 'none') {
                        var a = matrix[0], b = matrix[1];
                        scale = Math.sqrt(a * a + b * b);
                    }
                    return scale;
                };
                var resizeImage = function (width, height) {
                    resize_canvas.width = width;
                    resize_canvas.height = height;
                    resize_canvas.getContext('2d').drawImage(orig_src, 0, 0, width, height);
                    $(image_target).attr('src', resize_canvas.toDataURL('image/png'));
                };
                var drawRotatedImage = function (context, image, x, y, angle, width, height, scale, difference) {
                    context.save();
                    context.translate(x + image.width / 2, y + image.height / 2);
                    context.rotate(angle * Math.PI / 180);
                    context.scale(scale, scale);
                    context.translate((-image.width) / 2, (-image.height) / 2);
                    context.drawImage(image, 0, 0);
                    context.restore();
                };
                var crop = function () {
                    var $overlaySelector = $('.overlay');
                    var imageScaleFactor = getScaleDegrees($container);
                    var imageDegrees = getRotateDegrees($container);
                    var difference = {
                        x: oldPosition.x - $container.offset().left,
                        y: oldPosition.y - $container.offset().top
                    };
                    var crop_canvas, left = -($overlaySelector.offset().left - transform.translate.x - oldPosition.x), top = -($overlaySelector.offset().top - transform.translate.y - oldPosition.y), width = $overlaySelector.width(), height = $overlaySelector.height();
                    crop_canvas = document.createElement('canvas');
                    var context = crop_canvas.getContext('2d');
                    crop_canvas.width = width;
                    crop_canvas.height = height;
                    drawRotatedImage(context, image_target, left, top, imageDegrees, width, height, imageScaleFactor, difference);
                    $('.current-avatar').attr('src', crop_canvas.toDataURL('image/png')).attr('changed', '');
                    currentState = 'initial';
                    Fractal.setAccountTransformations($('#account-sections'), currentState);
                    hamContainer = null;
                };
                init();
            };
            $('input').off('keyup').on('keyup', function (e) {
                $(e.currentTarget).removeClass('required');
            });
            $('.account-modal').find('.close-modal').off('click').on('click', function () {
                self.closeAccount();
            });
        };
        User.prototype.closeLeaderboard = function () {
            var _$leaderboardWrapperModal = $('.leaderboard-wrapper-modal');
            var _$buttons = $('.leaderboard-button');
            _$buttons.removeClass('active');
            _$leaderboardWrapperModal.addClass('not-visible');
            _$leaderboardWrapperModal.find('.leaderboard-modal-wrapper-sides').removeClass('show-right').addClass('show-front');
            $('body').removeClass('modal-open');
        };
        User.prototype.closeAccount = function () {
            $('.loading-rect').removeClass('greated-index');
            $('.account-wrapper-modal').addClass('not-visible');
            $('body').removeClass('modal-open');
            $('.resize-container').css('visibility', 'hidden');
            $('.half-account-cube-sides').removeClass('transition-toggle');
            var currentState = 'initial';
            Fractal.setAccountTransformations($('#account-sections'), currentState);
            $('.account-wrapper input').each(function (index, el) {
                $(el).val('').removeClass('required').removeClass('no-red-required');
            });
            $('.login-overlay').removeClass('show');
        };
        User.prototype.closeLogin = function () {
            $('.missing-email').removeClass('active-form');
            $('#forgotten-email-form').removeClass('active-form');
            $('.tabs-3 li').removeClass('active');
            $('.login-wrapper-modal').addClass('not-visible');
            $('#login-signup-tabs').removeClass('transition-toggle');
            var currentState = 'initial';
            $('body').removeClass('modal-open');
            TheColourGame.setLoginTransformations($('#login-signup-tabs'), currentState);
            $('.login-wrapper input').each(function (index, el) {
                $(el).val('').removeClass('required').removeClass('no-red-required');
            });
            $('.login-overlay').removeClass('show');
        };
        User.isLoggedIn = function () {
            var jsonUserInfo = localStorage.getItem('userCred');
            return jsonUserInfo != null;
        };
        User.getSavedCred = function () {
            var jsonUserInfo = localStorage.getItem('userCred');
            if (jsonUserInfo != null) {
                jsonUserInfo = JSON.parse(jsonUserInfo);
                return jsonUserInfo;
            }
            return null;
        };
        User.getSavedInfo = function () {
            var jsonUserInfo = localStorage.getItem('userInfo');
            if (jsonUserInfo != null) {
                jsonUserInfo = JSON.parse(jsonUserInfo);
                return jsonUserInfo;
            }
            return null;
        };
        return User;
    }());
    Fractal.User = User;
})(Fractal || (Fractal = {}));
var Fractal;
(function (Fractal) {
    var RequestsHandler = (function () {
        function RequestsHandler() {
        }
        RequestsHandler.prototype.initalize = function () {
            var user = Fractal.User.getSavedCred();
            if (user) {
                $.ajaxSetup({
                    headers: {
                        'Authorization': 'Token ' + user.token
                    }
                });
                this.token = user.token;
                console.log("User Token: " + user.token);
            }
            else {
                console.warn('User is not logged in. Token not put in ajax request.');
            }
        };
        RequestsHandler.prototype.removeToken = function () {
            this.token = null;
            delete $.ajaxSettings.headers["Authorization"];
        };
        RequestsHandler.prototype.insertToken = function () {
            this.initalize();
        };
        RequestsHandler.prototype.login = function (username, password, successCallback, errorCallback) {
            var requestData = {
                username: username,
                password: password
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.LOGIN_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.shortAccountInfo = function (successCallback, errorCallback) {
            $.ajax({
                type: 'GET',
                url: Fractal.MAIN_URL + Fractal.SHORT_ACCOUNT_INFO_URL,
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.registration = function (email, username, password, successCallback, errorCallback) {
            var requestData = {
                email: email,
                username: username,
                password1: password,
                password2: password
            };
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: Fractal.MAIN_URL + Fractal.REGISTRATION_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.forgottenPassword = function (email, successCallback, errorCallback) {
            var requestData = {
                email: email
            };
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: Fractal.MAIN_URL + Fractal.FORGOTTEN_PASSWORD_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.changeInfo = function (obj, successCallback, errorCallback) {
            $.ajax({
                type: 'PUT',
                url: Fractal.MAIN_URL + Fractal.CHANGE_INFO,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(obj),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                },
                dataType: ''
            });
        };
        RequestsHandler.prototype.checkUsername = function (username, callback) {
            var obj = {
                username: username
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.CHECK_USERNAME_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(obj),
                success: function (response) {
                    callback(response);
                },
                error: function (response) {
                    callback(response);
                },
                dataType: ''
            });
        };
        RequestsHandler.prototype.checkEmail = function (email, callback) {
            var obj = {
                email: email
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.CHECK_EMAIL_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(obj),
                success: function (response) {
                    callback(response);
                },
                error: function (response) {
                    callback(response);
                },
                dataType: ''
            });
        };
        RequestsHandler.prototype.sendScore = function (score, successCallback, errorCallback) {
            console.info('Sending score...');
            var encryptedData = window.encrypt(score, window.CryptoJS.enc.Utf8.parse(this.token.substring(0, 16)));
            var data = {
                enc: encryptedData,
                game_identifier: 'the-colour-game'
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.POST_SCORE,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.sendHighScore = function (score, modeIdentifier, successCallback, errorCallback) {
            console.info('Sending high score...');
            var encryptedData = window.encrypt(score, window.CryptoJS.enc.Utf8.parse(this.token.substring(0, 16)));
            var data = {
                enc: encryptedData,
                game_identifier: TheColourGame.GAME_IDENTIFIER,
                mode_identifier: modeIdentifier
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.POST_HIGH_SCORE,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.getHighScores = function (successCallback, errorCallback) {
            var data = {
                game_identifier: TheColourGame.GAME_IDENTIFIER,
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.GET_HIGH_SCORES,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.getGlobalHighScoreLeaderboard = function (page, successCallback, errorCallback) {
            var data = {
                "page": page,
                "game_identifier": TheColourGame.GAME_IDENTIFIER
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.GLOBAL_HIGH_SCORES,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                success: function (r) {
                    successCallback(r);
                },
                error: function (r) {
                    console.error("There was an error getting the global high scores.");
                    errorCallback(r);
                }
            });
        };
        RequestsHandler.prototype.getFriendsHighScoreLeaderboard = function (page, friendsList, successCallback, errorCallback) {
            var data = {
                "page": page,
                "friends_list": friendsList,
                "game_identifier": TheColourGame.GAME_IDENTIFIER
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.FRIENDS_HIGH_SCORES,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                success: function (r) {
                    successCallback(r);
                },
                error: function (r) {
                    errorCallback(r);
                }
            });
        };
        RequestsHandler.prototype.checkUserInfo = function (successCallback, errorCallback) {
            $.ajax({
                type: 'GET',
                crossDomain: true,
                url: Fractal.MAIN_URL + Fractal.USER_INFO,
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                },
                statusCode: {
                    401: function (response) {
                        errorCallback(response);
                    }
                }
            });
        };
        RequestsHandler.prototype.facebookLogin = function (accessToken, successCallback, errorCallback) {
            var requestData = {
                access_token: accessToken
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.FACEBOOK_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.googleLogin = function (requestData, successCallback, errorCallback) {
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.GOOGLE_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.buyNonConsumableItem = function (gameIdentifier, productId, token, successCallback, errorCallback) {
            var data = {
                game_identifier: gameIdentifier,
                package_name: TheColourGame.PACKAGE_NAME,
                product_id: productId,
                token: token
            };
            $.ajax({
                type: "POST",
                url: Fractal.MAIN_URL + Fractal.BUY_GAME_URL,
                headers: {
                    "Authorization": "Token " + this.token
                },
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.checkServiceProBought = function (gameIdentifier, successCallback, errorCallback) {
            var data = {
                game_identifier: gameIdentifier,
            };
            $.ajax({
                type: "POST",
                url: Fractal.MAIN_URL + Fractal.CHECK_GAME_BOUGHT_URL,
                headers: {
                    "Authorization": "Token " + this.token
                },
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        return RequestsHandler;
    }());
    Fractal.RequestsHandler = RequestsHandler;
})(Fractal || (Fractal = {}));
var Fractal;
(function (Fractal) {
    var accountHandler = (function () {
        function accountHandler() {
            var _this = this;
            this.serviceHandler = new Fractal.RequestsHandler();
            this.serviceHandler.initalize();
            Fractal.TemplateCompilerController.registerHelpers();
            this.loginCompiler = new Fractal.TemplateCompilerController($('.login-overlay'), $('#hb-template-login'));
            this.accountCompiler = new Fractal.TemplateCompilerController($('.account-modal'), $('#hb-template-account'));
            this.leaderboardGlobalCompiler = new Fractal.TemplateCompilerController($('.leaderboard-modal-globals-users'), $('#hb-template-globals-leaderboard'));
            this.leaderboardFriendsCompiler = new Fractal.TemplateCompilerController($('.leaderboard-modal-friends-users'), $('#hb-template-friends-leaderboard'));
            if (navigator.connection.type !== 'none') {
                this.serviceHandler.shortAccountInfo(function (r) {
                    _this.user = _this.registerUser(r.user);
                    _this.user.userInit();
                    _this.user.getHighScores();
                }, function (r) {
                    localStorage.removeItem('userCred');
                    localStorage.removeItem('userInfo');
                    _this.user = _this.registerAnonymous();
                    _this.user.userInit();
                });
            }
            else {
                debugger;
                var user = Fractal.User.getSavedInfo();
                if (user) {
                    this.user = this.registerUser(user);
                    this.user.userInit();
                }
                else {
                    localStorage.removeItem('userCred');
                    localStorage.removeItem('userInfo');
                    this.user = this.registerAnonymous();
                    this.user.userInit();
                }
            }
        }
        accountHandler.prototype.registerUser = function (user) {
            return new Fractal.User(false, this.serviceHandler, this.loginCompiler, this.accountCompiler, user.username, user.user_avatar_url, user.total_iq, user.level, user.prev_level_iq, user.prev_level_name, user.next_level_iq, user.next_level_name);
        };
        accountHandler.prototype.registerAnonymous = function () {
            return new Fractal.User(true, this.serviceHandler, this.loginCompiler, this.accountCompiler, null, null, 0, "Kindergarten", 0, null, 1000, "Pre-school");
        };
        return accountHandler;
    }());
    Fractal.accountHandler = accountHandler;
})(Fractal || (Fractal = {}));
var Fractal;
(function (Fractal) {
    var Product = (function () {
        function Product(id, alias, productType) {
            this.id = id;
            this.alias = alias;
            this.productType = productType;
            this.register();
            this.onBuyAndError();
        }
        Product.prototype.register = function () {
            var self = this;
            window.store.register({
                id: self.id,
                alias: self.alias,
                type: self.productType
            });
            window.store.when(self.id).approved(function (product) {
                product.finish();
            });
        };
        Product.prototype.onBuyAndError = function () {
            var self = this;
            window.store.when(self.id).owned(function (product) {
                self.unlockAndSave();
                self.handleProServiceCheck(product);
            });
            window.store.when(self.id).error(function (product) {
                console.error(product);
            });
        };
        Product.prototype.handleProServiceCheck = function (nonConProduct) {
            if (!nonConProduct) {
                nonConProduct = window.store.get('noadsextramodes');
            }
            var self = this;
            var serviceTransactionSaved = JSON.parse(localStorage.getItem('serviceTransactionSaved'));
            if (TheColourGame.accountHandler.user && !TheColourGame.accountHandler.user.anonymous) {
                TheColourGame.accountHandler.serviceHandler.checkServiceProBought(TheColourGame.GAME_IDENTIFIER, function (r) {
                    var is_pro = r.is_pro;
                    if (is_pro) {
                        self.unlockAndSave();
                    }
                    else {
                        self.lockAndSave();
                        if (nonConProduct.owned && nonConProduct.transaction) {
                            var transactionInfo = JSON.parse(nonConProduct.transaction.receipt);
                            TheColourGame.accountHandler.serviceHandler.buyNonConsumableItem(TheColourGame.GAME_IDENTIFIER, transactionInfo.productId, transactionInfo.purchaseToken, function (r) {
                                self.unlockAndSave();
                                console.info(r);
                            }, function (r) {
                                self.lockAndSave();
                                console.error(r);
                            });
                        }
                    }
                }, function (r) {
                    console.error(r);
                });
            }
            else {
                if (self.checkBought()) {
                    self.unlockAndSave();
                }
                else {
                    self.lockAndSave();
                }
            }
        };
        Product.prototype.lockAndSave = function () {
            var self = this;
            var modeButtons = $('.mode-buttons');
            modeButtons.eq(2).removeClass('unlocked');
            modeButtons.eq(3).removeClass('unlocked');
            localStorage.setItem('productBought', 'false');
        };
        Product.prototype.unlockAndSave = function () {
            var self = this;
            $('.buy-extra-mods-wrapper').removeClass('show-buy');
            $('.mode-buttons').addClass('unlocked');
            localStorage.setItem('productBought', 'true');
        };
        Product.prototype.checkBought = function () {
            var self = this;
            if (TheColourGame.storeReady) {
                if (JSON.parse(localStorage.getItem("productBought"))) {
                    return JSON.parse(localStorage.getItem("productBought"));
                }
                return window.store.get(self.id).owned;
            }
            else {
                return JSON.parse(localStorage.getItem("productBought"));
            }
        };
        Product.prototype.buy = function () {
            window.store.order(this.id);
        };
        return Product;
    }());
    Fractal.Product = Product;
})(Fractal || (Fractal = {}));
var Fractal;
(function (Fractal) {
    var Store = (function () {
        function Store() {
            window.store.ready(function () {
                TheColourGame.storeReady = true;
                console.info('Store ready \\m/');
            });
            window.store.error(function (error) {
                if (error.code === 7) {
                    navigator.notification.alert("This item has already been purchased on this device. To buy it again please change the google account on your phone.", null, 'Buy Extra Modes and Remove Ads.', 'OK');
                }
                else if (navigator.connection.type !== 'none') {
                    navigator.notification.alert("An error occurred. Please try again later.", null, 'Buy Extra Modes and Remove Ads.', 'OK');
                }
            });
            this.products = [];
        }
        Store.prototype.addProduct = function (id, alias, type) {
            var product = new Fractal.Product(id, alias, type);
            this.products.push(product);
        };
        Store.prototype.getProduct = function (id) {
            var self = this;
            var product = null;
            for (var i = 0; i < self.products.length; i++) {
                if (self.products[i].id === id) {
                    product = self.products[i];
                }
            }
            return product;
        };
        Store.prototype.load = function () {
            window.store.refresh();
        };
        return Store;
    }());
    Fractal.Store = Store;
})(Fractal || (Fractal = {}));
var Fractal;
(function (Fractal) {
    var HeyzapController = (function () {
        function HeyzapController(personal_id) {
            this.personal_id = personal_id;
            window.HeyzapAds.showMediationTestSuite();
            this.init();
        }
        HeyzapController.prototype.init = function () {
            window.HeyzapAds.showMediationTestSuite();
            HeyzapAds.start(this.personal_id).then(function () {
                console.info("Heyzap initialised.");
            }, function (error) {
                console.error("Heyzap unable to initialise: " + error);
            });
        };
        HeyzapController.prototype.showInterstitialAd = function () {
            HeyzapAds.InterstitialAd.show().then(function () {
            }, function (error) {
            });
        };
        HeyzapController.prototype.showVideoAd = function () {
            HeyzapAds.VideoAd.show().then(function () {
            }, function (error) {
                console.error("Native call wasn't successful: " + error);
            });
        };
        HeyzapController.prototype.showBannerAd = function (position) {
            var bannerPosition = (position === "bottom") ? HeyzapAds.BannerAd.POSITION_BOTTOM : HeyzapAds.BannerAd.POSITION_TOP;
            HeyzapAds.BannerAd.show(bannerPosition).then(function () {
            }, function (error) {
                console.error("Native call wasn't successful: " + error);
            });
        };
        HeyzapController.prototype.destroyBannerAd = function () {
            HeyzapAds.BannerAd.destroy().then(function () {
            }, function (error) {
                console.error("Native call wasn't successful: " + error);
            });
        };
        return HeyzapController;
    }());
    Fractal.HeyzapController = HeyzapController;
})(Fractal || (Fractal = {}));
var TheColourGame;
(function (TheColourGame) {
    TheColourGame.playButtons = $('.mode-play');
    TheColourGame.chooseShootdown = $('.mode-play-shootdown');
    TheColourGame.chooseTarget = $('.mode-play-target');
    TheColourGame.chooseCountdown = $('.mode-play-countdown');
    TheColourGame.chooseSurvival = $('.mode-play-survival');
    TheColourGame.lockButton = $('.mode-locked');
    TheColourGame.buyExtra = $('.buy-extra-mods-wrapper');
    var width;
    var height;
    if (height > 1280) {
        height = 1280;
        width = 1280 / (height / width);
    }
    TheColourGame._$mainMenuChose = $('.mainMenu');
    TheColourGame._$animatedButton = $('.button-animated');
    TheColourGame._$clickPreventor = $(".clickPreventor");
    TheColourGame._$chooseMode = $('.chooseMode');
    $('.backMenuButton').on("click", function () {
        TheColourGame._$clickPreventor.addClass("preventOn");
        TheColourGame._$mainMenuChose.addClass("blocked");
        setTimeout(function () {
            TheColourGame._$mainMenuChose.removeClass("moveToLeft");
            TheColourGame._$chooseMode.removeClass("slideToLeft");
            setTimeout(function () {
                TheColourGame._$animatedButton.removeClass("slideDown");
                TheColourGame._$clickPreventor.removeClass("preventOn");
            }, 900);
        }, 0);
    });
    TheColourGame.playButtons.on("click", function (e) {
        var modeName = $.grep(this.className.split(" "), function (v, i) {
            return v.indexOf('mode-play-') === 0;
        }).join();
        modeName = modeName.replace('mode-play-', '');
        if (!TheColourGame.storeCG.products[0].checkBought() && modeName === 'shootdown' ||
            !TheColourGame.storeCG.products[0].checkBought() && modeName === 'target') {
            TheColourGame.buyExtra.addClass('show-buy');
        }
        else {
            Fractal.Helpers.rotateUntilCallback(function () {
                TheColourGame.hideOverlays();
                width = window.innerWidth * ((devicePixelRatio / 2) + 0.5);
                height = window.innerHeight * ((devicePixelRatio / 2) + 0.5);
                if (height > 1280) {
                    height = 1280;
                    width = 1280 / (window.innerHeight / window.innerWidth);
                }
                TheColourGame.colourGame = new TheColourGame.ColourGame(width, height, modeName);
            });
        }
    });
    TheColourGame.lockButton.on("click", function () {
        TheColourGame.buyExtra.addClass('show-buy');
    });
    TheColourGame.buyExtra.find('.buy-extra-mods-button-buy').on('click', function () {
        if (navigator.connection.type !== 'none') {
            if (!TheColourGame.storeReady) {
                navigator.notification.alert('In-app purchases are not ready yet. Please try again after 5 seconds.', null, 'Buy Extra Modes and Remove Ads.', 'OK');
                TheColourGame.storeCG.load();
            }
            else {
                TheColourGame.storeCG.getProduct('com.fractalgames.thecolourgame.noads.extramodes').buy();
            }
        }
        else {
            navigator.notification.alert('Please check your internet connection and try again.', null, 'Buy Extra Modes and Remove Ads.', 'OK');
        }
    });
    TheColourGame.buyExtra.find('.buy-extra-mods-button-close').on('click', function () {
        TheColourGame.buyExtra.removeClass('show-buy');
    });
})(TheColourGame || (TheColourGame = {}));
var TheColourGame;
(function (TheColourGame) {
    document.addEventListener("deviceready", onDeviceReady, false);
    TheColourGame.storeCG = null;
    TheColourGame.storeReady = false;
    TheColourGame.accountHandler = null;
    TheColourGame.adCounter = 0;
    TheColourGame.randomAdShow = Math.floor(Math.random() * 2) + 2;
    var _$mainMenu = $('.mainMenu');
    var _$moreInfo = $('.moreInfo');
    var _$backInfoButton = $('.backInfoButton');
    var _$chooseMode = $('.chooseMode');
    var _$backMenuButton = $('.backMenuButton');
    var _$gameOver = $('.gameOver');
    var _$loginWrapperModal = $('.login-wrapper-modal');
    var _$accountWrapperModal = $('.account-wrapper-modal');
    var _$leaderboardWrapperModal = $('.leaderboard-wrapper-modal');
    var _backButtonFloodPrevention = true;
    var _$gameOverBackButton = $('.gameOverButtons').find('.back');
    function onBackButtonPressed() {
        if (_backButtonFloodPrevention) {
            _backButtonFloodPrevention = false;
            setTimeout(function () {
                _backButtonFloodPrevention = true;
            }, 1500);
            if (!_$loginWrapperModal.hasClass('not-visible')) {
                _$loginWrapperModal.find('.close-modal').click();
                return;
            }
            if (!_$accountWrapperModal.hasClass('not-visible')) {
                _$accountWrapperModal.find('.close-modal').click();
                return;
            }
            if (!_$leaderboardWrapperModal.hasClass('not-visible')) {
                _$leaderboardWrapperModal.find('.close-modal').click();
                return;
            }
            if (!_$mainMenu.hasClass('moveToLeft') && !_$mainMenu.hasClass('moveToRight')) {
                navigator.app.exitApp();
                return;
            }
            if (_$moreInfo.hasClass('slideToRight')) {
                _$backInfoButton.click();
                return;
            }
            if (_$chooseMode.hasClass('slideToLeft') && !_$chooseMode.hasClass('slideAway')) {
                _$backMenuButton.click();
                return;
            }
            if (_$chooseMode.hasClass('slideAway') && !_$gameOver.hasClass('blocked')) {
                TheColourGame.colourGame.state.getCurrentState().quitGame();
                return;
            }
            if (_$gameOver.hasClass('blocked')) {
                _$gameOverBackButton.click();
                return;
            }
        }
    }
    function onDeviceReady() {
        document.addEventListener("backbutton", onBackButtonPressed, false);
        TheColourGame.accountHandler = new Fractal.accountHandler();
        TheColourGame.heyzap = new Fractal.HeyzapController("523bd65c5584786ae70204da410980be");
        TheColourGame.storeCG = new Fractal.Store();
        for (var i = 0; i < Fractal.PRODUCTS.length; i++) {
            TheColourGame.storeCG.addProduct(Fractal.PRODUCTS[i].id, Fractal.PRODUCTS[i].alias, window.store.NON_CONSUMABLE);
        }
        TheColourGame.storeCG.load();
        navigator.splashScreen.hide();
        TheColourGame.splashScreen(function () {
            TheColourGame.startLogo(function () {
                setTimeout(function () {
                    var readyCheck = setInterval(function () {
                        if (document.readyState === 'complete') {
                            TheColourGame.startMainMenu(function () {
                                if (!TheColourGame.storeCG.products[0].checkBought()) {
                                    TheColourGame.heyzap.showInterstitialAd();
                                }
                                TheColourGame.accountHandler.user.showLoginOverlay();
                                TheColourGame.accountHandler.user.showLeaderboardOverlay();
                            });
                            clearInterval(readyCheck);
                        }
                    }, 100);
                }, 1000);
            });
        });
    }
})(TheColourGame || (TheColourGame = {}));
var TheColourGame;
(function (TheColourGame) {
    var POFG = (function () {
        function POFG(game) {
            this.game = game;
            this.fullDrawDelay = 1200;
            this.timerUpdateDelay = 40;
            this.fakeSprite = this.game.add.sprite(-30, -10);
            this.fakeSprite.alpha = 0;
            this.fakeSprite.width = 1;
            this.fakeSprite.height = 1;
            this.createFakeTween(this.fullDrawDelay);
        }
        POFG.prototype.update = function () {
            if (this.game.tweens.getAll().length > 0) {
                this.game.lockRender = false;
                this.tweensActive = true;
            }
            else {
                this.tweensActive = false;
            }
            if (this.game.input.activePointer.isDown) {
                this.game.lockRender = false;
            }
            else if (!this.tweensActive) {
                this.game.lockRender = true;
            }
        };
        POFG.prototype.destroy = function () {
            this.fullDrawDelay = null;
            this.timerUpdateDelay = null;
            this.tweensActive = null;
            this.fakeSprite.destroy();
            this.fakeSprite = null;
        };
        ;
        POFG.prototype.forceUnlock = function (isChangingOrientation) {
            if (!isChangingOrientation) {
                this.game.lockRender = false;
                this.createFakeTween(this.timerUpdateDelay);
            }
            else {
                this.createFakeTween(this.fullDrawDelay);
            }
        };
        POFG.prototype.createFakeTween = function (tweenDuration) {
            this.game.add.tween(this.fakeSprite).to({
                x: this.fakeSprite.x - 10,
                y: this.fakeSprite.y - 10
            }, tweenDuration, Phaser.Easing.Default, true, 0, 0, false);
        };
        return POFG;
    }());
    TheColourGame.POFG = POFG;
})(TheColourGame || (TheColourGame = {}));
var TheColourGame;
(function (TheColourGame) {
    var Play = (function (_super) {
        __extends(Play, _super);
        function Play() {
            _super.apply(this, arguments);
        }
        Play.prototype.init = function (gameMode) {
            this.gameMode = gameMode;
        };
        Play.prototype.preload = function () {
            this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.game.stage.disableVisibilityChange = true;
            this.game.time.advancedTiming = false;
            this.game.forceSingleUpdate = true;
            this.game.load.image("gameLogoSprite", "Assets/ingameSprites/gameLogoSprite.png");
            this.game.load.image("refreshSprite", "Assets/ingameSprites/refreshSprite.png");
            this.game.load.image("shadowSprite", "Assets/ingameSprites/shadowSprite.png");
            this.game.load.image("tickSprite", "Assets/ingameSprites/tickSprite.png");
            this.game.load.image("timeSprite", "Assets/ingameSprites/timeSprite.png");
            this.game.load.image("xSprite", "Assets/ingameSprites/xSprite.png");
        };
        ;
        Play.prototype.create = function () {
            var _this = this;
            this.POFG = new TheColourGame.POFG(this.game);
            this.orientationChangeLocked = true;
            this.game.time.events.add(TheColourGame.ORIENTATION_CHANGE_MINIMAL_INTERVAL, function () {
                _this.orientationChangeLocked = false;
            });
            if (window.innerHeight > window.innerWidth) {
                this.orientation = TheColourGame.ORIENTATIONS.PORTRAIT;
            }
            else {
                this.orientation = TheColourGame.ORIENTATIONS.LANDSCAPE;
            }
            this.timePassed = 0;
            this.board = new TheColourGame.Board(this.game);
            this.loadGameObjects();
            this.UI = new TheColourGame.UI(this.game, this.timer, this.pointsTrackerStartingValue, this.board.boardImage.width, this.gameMode, this);
            this.levelIsRepeating = false;
            this.addInputSprites();
            window.onresize = function () {
                _this.game.paused = false;
                if (!_this.orientationChangeLocked) {
                    _this.orientationChangeLocked = true;
                    _this.game.time.events.add(TheColourGame.ORIENTATION_CHANGE_MINIMAL_INTERVAL, function () {
                        _this.orientationChangeLocked = false;
                    });
                    _this.game.time.events.add(50, function () {
                        if ((window.innerHeight > window.innerWidth && _this.orientation === TheColourGame.ORIENTATIONS.LANDSCAPE) || (window.innerHeight < window.innerWidth && _this.orientation === TheColourGame.ORIENTATIONS.PORTRAIT)) {
                            _this.changeOrientation();
                        }
                    });
                }
            };
            this.game.showContainer();
        };
        ;
        Play.prototype.update = function () {
            this.POFG.update();
        };
        Play.prototype.destroyState = function () {
            this.board.destroy();
            this.UI.destroy();
            this.score = null;
            this.level = null;
            this.timer = null;
            if (this.timersUpdater) {
                this.timersUpdater.destroy();
                this.timersUpdater = null;
            }
            this.correctInputSprite.destroy();
            this.correctInputSprite = null;
            this.inputSprite.destroy();
            this.inputSprite = null;
            this.levelIsRepeating = null;
            this.target = null;
            this.pointsTrackerStartingValue = null;
            this.progressOnTarget = null;
            this.pointsTrackerStartingValue = null;
            this.wrongClickCounter = null;
            this.gameMode = null;
            window.onresize = function () {
            };
            window.onresize = null;
            this.timePassed = null;
            this.POFG.destroy();
            this.POFG = null;
        };
        ;
        Play.prototype.restartGame = function () {
            var stateToStart = this.gameMode;
            this.destroyState();
            this.game.paused = false;
            this.game.state.start(this.game.state.current, true, false, stateToStart);
        };
        ;
        Play.prototype.sendHighScores = function () {
            if (navigator.connection.type !== 'none') {
                var modes = ['highScoreSurvival', 'highScoreShootdown', 'highScoreTarget', 'highScoreCountdown'];
                for (var i = 0; i < modes.length; i++) {
                    var modeScore = localStorage.getItem(modes[i]);
                    if (modeScore) {
                        var modeIdentifier = modes[i].replace('highScore', '');
                        modeIdentifier = modeIdentifier.charAt(0).toLowerCase() + modeIdentifier.slice(1);
                        TheColourGame.accountHandler.serviceHandler.sendHighScore(modeScore.toString(), modeIdentifier, function (r) {
                        }, function (r) {
                            console.error(r.responseJSON.detail);
                        });
                    }
                }
            }
        };
        Play.prototype.quitGame = function () {
            var self = this;
            this.game.paused = true;
            this.game.time.removeAll();
            var currentMode = this.gameMode.charAt(0).toUpperCase() + this.gameMode.slice(1);
            var currentHighScore = parseInt(localStorage.getItem("highScore" + currentMode));
            var score = 0;
            var _isHighScoreMade = false;
            if (TheColourGame.adCounter === TheColourGame.randomAdShow) {
                TheColourGame.adCounter = 0;
                TheColourGame.randomAdShow = Math.floor(Math.random() * 2) + 1;
                if (!TheColourGame.storeCG.products[0].checkBought()) {
                    TheColourGame.heyzap.showInterstitialAd();
                }
            }
            else {
                TheColourGame.adCounter++;
            }
            if (!currentHighScore || this.score > currentHighScore) {
                localStorage.setItem("highScore" + currentMode, this.score.toString());
                _isHighScoreMade = true;
            }
            if (TheColourGame.accountHandler) {
                if (!TheColourGame.accountHandler.user.anonymous) {
                    var previousScore = parseInt(localStorage.getItem('previousScore'));
                    if (isNaN(previousScore)) {
                        previousScore = 0;
                    }
                    score = previousScore + self.score;
                    TheColourGame.accountHandler.user.updateIQ(score);
                    if (navigator.connection.type !== 'none') {
                        if (_isHighScoreMade) {
                            this.sendHighScores();
                        }
                        TheColourGame.accountHandler.serviceHandler.sendScore(score.toString(), function (r) {
                        }, function (r) {
                        });
                    }
                    else {
                        localStorage.setItem('previousScore', score.toString());
                    }
                }
            }
            $('.gameOver').addClass("blocked");
            $('.gameOverLayout').addClass("blocked");
            $('.pointsNumber').html(this.score.toString());
            $('.timerNumber').html(Math.round(this.timePassed).toString());
            $('.wrongNumber').html(this.wrongClickCounter.toString());
            $('.iqPoints').html(this.score.toString());
            $('.back').off().on("click", function () {
                $('.gameOver').removeClass("blocked");
                self.game.hideContainer(function () {
                    $('.gameOverLayout').removeClass("blocked");
                    self.destroyState();
                });
            });
            $('.leaderboard').off().on("click", function () {
                TheColourGame.accountHandler.user.leaderboardModalRequest();
            });
            $(".again").off().on("click", function () {
                $('.gameOver').removeClass("blocked");
                $('.gameOverLayout').removeClass("blocked");
                self.restartGame();
            });
            $(".share").off().on('click', function () {
                window.plugins.socialsharing.share("I scored " + self.score + " points playing The Colour Game. Try and beat me!", "Fractal Games The Colour Game", null, 'www.fractalgames.com');
            });
        };
        ;
        Play.prototype.addInputSprites = function () {
            var _this = this;
            this.inputSprite = this.game.add.sprite(this.board.boardImage.x, this.board.boardImage.y);
            this.inputSprite.width = this.board.boardImage.width;
            this.inputSprite.height = this.board.boardImage.height;
            this.inputSprite.anchor.set(0.5);
            this.inputSprite.alpha = 0;
            this.inputSprite.inputEnabled = true;
            this.inputSprite.input.priorityID = 0;
            this.inputSprite.events.onInputDown.add(function () {
                _this.handleWrongInput();
            });
            this.correctInputSprite = this.game.add.sprite(this.board.chosenTilePosition.x, this.board.chosenTilePosition.y);
            this.correctInputSprite.width = this.board.tileSideLength;
            this.correctInputSprite.height = this.board.tileSideLength;
            this.correctInputSprite.anchor.set(0.5);
            this.correctInputSprite.alpha = 0;
            this.correctInputSprite.inputEnabled = true;
            this.correctInputSprite.input.priorityID = 1;
            this.correctInputSprite.events.onInputDown.add(function () {
                _this.changeLevel();
            });
        };
        ;
        Play.prototype.handleWrongInput = function () {
            if (!this.timersUpdater) {
                this.createCountDownTimer();
            }
            this.wrongClickCounter++;
            if (this.gameMode === "shootdown") {
                this.restartGame();
            }
            else if (this.gameMode === "survival") {
                this.timer -= 2;
                this.UI.playPenaltyAnimation();
                if (this.timer <= 0) {
                    this.timer = 0;
                    this.quitGame();
                    return;
                }
                this.UI.updateTimerText(this.timer, this.gameMode);
            }
        };
        ;
        Play.prototype.changeLevel = function () {
            if (!this.timersUpdater) {
                this.createCountDownTimer();
            }
            if (this.level >= TheColourGame.HIGHEST_LEVEL_INDEX) {
                this.level++;
                this.updateGameObjects(true);
            }
            else {
                if (TheColourGame.LEVELS_TO_REPEAT.indexOf(this.level) === -1) {
                    this.level++;
                    this.board.resize(this.board.tilesPerSide + 1);
                    this.updateGameObjects(true);
                }
                else {
                    if (this.levelIsRepeating) {
                        this.levelIsRepeating = false;
                        this.level++;
                        this.board.resize(this.board.tilesPerSide + 1);
                        this.updateGameObjects(true);
                    }
                    else {
                        this.levelIsRepeating = true;
                        this.updateGameObjects(false);
                    }
                }
            }
        };
        ;
        Play.prototype.adjustInputSprite = function () {
            if (this.inputSprite.x !== this.board.boardImage.x || this.inputSprite.y !== this.board.boardImage.y) {
                this.inputSprite.x = this.board.boardImage.x;
                this.inputSprite.y = this.board.boardImage.y;
            }
            this.correctInputSprite.x = this.board.chosenTilePosition.x;
            this.correctInputSprite.y = this.board.chosenTilePosition.y;
            if (this.correctInputSprite.height > this.board.tileSideLength) {
                this.correctInputSprite.height = this.board.tileSideLength;
                this.correctInputSprite.width = this.board.tileSideLength;
            }
        };
        ;
        Play.prototype.updateTimers = function () {
            this.POFG.forceUnlock(false);
            if (this.gameMode !== "survival") {
                this.timer--;
                this.timePassed++;
            }
            else {
                this.timer -= 0.1;
                this.timePassed += 0.1;
            }
            if (this.timer <= 0) {
                this.timer = 0;
                this.UI.updateTimerText(this.timer, this.gameMode);
                this.inputSprite.inputEnabled = false;
                this.correctInputSprite.inputEnabled = false;
                this.quitGame();
                return;
            }
            else {
                this.UI.updateTimerText(this.timer, this.gameMode);
            }
        };
        ;
        Play.prototype.updateGameObjects = function (shouldUpdateChosenTileAlpha) {
            this.board.drawTiles(shouldUpdateChosenTileAlpha, false);
            this.adjustInputSprite();
            this.score++;
            if (this.gameMode === "countdown") {
                this.UI.updatePointsTracker(this.score);
            }
            else if (this.gameMode === "shootdown") {
                this.progressOnTarget--;
                this.UI.updatePointsTracker(this.progressOnTarget);
                if (this.progressOnTarget === 0) {
                    if (!localStorage.getItem(TheColourGame.SHOOTDOWN_STORAGE_TARGET_KEY)) {
                        localStorage.setItem(TheColourGame.SHOOTDOWN_STORAGE_TARGET_KEY, (TheColourGame.SHOOTDOWN_DEFAULT_TARGET + 1).toString());
                    }
                    else {
                        localStorage.setItem(TheColourGame.SHOOTDOWN_STORAGE_TARGET_KEY, (parseInt(localStorage.getItem(TheColourGame.SHOOTDOWN_STORAGE_TARGET_KEY)) + 1).toString());
                    }
                    this.quitGame();
                    return;
                }
                var progress = (this.target - this.progressOnTarget) / this.target;
                if (progress <= 0.2) {
                    this.timer = 5;
                }
                else if (progress > 0.2 && progress <= 0.4) {
                    this.timer = 4;
                }
                else {
                    this.timer = 3;
                }
                this.UI.updateTimerText(this.timer, this.gameMode);
                this.refreshTimer();
            }
            else if (this.gameMode === "target") {
                this.progressOnTarget--;
                this.UI.updatePointsTracker(this.progressOnTarget);
                if (this.progressOnTarget === 0) {
                    if (!localStorage.getItem(TheColourGame.TARGET_STORAGE_TARGET_KEY)) {
                        localStorage.setItem(TheColourGame.TARGET_STORAGE_TARGET_KEY, (TheColourGame.TARGET_DEFAULT_TARGET + 1).toString());
                    }
                    else {
                        localStorage.setItem(TheColourGame.TARGET_STORAGE_TARGET_KEY, (parseInt(localStorage.getItem(TheColourGame.TARGET_STORAGE_TARGET_KEY)) + 1).toString());
                    }
                    this.quitGame();
                    return;
                }
            }
            else if (this.gameMode === "survival") {
                if (this.score <= 10) {
                    this.timer += 1.5;
                    this.UI.playAddBonusAnimation(1.5);
                }
                else if (this.score > 10 && this.score <= 20) {
                    this.timer += 1;
                    this.UI.playAddBonusAnimation(1);
                }
                else if (this.score > 20 && this.score <= 30) {
                    this.timer += 0.7;
                    this.UI.playAddBonusAnimation(0.7);
                }
                else {
                    this.timer += 0.5;
                    this.UI.playAddBonusAnimation(0.5);
                }
                this.refreshTimer();
                this.UI.updateTimerText(this.timer, this.gameMode);
                this.UI.updatePointsTracker(this.score);
            }
        };
        Play.prototype.changeOrientation = function () {
            if (this.orientation === TheColourGame.ORIENTATIONS.LANDSCAPE) {
                this.orientation = TheColourGame.ORIENTATIONS.PORTRAIT;
            }
            else {
                this.orientation = TheColourGame.ORIENTATIONS.LANDSCAPE;
            }
            this.POFG.forceUnlock(true);
            var newWidth = this.game.height;
            var newHeight = this.game.width;
            this.game.scale.setGameSize(newWidth, newHeight);
            this.board.redrawWithNewOrientation();
            this.UI.redrawWithNewOrientation(this.timer, this.score, this.board.boardImage.width, this.gameMode, this);
            this.adjustInputSprite();
        };
        ;
        Play.prototype.loadGameObjects = function () {
            this.score = 0;
            this.level = 0;
            this.wrongClickCounter = 0;
            if (this.gameMode === "countdown") {
                this.timer = TheColourGame.COUNTDOWN_STARTING_TIMER;
                this.pointsTrackerStartingValue = this.score;
            }
            else if (this.gameMode === "shootdown") {
                this.timer = TheColourGame.SHOOTDOWN_STARTING_TIMER;
                this.target = parseInt(localStorage.getItem(TheColourGame.SHOOTDOWN_STORAGE_TARGET_KEY)) || TheColourGame.SHOOTDOWN_DEFAULT_TARGET;
                this.progressOnTarget = this.target;
                this.pointsTrackerStartingValue = this.target;
            }
            else if (this.gameMode === "target") {
                this.target = parseInt(localStorage.getItem(TheColourGame.TARGET_STORAGE_TARGET_KEY)) || TheColourGame.TARGET_DEFAULT_TARGET;
                this.timer = Math.ceil(this.target / TheColourGame.TARGET_TIME_RATIO_TO_TARGET);
                this.progressOnTarget = this.target;
                this.pointsTrackerStartingValue = this.target;
            }
            else if (this.gameMode === "survival") {
                this.timer = TheColourGame.SURVIVAL_DEFAULT_TIMER;
                this.pointsTrackerStartingValue = this.score;
            }
        };
        ;
        Play.prototype.createCountDownTimer = function () {
            var _this = this;
            var delay = 0;
            if (this.gameMode !== "survival") {
                delay = Phaser.Timer.SECOND;
            }
            else {
                delay = Phaser.Timer.SECOND * 0.1;
            }
            this.timersUpdater = this.game.time.create();
            this.timersUpdater.loop(delay, function () {
                _this.updateTimers();
            }, null, null);
            this.timersUpdater.start();
        };
        ;
        Play.prototype.refreshTimer = function () {
            var _this = this;
            var delay = 0;
            if (this.gameMode !== "survival") {
                delay = Phaser.Timer.SECOND;
            }
            else {
                delay = 100;
            }
            this.timersUpdater.destroy();
            this.timersUpdater = this.game.time.create();
            this.timersUpdater.loop(delay, function () {
                _this.updateTimers();
            }, null, null);
            this.timersUpdater.start();
        };
        ;
        return Play;
    }(Phaser.State));
    TheColourGame.Play = Play;
})(TheColourGame || (TheColourGame = {}));
var TheColourGame;
(function (TheColourGame) {
    var ColourGame = (function (_super) {
        __extends(ColourGame, _super);
        function ColourGame(width, height, name) {
            _super.call(this, width, height, Phaser.AUTO, "game", true, true);
            this.state.add("Play", TheColourGame.Play, false);
            this.state.start("Play", true, true, name);
        }
        ColourGame.prototype.showContainer = function () {
            var game = $(".game");
            game.addClass("blocked");
            $('.clickPreventorModes').addClass("preventOn");
            $(".chooseMode").addClass("slideAway");
            game.addClass("slideIn");
            Fractal.Helpers.removeLoader();
            setTimeout(function () {
                $(".color-game").removeClass("blocked");
                $('.clickPreventorModes').removeClass("preventOn");
            }, 1000);
        };
        ColourGame.prototype.hideContainer = function (callback) {
            var game = this;
            TheColourGame.setHighScoresPlay();
            $(".clickPreventor").addClass("preventOn");
            $(".color-game").addClass("blocked");
            setTimeout(function () {
                $(".chooseMode").removeClass("slideAway");
                $(".game").removeClass("slideIn");
                TheColourGame.showOverlays();
                setTimeout(function () {
                    $('.clickPreventor').removeClass("preventOn");
                    callback();
                    game.destroy();
                    Phaser.GAMES = [];
                }, 1000);
            }, 1000);
        };
        return ColourGame;
    }(Phaser.Game));
    TheColourGame.ColourGame = ColourGame;
})(TheColourGame || (TheColourGame = {}));
//# sourceMappingURL=ColourGame.js.map