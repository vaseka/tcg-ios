///<reference path="Game/Objects/Board.ts"/>
///<reference path="Lib/Typings/phaser/phaser.d.ts"/>
///<reference path="Game/States/Play.ts"/>
///<reference path="Lib/Typings/jquery/jquery.d.ts"/>
///<reference path="Lib/Custom/moreInfo.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TheColourGame;
(function (TheColourGame) {
    var ColourGame = (function (_super) {
        __extends(ColourGame, _super);
        function ColourGame(width, height, name) {
            _super.call(this, width, height, Phaser.AUTO, "game", true, true);
            this.state.add("Play", TheColourGame.Play, false);
            this.state.start("Play", true, true, name);
        }
        ColourGame.prototype.showContainer = function () {
            var game = $(".game");
            game.addClass("blocked");
            $('.clickPreventorModes').addClass("preventOn");
            $(".chooseMode").addClass("slideAway");
            game.addClass("slideIn");
            Fractal.Helpers.removeLoader();
            setTimeout(function () {
                $(".color-game").removeClass("blocked");
                $('.clickPreventorModes').removeClass("preventOn");
            }, 1000);
        };
        ColourGame.prototype.hideContainer = function (callback) {
            var game = this;
            TheColourGame.setHighScoresPlay();
            $(".clickPreventor").addClass("preventOn");
            $(".color-game").addClass("blocked");
            setTimeout(function () {
                $(".chooseMode").removeClass("slideAway");
                $(".game").removeClass("slideIn");
                TheColourGame.showOverlays();
                setTimeout(function () {
                    $('.clickPreventor').removeClass("preventOn");
                    callback();
                    // game.cache.destroy();
                    // game.world.destroy(true);
                    game.destroy();
                    Phaser.GAMES = [];
                }, 1000);
            }, 1000);
        };
        return ColourGame;
    }(Phaser.Game));
    TheColourGame.ColourGame = ColourGame;
})(TheColourGame || (TheColourGame = {}));
