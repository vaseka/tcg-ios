/**
 *
 * Created by Ivaylo Hristov on 10.2.2016 г..
 */
///<reference path='./../Lib/typings/jquery/jquery.d.ts'/>

module Fractal {
    export class Helpers {

        static $helperContainer = $('.helpers');
        static keys = {37: 1, 38: 1, 39: 1, 40: 1};
// Get parameters from the url
        static getQueryParams(qs) {
            qs = qs.split('+').join(' ');

            let params = {};
            let tokens;
            let re = /[?&]?([^=]+)=([^&]*)/g;

            while (tokens = re.exec(qs)) {
                params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
            }

            return params;
        }

// Rotate the cube on the fractal games loader until callback is fired ( you need to clearInterval the cubeInterval )
        static rotateUntilCallback(callback) {
            Helpers.$helperContainer.addClass('show-loader');
            callback();
        }

        static removeLoader() {
            Helpers.$helperContainer.removeClass('show-loader');
        }

// Overlay hack for forbidding clicking
        static disableClicking() {
            $('.helpers').addClass('prevent-clicking');
            //let el = document.getElementsByClassName('helpers')[0];
            //el.className = el.className + ' prevent-clicking';
        }

// Enable clicking
        static enableClicking() {
            $('.helpers').removeClass('prevent-clicking');
            //let el = document.getElementsByClassName('helpers')[0];
            //el.className = 'helpers';
        }

        static disallowScrolling() {
            $('html').addClass('noscroll');
            //let el = document.getElementsByTagName('html')[0];
            //$('html').addClass('noscroll');
        }

        static enableScrolling() {
            $('html').removeClass('noscroll');
        }

// Detect if browser or mobile
        mobileCheck() {
            let check = false;
            (function (a) {
                if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))check = true;
            })(navigator.userAgent || navigator.vendor || (<any>window).opera);
            return check;
        }

        static abbreviateNumber(value) {
            let newValue = value;
            if (value >= 9999) {
                let suffixes = ['', 'K', 'M', 'B', 'T'];
                let suffixNum = Math.floor(('' + value).length / 3);
                let shortValue = '';
                for (let precision = 2; precision >= 1; precision--) {
                    shortValue = parseFloat((suffixNum !== 0 ? (value / Math.pow(1000, suffixNum)) : value).toPrecision(precision)).toString();
                    let dotLessShortValue = (shortValue + '').replace(/[^a-zA-Z 0-9]+/g, '');
                    if (dotLessShortValue.length <= 2) {
                        break;
                    }
                }
                if ((<any>shortValue) % 1 !== 0) {
                    let shortNum = (<any>shortValue).toFixed(1);
                }
                newValue = shortValue + suffixes[suffixNum];
            }
            return newValue;
        }

// Convert text to slugtext
        static convertToSlug(Text) {
            return Text
                .toLowerCase()
                .replace(/[^\w ]+/g, '')
                .replace(/ +/g, '-')
                ;
        }

        static iq_box_progress(iq, level) {
            let progress;
            progress = (iq / (level / 87) + 3);
            $('.bar-iq-box').css('left', progress + '%');
            $('.under-box-centered-iq').html(Helpers.abbreviateNumber(iq));
        }

        static iq_box_progress_account(iq, level) {
            let progress;
            progress = (iq / (level / 95));
            $('.bar-iq-box').css('left', progress + '%');
            $('.under-box-centered-iq').html(Helpers.abbreviateNumber(iq));
        }

        static delayExecution(time, callback) {
            return setTimeout(function () {
                callback();
            }, time);
        }

        static showLoadingButton($button, loading) {
            if (loading) {
                $button.find('.loading-rect').addClass('show-loading');
            }
            else {
                $button.find('.loading-rect').removeClass('show-loading');
            }
        }


        static preventDefault(e) {
            e = e || window.event;
            if (e.preventDefault)
                e.preventDefault();
            e.returnValue = false;
        }

        static preventDefaultForScrollKeys(e) {
            if (Helpers.keys[e.keyCode]) {
                Helpers.preventDefault(e);
                return false;
            }
        }

        static disableScroll() {
            if (window.addEventListener) // older FF
                window.addEventListener('DOMMouseScroll', Helpers.preventDefault, false);
            window.onwheel = Helpers.preventDefault; // modern standard
            window.onmousewheel = document.onmousewheel = Helpers.preventDefault; // older browsers, IE
            window.ontouchmove = Helpers.preventDefault; // mobile
            document.onkeydown = Helpers.preventDefaultForScrollKeys;
        }

        static enableScroll() {
            if (window.removeEventListener)
                window.removeEventListener('DOMMouseScroll', Helpers.preventDefault, false);
            window.onmousewheel = document.onmousewheel = null;
            window.onwheel = null;
            window.ontouchmove = null;
            document.onkeydown = null;
        }


        static validateEmail(email) {
            let re = /^(([^<>()[\]\\.,;:\s@']+(\.[^<>()[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        static hasWhiteSpace(string) {
            return string.indexOf(' ') >= 0;
        }


        static displayLoaderDots($parentContainer) {
            let loader = $parentContainer.find('.loading-footer');
            loader.addClass('show-loader');
        }

        static hideLoaderDots($parentContainer) {
            let loader = $parentContainer.find('.loading-footer');
            loader.removeClass('show-loader');
        }

        static detectIE() {
            let ua = window.navigator.userAgent;

            let msie = ua.indexOf('MSIE ');
            if (msie > 0) {
                // IE 10 or older => return version number
                return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            }

            let trident = ua.indexOf('Trident/');
            if (trident > 0) {
                // IE 11 => return version number
                let rv = ua.indexOf('rv:');
                return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
            }

            let edge = ua.indexOf('Edge/');
            if (edge > 0) {
                // Edge (IE 12+) => return version number
                return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
            }

            // other browser
            return 0;
        }

        static isInt(value) {
            return !isNaN(value) &&
                parseInt((<any>Number(value))) == value && !isNaN(parseInt(value, 10));
        }

        static transitionEndEventName() {
            var i,
                undefined,
                el = document.createElement('div'),
                transitions = {
                    'transition': 'transitionend',
                    'OTransition': 'otransitionend',  // oTransitionEnd in very old Opera
                    'MozTransition': 'transitionend',
                    'WebkitTransition': 'webkitTransitionEnd'
                };

            for (i in transitions) {
                if (transitions.hasOwnProperty(i) && el.style[i] !== undefined) {
                    return transitions[i];
                }
            }
        }


        static showPopup(text) {
            // var $commentContainer = $('.login-overlay-message');
            // var $commentContainerInner = $commentContainer.find('.login-overlay-popup-message-text-inner');
            // $commentContainerInner.html(text);
            // $commentContainer.addClass('show-popup');
            (<any>navigator).notification.alert(text, null, 'Fractal Games', 'OK');
        }

        //$('html').on('mousedown',function(ev) {if (ev.which === 2) {ev.preventDefault();return false}});
        //$('input').on('keypress',function(e) { if (e.which === 32) {e.preventDefault();return false;}});

    }
}
