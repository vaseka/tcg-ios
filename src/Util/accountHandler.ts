///<reference path="Service/requestsHandler.ts"/>
///<reference path="../Lib/Custom/mainReady.ts"/>

module Fractal {
    export class accountHandler {
        loginCompiler:Fractal.TemplateCompilerController;
        accountCompiler:Fractal.TemplateCompilerController;
        leaderboardGlobalCompiler:Fractal.TemplateCompilerController;
        leaderboardFriendsCompiler:Fractal.TemplateCompilerController;
        serviceHandler:Fractal.RequestsHandler;
        user:Fractal.User;

        constructor() {
            this.serviceHandler = new Fractal.RequestsHandler();
            this.serviceHandler.initalize();

            Fractal.TemplateCompilerController.registerHelpers();

            this.loginCompiler = new Fractal.TemplateCompilerController($('.login-overlay'), $('#hb-template-login'));
            this.accountCompiler = new Fractal.TemplateCompilerController($('.account-modal'), $('#hb-template-account'));
            this.leaderboardGlobalCompiler = new Fractal.TemplateCompilerController($('.leaderboard-modal-globals-users'), $('#hb-template-globals-leaderboard'));
            this.leaderboardFriendsCompiler = new Fractal.TemplateCompilerController($('.leaderboard-modal-friends-users'), $('#hb-template-friends-leaderboard'));
            if ((<any>navigator).connection.type !== 'none') {
                this.serviceHandler.shortAccountInfo((r)=> {
                    // console.log(r);
                    this.user = this.registerUser(r.user);
                    this.user.userInit();
                    this.user.getHighScores();
                    // console.log(this.user);
                }, (r)=> {
                    localStorage.removeItem('userCred');
                    localStorage.removeItem('userInfo');
                    // this.serviceHandler.removeToken();
                    this.user = this.registerAnonymous();
                    this.user.userInit();
                    // console.log(this.user);
                });
            }
            else {
                debugger;
                let user = Fractal.User.getSavedInfo();
                if (user) {
                    this.user = this.registerUser(user);
                    this.user.userInit();
                }
                else {
                    localStorage.removeItem('userCred');
                    localStorage.removeItem('userInfo');
                    // this.serviceHandler.removeToken();
                    this.user = this.registerAnonymous();
                    this.user.userInit();
                }
            }
        }

        registerUser(user) {
            return new Fractal.User(false, this.serviceHandler, this.loginCompiler, this.accountCompiler,
                user.username, user.user_avatar_url, user.total_iq, user.level, user.prev_level_iq,
                user.prev_level_name, user.next_level_iq, user.next_level_name);
        }

        registerAnonymous() {
            return new Fractal.User(true, this.serviceHandler, this.loginCompiler, this.accountCompiler,
                null, null, 0, "Kindergarten", 0, null, 1000, "Pre-school");
        }
    }
}