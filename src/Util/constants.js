// const MAIN_URL = 'http://10.7.3.150:8000';
var Fractal;
(function (Fractal) {
    Fractal.MAIN_URL = 'http://api.fractalgames.com';
    // export const MAIN_URL = 'http://10.7.3.150:8000';
    Fractal.LOGIN_URL = '/api/user/login/';
    Fractal.SHORT_ACCOUNT_INFO_URL = '/api/user/shortaccountinfo/';
    Fractal.FACEBOOK_URL = '/api/user/facebook/';
    Fractal.GOOGLE_URL = '/api/user/google/';
    Fractal.TWITTER_LOGIN_URL = '/api/user/twitter/';
    Fractal.TWITTER_REGISTER_URL = '/api/user/twitterregister/';
    Fractal.POST_SCORE = "/api/game/postscore/";
    Fractal.POST_HIGH_SCORE = "/api/game/posthighscore/";
    Fractal.GET_HIGH_SCORES = "/api/game/gethighscores/";
    Fractal.GLOBAL_HIGH_SCORES = "/api/game/specificglobalsleaderboard/";
    Fractal.FRIENDS_HIGH_SCORES = "/api/game/specificfriendsleaderboard/";
    Fractal.USER_INFO = '/api/user/userinfo/';
    Fractal.CHANGE_INFO = '/api/user/changeinfo/';
    Fractal.CHECK_USERNAME_URL = '/api/user/checkusername/';
    Fractal.CHECK_EMAIL_URL = '/api/user/checkemail/';
    Fractal.REGISTRATION_URL = '/api/user/registration/';
    Fractal.FORGOTTEN_PASSWORD_URL = '/api/user/forgotten-password/';
    Fractal.CHECK_GAME_BOUGHT_URL = '/api/user/checkgamebought/';
    Fractal.BUY_GAME_URL = '/api/user/buypro/';
    Fractal.waitTime = 500;
    Fractal.cubeTransitionTime = 1600;
    Fractal.fractalEndTransitionTime = 600;
    Fractal.logoTransitionTime = 1300;
    Fractal.PRODUCTS = [
        {
            id: "com.fractalgames.thecolourgame.noads.extramodes",
            alias: "noadsextramodes",
            type: "non consumable"
        }
    ];
})(Fractal || (Fractal = {}));
var TheColourGame;
(function (TheColourGame) {
    (function (ORIENTATIONS) {
        ORIENTATIONS[ORIENTATIONS["PORTRAIT"] = 0] = "PORTRAIT";
        ORIENTATIONS[ORIENTATIONS["LANDSCAPE"] = 1] = "LANDSCAPE";
    })(TheColourGame.ORIENTATIONS || (TheColourGame.ORIENTATIONS = {}));
    var ORIENTATIONS = TheColourGame.ORIENTATIONS;
    TheColourGame.BOARD_STARTING_TILES = 2;
    TheColourGame.BOARD_PORTRAIT_HEIGHT_MULTIPLIER = 0.6; //1025 / 1920;
    TheColourGame.BOARD_PORTRAIT_LINE_WIDTH_MULTIPLIER = 20 / 1080;
    TheColourGame.MIN_RANDOM_COLOR = 20;
    TheColourGame.MAX_RANDOM_COLOR = 230;
    TheColourGame.HIGHEST_LEVEL_INDEX = 6;
    TheColourGame.LEVELS_TO_REPEAT = [3, 4, 5];
    TheColourGame.CHOSEN_TILE_MIN_ALPHA = 60;
    TheColourGame.CHOSEN_TILE_MAX_ALPHA = 82;
    TheColourGame.UI_PORTRAIT_NUMBERS_FONT_SIZE_MULTIPLIER = 0.06;
    TheColourGame.UI_LANDSCAPE_NUMBERS_FONT_SIZE_MULTIPLIER = 0.09;
    TheColourGame.UI_ELEMENTS_VERTICAL_OFFSET_MULTIPLIER = 1.5;
    TheColourGame.TABLET_RESOLUTION_RATIO_THRESHOLD = 0.65;
    TheColourGame.UI_PHONE_NUMBERS_ADDITIONAL_OFFSET_MULTIPLIER = 1.1;
    TheColourGame.UI_QUIT_REFRESH_Y_OFFSET_MULTIPLIER = 0.125;
    TheColourGame.UI_LANDSCAPE_RIGHT_ELEMENTS_VOFFSET_MULTIPLIER = 0.15;
    TheColourGame.UI_LANDSCAPE_TABLET_LOGO_REDUCTION_MULTIPLIER = 0.7;
    TheColourGame.UI_LANDSCAPE_TABLET_MODE_TEXT_REDUCTION_MULTIPLIER = 0.5;
    TheColourGame.UI_SURVIVAL_TABLET_TIME_OFFSET_MULTIPLIER = 0.02;
    TheColourGame.UI_DECIMAL_TIME_FONT_SIZE_REDUCTION_MULTIPLIER = 0.5;
    TheColourGame.ORIENTATION_CHANGE_MINIMAL_INTERVAL = 100;
    TheColourGame.COUNTDOWN_STARTING_TIMER = 60;
    TheColourGame.SHOOTDOWN_STARTING_TIMER = 5;
    TheColourGame.SHOOTDOWN_DEFAULT_TARGET = 40;
    TheColourGame.SHOOTDOWN_STORAGE_TARGET_KEY = "shootdownTarget";
    TheColourGame.TARGET_DEFAULT_TARGET = 40;
    TheColourGame.TARGET_STORAGE_TARGET_KEY = "targetTarget";
    TheColourGame.TARGET_TIME_RATIO_TO_TARGET = 1.35;
    TheColourGame.SURVIVAL_DEFAULT_TIMER = 10;
    TheColourGame.SURVIVAL_BONUS_PENALTY_TWEENS_DURATION = 250;
    TheColourGame.SURVIVAL_DECIMAL_NUMBER_VOFFSET_MULTIPLIER = 0.4;
    TheColourGame.SURVIVAL_BONUS_PENALTY_FONT_SIZE_MULTIPLIER = 0.5;
    TheColourGame.SURVIVAL_PORTRAIT_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER = 0.04;
    TheColourGame.SURVIVAL_LANDSCAPE_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER = 0.06;
    TheColourGame.SURVIVAL_LANDSCAPE_TABLET_FLOATING_NUMBERS_TWEEN_DISTANCE_REDUCTION_MULTIPLIER = 0.02;
    TheColourGame.PACKAGE_NAME = "com.fractalgames.thecolourgame";
    TheColourGame.GAME_IDENTIFIER = "the-colour-game";
})(TheColourGame || (TheColourGame = {}));
