///<reference path='../Lib/typings/jquery/jquery.d.ts'/>
/**
 * Created by Ivaylo Hristov on 10.2.2016 г..
 */
module Fractal {
    export let old_picture;

    export function setAccountTransformations($item, transform) {
        let cubeWidth;
        cubeWidth = $item.find('.cube-size').width() / 2;
        switch (transform) {
            case 'initial':
                $($item).find('.profile').css({
                    WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                });
                $($item).find('.cropping-section').css({
                    WebkitTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                    MozTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                    msTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                    OTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                    transform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)'
                });
                break;
            case 'right':
                $($item).find('.profile').css({
                    WebkitTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                    MozTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                    OTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                    msTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                    transform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)'
                });
                $($item).find('.cropping-section').css({
                    WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                    transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                });
                break;
            default:
                console.error('Forgotten transform keyword');
                return 0;
        }


        $item.find('.half-account-cube-sides').css({
            WebkitTransform: 'translateZ(-' + (cubeWidth) + 'px)',
            MozTransform: 'translateZ(-' + (cubeWidth) + 'px)',
            OTransform: 'translateZ(-' + (cubeWidth) + 'px)',
            msTransform: 'translateZ(-' + (cubeWidth) + 'px)',
            transform: 'translateZ(-' + (cubeWidth) + 'px)'
        });

    }
}