/**
 * Created by Ivaylo Hristov on 4.4.2016 г..
 */
//Fix missing definitions
declare var HeyzapAds:any;

module Fractal {
    export class HeyzapController {
        private personal_id:string;

        constructor(personal_id:string) {
            this.personal_id = personal_id;

            (<any>window).HeyzapAds.showMediationTestSuite();
            this.init();
        }

        init() {
            (<any>window).HeyzapAds.showMediationTestSuite();
            HeyzapAds.start(this.personal_id).then(function () {
                console.info("Heyzap initialised.");

            }, function (error) {
                console.error("Heyzap unable to initialise: " + error);

            });
        }

        showInterstitialAd() {
            HeyzapAds.InterstitialAd.show().then(function () {
                // Native call successful.

            }, function (error) {

            });
        }

        showVideoAd() {
            HeyzapAds.VideoAd.show().then(function () {
                // Native call successful.

            }, function (error) {
                console.error("Native call wasn't successful: " + error);

            });
        }

        showBannerAd(position:string) {
            let bannerPosition = (position === "bottom") ? HeyzapAds.BannerAd.POSITION_BOTTOM : HeyzapAds.BannerAd.POSITION_TOP;
            HeyzapAds.BannerAd.show(bannerPosition).then(function () {
            }, function (error) {
                console.error("Native call wasn't successful: " + error);

            });
        }

        destroyBannerAd() {
            HeyzapAds.BannerAd.destroy().then(function () {
            }, function (error) {
                console.error("Native call wasn't successful: " + error);
            });
        }

    }
}