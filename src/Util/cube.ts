///<reference path="helpers.ts"/>
// /**
//  * Created by Ivaylo Hristov on 10.2.2016 г..
//  */



//
//
// function sinusoidalRotation($cube, transform, x_times, y_times) {
//     // let cubeWidth;
//     // cubeWidth = $cube.width() / 2;
//     // switch (transform) {
//     //     case 'first':
//     //         $($cube).find('.left').css({
//     //             WebkitTransform: 'rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             MozTransform: 'rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             OTransform: 'rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             msTransform: 'rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             transform: 'rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)'
//     //         });
//     //         $($cube).find('.front').css({
//     //             WebkitTransform: 'rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             MozTransform: 'rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             msTransform: 'rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             OTransform: 'rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             transform: 'rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)'
//     //         });
//     //         $($cube).find('.right').css({
//     //             WebkitTransform: 'rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             MozTransform: 'rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             OTransform: 'rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             msTransform: 'rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             transform: 'rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)'
//     //         });
//     //         $($cube).find('.top').css({
//     //             WebkitTransform: 'rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(' + (cubeWidth) + 'px)',
//     //             MozTransform: 'rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(' + (cubeWidth ) + 'px)',
//     //             msTransform: 'rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(' + (cubeWidth) + 'px)',
//     //             OTransform: 'rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(' + (cubeWidth) + 'px)',
//     //             transform: 'rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(' + (cubeWidth ) + 'px)'
//     //         });
//     //         $($cube).find('.bottom').css({
//     //             WebkitTransform: 'rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(' + (cubeWidth) + 'px)',
//     //             MozTransform: 'rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(' + (cubeWidth ) + 'px)',
//     //             msTransform: 'rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(' + (cubeWidth) + 'px)',
//     //             OTransform: 'rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(' + (cubeWidth) + 'px)',
//     //             transform: 'rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(' + (cubeWidth ) + 'px)'
//     //         });
//     //         $($cube).find('.back').css({
//     //             WebkitTransform: 'rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             MozTransform: 'rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             OTransform: 'rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             msTransform: 'rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             transform: 'rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)'
//     //         });
//     //
//     //         break;
//     //     case 'second':
//     //         $($cube).find('.left').css({
//     //             WebkitTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (-46 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             MozTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (-46 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             OTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (-46 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             msTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (-46 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             transform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (-46 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)'
//     //         });
//     //         $($cube).find('.front').css({
//     //             WebkitTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (44 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             MozTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (44 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             msTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (44 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             OTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (44 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             transform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (44 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)'
//     //         });
//     //         $($cube).find('.right').css({
//     //             WebkitTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (134 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             MozTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (134 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             OTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (134 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             msTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (134 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             transform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (134 + (360 * y_times)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)'
//     //         });
//     //         $($cube).find('.top').css({
//     //             WebkitTransform: 'rotateX(' + (54 + (360 * x_times)) + 'deg) rotateY(0deg) rotateZ(' + (46 - (y_times * 360)) + 'deg) translateZ(' + (cubeWidth) + 'px)',
//     //             MozTransform: 'rotateX(' + (54 + (360 * x_times)) + 'deg) rotateY(0deg) rotateZ(' + (46 - (y_times * 360)) + 'deg) translateZ(' + (cubeWidth ) + 'px)',
//     //             msTransform: 'rotateX(' + (54 + (360 * x_times)) + 'deg) rotateY(0deg) rotateZ(' + (46 - (y_times * 360)) + 'deg) translateZ(' + (cubeWidth) + 'px)',
//     //             OTransform: 'rotateX(' + (54 + (360 * x_times)) + 'deg) rotateY(0deg) rotateZ(' + (46 - (y_times * 360)) + 'deg) translateZ(' + (cubeWidth) + 'px)',
//     //             transform: 'rotateX(' + (54 + (360 * x_times)) + 'deg) rotateY(0deg) rotateZ(' + (46 - (y_times * 360)) + 'deg) translateZ(' + (cubeWidth ) + 'px)'
//     //         });
//     //         $($cube).find('.bottom').css({
//     //             WebkitTransform: 'rotateX(' + (234 + (360 * x_times)) + 'deg) rotateY(0deg)  rotateZ(' + (44 + (y_times * 360)) + 'deg)  translateZ(' + (cubeWidth) + 'px)',
//     //             MozTransform: 'rotateX(' + (234 + (360 * x_times)) + 'deg) rotateY(0deg)  rotateZ(' + (44 + (y_times * 360)) + 'deg)  translateZ(' + (cubeWidth ) + 'px)',
//     //             msTransform: 'rotateX(' + (234 + (360 * x_times)) + 'deg) rotateY(0deg)  rotateZ(' + (44 + (y_times * 360)) + 'deg)  translateZ(' + (cubeWidth) + 'px)',
//     //             OTransform: 'rotateX(' + (234 + (360 * x_times)) + 'deg) rotateY(0deg)  rotateZ(' + (44 + (y_times * 360)) + 'deg)  translateZ(' + (cubeWidth) + 'px)',
//     //             transform: 'rotateX(' + (234 + (360 * x_times)) + 'deg) rotateY(0deg)  rotateZ(' + (44 + (y_times * 360)) + 'deg)  translateZ(' + (cubeWidth ) + 'px)'
//     //         });
//     //         $($cube).find('.back').css({
//     //             WebkitTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (224 + (y_times * 360)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             MozTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (224 + (y_times * 360)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             OTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (224 + (y_times * 360)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             msTransform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (224 + (y_times * 360)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)',
//     //             transform: 'rotateX(' + (-36 + (360 * x_times)) + 'deg) rotateY(' + (224 + (y_times * 360)) + 'deg) rotateZ(0deg) translateZ(' + (cubeWidth) + 'px)'
//     //         });
//     //         break;
//     //     default:
//     //         console.log('Forgotten transform keyword');
//     //         return 0;
//     //         //}
//     // }
// }
//
// //function initializeCube() {
// //    let state = 'first';
// //
// //    let cube = $('#pack > .fractal-games-splashscreen .main-cube .cube');
// //
// //    let sides = $('.main-cube .side');
// //
// //    //Make the cube
// //    sinusoidalRotation(cube, state, 0, 0);
// //
// //    // Wait 0.5s before rotating the cube
// //    setTimeout(function () {
// //        state = 'second';
// //        sinusoidalRotation(cube, state, 0, 1);
// //        sides.addClass('cube-transition');
// //    }, waitTime);
// //}
// let timeoutInit;
// let timeoutEnd;
// let timeoutPulsate;
// function initializeCube(cube, sides) {
//     let state = 'first';
//     clearInterval(timeoutInit);
//     //let cube = $('.top-the-colour-game .fractal-games-splashscreen .fractal-games-splashscreen-cube');
//
//     //let sides = $('.top-the-colour-game .fractal-games-splashscreen-main-cube .fractal-games-splashscreen-side');
//
//     //Make the cube
//     sinusoidalRotation(cube, state, 0, 0);
//
// }
//
// // function splashScreen($cube, $sides, callback) {
// //     let fractalGamesSplashscreen = $cube.closest('.fractal-games-splashscreen');
// //     //   Cube initialize
// //     clearInterval(timeoutEnd);
// //     clearInterval(timeoutPulsate);
// //
// //     initializeCube($cube, $sides);
// //     //Wait 2400 ms before transitioning the cube
// //     //fractalGamesSplashscreen.find('.fractal-games-splashscreen-top').one('transitionend oTransitionEnd', function () {
// //     //alert('yee');
// //     timeoutEnd = setTimeout(()=>{
// //         fractalGamesSplashscreen.addClass('fractal-games-splashscreen-end');
// //     },2400);
// //
// //
// //     timeoutPulsate = setTimeout(()=>{
// //         $sides.parent().addClass('pulsate-cube');
// //         callback();
// //     },3200);
// //
// // }
//
// //function splashScreen(callback) {
// //    let fractalGamesSplashscreen = $('.fractal-games-splashscreen');
// //    //   Cube initialize
// //    initializeCube();
// //    //Wait 2400 ms before transitioning the cube
// //    setTimeout(function () {
// //        fractalGamesSplashscreen.addClass('fractal-games-splashscreen-end');
// //        setTimeout(function () {
// //            fractalGamesSplashscreen.addClass('hidden');
// //
// //            callback();
// //        }, fractalEndTransitionTime + waitTime)
// //    }, waitTime * 2 + cubeTransitionTime)
// //}
