// const MAIN_URL = 'http://10.7.3.150:8000';
module Fractal {
    export const MAIN_URL = 'http://api.fractalgames.com';
    // export const MAIN_URL = 'http://10.7.3.150:8000';

    export const LOGIN_URL = '/api/user/login/';
    export const SHORT_ACCOUNT_INFO_URL = '/api/user/shortaccountinfo/';
    export const FACEBOOK_URL = '/api/user/facebook/';
    export const GOOGLE_URL = '/api/user/google/';
    export const TWITTER_LOGIN_URL = '/api/user/twitter/';
    export const TWITTER_REGISTER_URL = '/api/user/twitterregister/';
    export const POST_SCORE = "/api/game/postscore/";
    export const POST_HIGH_SCORE = "/api/game/posthighscore/";
    export const GET_HIGH_SCORES = "/api/game/gethighscores/";

    export const GLOBAL_HIGH_SCORES = "/api/game/specificglobalsleaderboard/";
    export const FRIENDS_HIGH_SCORES = "/api/game/specificfriendsleaderboard/";

    export const USER_INFO = '/api/user/userinfo/';
    export const CHANGE_INFO = '/api/user/changeinfo/';
    export const CHECK_USERNAME_URL = '/api/user/checkusername/';
    export const CHECK_EMAIL_URL = '/api/user/checkemail/';

    export const REGISTRATION_URL = '/api/user/registration/';
    export const FORGOTTEN_PASSWORD_URL = '/api/user/forgotten-password/';

    export const CHECK_GAME_BOUGHT_URL = '/api/user/checkgamebought/';
    export const BUY_GAME_URL = '/api/user/buypro/';

    export const waitTime = 500;
    export const cubeTransitionTime = 1600;
    export const fractalEndTransitionTime = 600;
    export const logoTransitionTime = 1300;


    export const PRODUCTS = [
        {
            id: "com.fractalgames.thecolourgame.noads.extramodes",
            alias: "noadsextramodes",
            type: "non consumable"
        }
    ];
}
module TheColourGame {

    export enum ORIENTATIONS{
        PORTRAIT,
        LANDSCAPE
    }

    export const BOARD_STARTING_TILES:number = 2;
    export const BOARD_PORTRAIT_HEIGHT_MULTIPLIER:number = 0.6; //1025 / 1920;
    export const BOARD_PORTRAIT_LINE_WIDTH_MULTIPLIER:number = 20 / 1080;
    export const MIN_RANDOM_COLOR:number = 20;
    export const MAX_RANDOM_COLOR:number = 230;
    export const HIGHEST_LEVEL_INDEX:number = 6;
    export const LEVELS_TO_REPEAT:Array<number> = [3, 4, 5];
    export const CHOSEN_TILE_MIN_ALPHA:number = 60;
    export const CHOSEN_TILE_MAX_ALPHA:number = 82;
    export const UI_PORTRAIT_NUMBERS_FONT_SIZE_MULTIPLIER = 0.06;
    export const UI_LANDSCAPE_NUMBERS_FONT_SIZE_MULTIPLIER = 0.09;
    export const UI_ELEMENTS_VERTICAL_OFFSET_MULTIPLIER = 1.5;
    export const TABLET_RESOLUTION_RATIO_THRESHOLD = 0.65;
    export const UI_PHONE_NUMBERS_ADDITIONAL_OFFSET_MULTIPLIER = 1.1;
    export const UI_QUIT_REFRESH_Y_OFFSET_MULTIPLIER = 0.125;
    export const UI_LANDSCAPE_RIGHT_ELEMENTS_VOFFSET_MULTIPLIER:number = 0.15;
    export const UI_LANDSCAPE_TABLET_LOGO_REDUCTION_MULTIPLIER:number = 0.7;
    export const UI_LANDSCAPE_TABLET_MODE_TEXT_REDUCTION_MULTIPLIER:number = 0.5;
    export const UI_SURVIVAL_TABLET_TIME_OFFSET_MULTIPLIER:number = 0.02;
    export const UI_DECIMAL_TIME_FONT_SIZE_REDUCTION_MULTIPLIER:number = 0.5;
    export const ORIENTATION_CHANGE_MINIMAL_INTERVAL:number = 100;

    export const COUNTDOWN_STARTING_TIMER:number = 60;

    export const SHOOTDOWN_STARTING_TIMER:number = 5;
    export const SHOOTDOWN_DEFAULT_TARGET:number = 40;
    export const SHOOTDOWN_STORAGE_TARGET_KEY:string = "shootdownTarget";

    export const TARGET_DEFAULT_TARGET:number = 40;
    export const TARGET_STORAGE_TARGET_KEY:string = "targetTarget";
    export const TARGET_TIME_RATIO_TO_TARGET:number = 1.35;

    export const SURVIVAL_DEFAULT_TIMER:number = 10;
    export const SURVIVAL_BONUS_PENALTY_TWEENS_DURATION:number = 250;
    export const SURVIVAL_DECIMAL_NUMBER_VOFFSET_MULTIPLIER:number = 0.4;
    export const SURVIVAL_BONUS_PENALTY_FONT_SIZE_MULTIPLIER:number = 0.5;
    export const SURVIVAL_PORTRAIT_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER:number = 0.04;
    export const SURVIVAL_LANDSCAPE_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER:number = 0.06;
    export const SURVIVAL_LANDSCAPE_TABLET_FLOATING_NUMBERS_TWEEN_DISTANCE_REDUCTION_MULTIPLIER:number = 0.02;
    export const PACKAGE_NAME:string = "com.fractalgames.thecolourgame";
    export const GAME_IDENTIFIER:string = "the-colour-game";
}




