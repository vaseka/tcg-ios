var Fractal;
(function (Fractal) {
    var HeyzapController = (function () {
        function HeyzapController(personal_id) {
            this.personal_id = personal_id;
            window.HeyzapAds.showMediationTestSuite();
            this.init();
        }
        HeyzapController.prototype.init = function () {
            window.HeyzapAds.showMediationTestSuite();
            HeyzapAds.start(this.personal_id).then(function () {
                console.info("Heyzap initialised.");
            }, function (error) {
                console.error("Heyzap unable to initialise: " + error);
            });
        };
        HeyzapController.prototype.showInterstitialAd = function () {
            HeyzapAds.InterstitialAd.show().then(function () {
                // Native call successful.
            }, function (error) {
            });
        };
        HeyzapController.prototype.showVideoAd = function () {
            HeyzapAds.VideoAd.show().then(function () {
                // Native call successful.
            }, function (error) {
                console.error("Native call wasn't successful: " + error);
            });
        };
        HeyzapController.prototype.showBannerAd = function (position) {
            var bannerPosition = (position === "bottom") ? HeyzapAds.BannerAd.POSITION_BOTTOM : HeyzapAds.BannerAd.POSITION_TOP;
            HeyzapAds.BannerAd.show(bannerPosition).then(function () {
            }, function (error) {
                console.error("Native call wasn't successful: " + error);
            });
        };
        HeyzapController.prototype.destroyBannerAd = function () {
            HeyzapAds.BannerAd.destroy().then(function () {
            }, function (error) {
                console.error("Native call wasn't successful: " + error);
            });
        };
        return HeyzapController;
    }());
    Fractal.HeyzapController = HeyzapController;
})(Fractal || (Fractal = {}));
