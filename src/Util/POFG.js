///<reference path="../Lib/Typings/phaser/phaser.d.ts"/>
var TheColourGame;
(function (TheColourGame) {
    var POFG = (function () {
        function POFG(game) {
            this.game = game;
            this.fullDrawDelay = 1200;
            this.timerUpdateDelay = 40;
            this.fakeSprite = this.game.add.sprite(-30, -10);
            this.fakeSprite.alpha = 0;
            this.fakeSprite.width = 1;
            this.fakeSprite.height = 1;
            this.createFakeTween(this.fullDrawDelay);
        }
        POFG.prototype.update = function () {
            //console.log(this.game.tweens.getAll().length.toString());
            if (this.game.tweens.getAll().length > 0) {
                this.game.lockRender = false;
                this.tweensActive = true;
            }
            else {
                this.tweensActive = false;
            }
            if (this.game.input.activePointer.isDown) {
                this.game.lockRender = false;
            }
            else if (!this.tweensActive) {
                this.game.lockRender = true;
            }
        };
        POFG.prototype.destroy = function () {
            this.fullDrawDelay = null;
            this.timerUpdateDelay = null;
            this.tweensActive = null;
            this.fakeSprite.destroy();
            this.fakeSprite = null;
        };
        ;
        POFG.prototype.forceUnlock = function (isChangingOrientation) {
            if (!isChangingOrientation) {
                this.game.lockRender = false;
                this.createFakeTween(this.timerUpdateDelay);
            }
            else {
                this.createFakeTween(this.fullDrawDelay);
            }
        };
        POFG.prototype.createFakeTween = function (tweenDuration) {
            this.game.add.tween(this.fakeSprite).to({
                x: this.fakeSprite.x - 10,
                y: this.fakeSprite.y - 10
            }, tweenDuration, Phaser.Easing.Default, true, 0, 0, false);
        };
        return POFG;
    }());
    TheColourGame.POFG = POFG;
})(TheColourGame || (TheColourGame = {}));
