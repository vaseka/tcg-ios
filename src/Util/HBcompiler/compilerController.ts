///<reference path='../../Lib/typings/handlebars/handlebars.d.ts'/>
///<reference path="../../Lib/Typings/jquery/jquery.d.ts"/>
///<reference path="../../Lib/Custom/mainReady.ts"/>
/**
 * Created by Ivaylo Hristov on 11.2.2016 г..
 */
module Fractal {
    export class TemplateCompilerController {
        //    Class that handles the handlebars templates
        //    needs to have properties with the handlebars sources and templates
        //    and functions to update them
        //TODO: For the near future me: remove real-time compilation and pre-compile them.
        container:JQuery;
        source:Function;
        static globalCurrentPlace:number = 1;
        static friendsCurrentPlace:number = 1;

        constructor(container:JQuery, source:JQuery) {
            this.source = Handlebars.compile(source.html());
            this.container = container;
            this.container.html('');
        }

        compileData(data:Object) {
            let dataSource = this.source(data);
            this.container.html(dataSource)
        }

        static registerHelpers() {
            Handlebars.registerHelper('incPlace', function (selector, options) {
                if (selector === "friends") {
                    TemplateCompilerController.friendsCurrentPlace++;
                }
                else {
                    TemplateCompilerController.globalCurrentPlace++;
                }
                return "";
            });

            Handlebars.registerHelper('ifCond', function (v1, v2, options) {
                if (v1 === v2) {
                    return options.fn(this);
                }
                return options.inverse(this);
            });

            Handlebars.registerHelper('inc', function (selector, options) {
                if (selector === "friends") {
                    return TemplateCompilerController.friendsCurrentPlace;
                }
                else {
                    return TemplateCompilerController.globalCurrentPlace;
                }
            });

            Handlebars.registerHelper('checkMode', function (object, options) {
                if (object) {
                    return object
                }
                else {
                    return 0
                }
            });

            Handlebars.registerHelper('checkUser', (user, options) => {
                if (user.user_info.username == TheColourGame.accountHandler.user.username) {
                    return "background:rgba(85, 85, 85, 0.2);"
                }
                return "";

            });

            Handlebars.registerHelper('checkPlace', (selector, options) => {
                if (selector === "friends") {
                    switch (TemplateCompilerController.friendsCurrentPlace) {
                        case 1:
                            return "gold";
                        case 2:
                            return "silver";
                        case 3:
                            return "bronze";
                        default:
                            return "red";
                    }
                }
                else {
                    switch (TemplateCompilerController.globalCurrentPlace) {
                        case 1:
                            return "gold";
                        case 2:
                            return "silver";
                        case 3:
                            return "bronze";
                        default:
                            return "red";
                    }
                }
            })
        }
    }
}
