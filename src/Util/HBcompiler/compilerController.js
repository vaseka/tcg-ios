///<reference path='../../Lib/typings/handlebars/handlebars.d.ts'/>
///<reference path="../../Lib/Typings/jquery/jquery.d.ts"/>
///<reference path="../../Lib/Custom/mainReady.ts"/>
/**
 * Created by Ivaylo Hristov on 11.2.2016 г..
 */
var Fractal;
(function (Fractal) {
    var TemplateCompilerController = (function () {
        function TemplateCompilerController(container, source) {
            this.source = Handlebars.compile(source.html());
            this.container = container;
            this.container.html('');
        }
        TemplateCompilerController.prototype.compileData = function (data) {
            var dataSource = this.source(data);
            this.container.html(dataSource);
        };
        TemplateCompilerController.registerHelpers = function () {
            Handlebars.registerHelper('incPlace', function (selector, options) {
                if (selector === "friends") {
                    TemplateCompilerController.friendsCurrentPlace++;
                }
                else {
                    TemplateCompilerController.globalCurrentPlace++;
                }
                return "";
            });
            Handlebars.registerHelper('ifCond', function (v1, v2, options) {
                if (v1 === v2) {
                    return options.fn(this);
                }
                return options.inverse(this);
            });
            Handlebars.registerHelper('inc', function (selector, options) {
                if (selector === "friends") {
                    return TemplateCompilerController.friendsCurrentPlace;
                }
                else {
                    return TemplateCompilerController.globalCurrentPlace;
                }
            });
            Handlebars.registerHelper('checkMode', function (object, options) {
                if (object) {
                    return object;
                }
                else {
                    return 0;
                }
            });
            Handlebars.registerHelper('checkUser', function (user, options) {
                if (user.user_info.username == TheColourGame.accountHandler.user.username) {
                    return "background:rgba(85, 85, 85, 0.2);";
                }
                return "";
            });
            Handlebars.registerHelper('checkPlace', function (selector, options) {
                if (selector === "friends") {
                    switch (TemplateCompilerController.friendsCurrentPlace) {
                        case 1:
                            return "gold";
                        case 2:
                            return "silver";
                        case 3:
                            return "bronze";
                        default:
                            return "red";
                    }
                }
                else {
                    switch (TemplateCompilerController.globalCurrentPlace) {
                        case 1:
                            return "gold";
                        case 2:
                            return "silver";
                        case 3:
                            return "bronze";
                        default:
                            return "red";
                    }
                }
            });
        };
        TemplateCompilerController.globalCurrentPlace = 1;
        TemplateCompilerController.friendsCurrentPlace = 1;
        return TemplateCompilerController;
    }());
    Fractal.TemplateCompilerController = TemplateCompilerController;
})(Fractal || (Fractal = {}));
