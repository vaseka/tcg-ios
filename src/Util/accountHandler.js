///<reference path="Service/requestsHandler.ts"/>
///<reference path="../Lib/Custom/mainReady.ts"/>
var Fractal;
(function (Fractal) {
    var accountHandler = (function () {
        function accountHandler() {
            var _this = this;
            this.serviceHandler = new Fractal.RequestsHandler();
            this.serviceHandler.initalize();
            Fractal.TemplateCompilerController.registerHelpers();
            this.loginCompiler = new Fractal.TemplateCompilerController($('.login-overlay'), $('#hb-template-login'));
            this.accountCompiler = new Fractal.TemplateCompilerController($('.account-modal'), $('#hb-template-account'));
            this.leaderboardGlobalCompiler = new Fractal.TemplateCompilerController($('.leaderboard-modal-globals-users'), $('#hb-template-globals-leaderboard'));
            this.leaderboardFriendsCompiler = new Fractal.TemplateCompilerController($('.leaderboard-modal-friends-users'), $('#hb-template-friends-leaderboard'));
            if (navigator.connection.type !== 'none') {
                this.serviceHandler.shortAccountInfo(function (r) {
                    // console.log(r);
                    _this.user = _this.registerUser(r.user);
                    _this.user.userInit();
                    _this.user.getHighScores();
                    // console.log(this.user);
                }, function (r) {
                    localStorage.removeItem('userCred');
                    localStorage.removeItem('userInfo');
                    // this.serviceHandler.removeToken();
                    _this.user = _this.registerAnonymous();
                    _this.user.userInit();
                    // console.log(this.user);
                });
            }
            else {
                debugger;
                var user = Fractal.User.getSavedInfo();
                if (user) {
                    this.user = this.registerUser(user);
                    this.user.userInit();
                }
                else {
                    localStorage.removeItem('userCred');
                    localStorage.removeItem('userInfo');
                    // this.serviceHandler.removeToken();
                    this.user = this.registerAnonymous();
                    this.user.userInit();
                }
            }
        }
        accountHandler.prototype.registerUser = function (user) {
            return new Fractal.User(false, this.serviceHandler, this.loginCompiler, this.accountCompiler, user.username, user.user_avatar_url, user.total_iq, user.level, user.prev_level_iq, user.prev_level_name, user.next_level_iq, user.next_level_name);
        };
        accountHandler.prototype.registerAnonymous = function () {
            return new Fractal.User(true, this.serviceHandler, this.loginCompiler, this.accountCompiler, null, null, 0, "Kindergarten", 0, null, 1000, "Pre-school");
        };
        return accountHandler;
    }());
    Fractal.accountHandler = accountHandler;
})(Fractal || (Fractal = {}));
