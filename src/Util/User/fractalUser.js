///<reference path='../../Lib/typings/jquery/jquery.d.ts'/>
///<reference path='../helpers.ts'/>
///<reference path="../cube.ts"/>
///<reference path="../accountScript.ts"/>
///<reference path="../HBcompiler/compilerController.ts"/>
///<reference path="../Service/requestsHandler.ts"/>
///<reference path="../../Lib/Custom/splashScripts.ts"/>
///<reference path="../../Lib/Custom/mainReady.ts"/>
var Fractal;
(function (Fractal) {
    var User = (function () {
        function User(anonymous, serviceHandler, loginCompiler, accountCompiler, username, user_avatar, iq, level, prevLevelIq, prevLevelName, nextLevelIq, nextLevelName) {
            this.anonymous = anonymous;
            this.serviceHandler = serviceHandler;
            this.loginCompiler = loginCompiler;
            this.accountCompiler = accountCompiler;
            this.username = username;
            this.user_avatar = user_avatar;
            this.iq = iq;
            this.level = level;
            this.prevLevelIq = prevLevelIq;
            this.prevLevelName = prevLevelName;
            this.nextLevelIq = nextLevelIq;
            this.nextLevelName = nextLevelName;
        }
        User.prototype.updateUser = function (data) {
            if (data) {
                this.anonymous = false;
                this.username = data.username;
                this.user_avatar = data.user_avatar_url;
                this.iq = data.total_iq;
                this.level = data.level;
                this.prevLevelIq = data.prev_level_iq;
                this.prevLevelName = data.prev_level_name;
                this.nextLevelIq = data.next_level_iq;
                this.nextLevelName = data.next_level_name;
                this.saveUserToLocalStorage();
            }
            else {
                this.anonymous = true;
                this.username = null;
                this.user_avatar = null;
                this.iq = 0;
                this.level = 'Kindergarten';
                this.prevLevelIq = 0;
                this.prevLevelName = '';
                this.nextLevelIq = 1000;
                this.nextLevelName = 'Pre-school';
            }
        };
        User.prototype.userInit = function () {
            this.loginCompiler.compileData(this);
            //Init jquery bullshits
            this.loginOverlayFunctions();
            this.leaderboardOverlayFunctions();
            this.accountCompiler.compileData(this);
            if (TheColourGame.storeReady) {
                TheColourGame.storeCG.products[0].handleProServiceCheck();
            }
            if (!this.anonymous) {
                this.saveUserToLocalStorage();
            }
        };
        User.prototype.clearHighScores = function () {
            localStorage.removeItem('highScoreSurvival');
            localStorage.removeItem('highScoreTarget');
            localStorage.removeItem('highScoreShootdown');
            localStorage.removeItem('highScoreCountdown');
        };
        User.prototype.getHighScores = function () {
            var self = this;
            this.serviceHandler.getHighScores(function (r) {
                // this.clearHighScores();
                var mode_scores = r.modes_scores;
                for (var key in mode_scores) {
                    if (mode_scores.hasOwnProperty(key)) {
                        var value = mode_scores[key];
                        var capitalisedKey = key.replace(/\w/, key.match(/\w/)[0].toUpperCase());
                        var currentScore = parseInt(localStorage.getItem("highScore" + capitalisedKey));
                        if (isNaN(currentScore) || currentScore <= value) {
                            localStorage.setItem("highScore" + capitalisedKey, value);
                        }
                        else {
                            TheColourGame.accountHandler.serviceHandler.sendHighScore(currentScore.toString(), key, function (r) {
                            }, function (r) {
                                console.error(r.responseJSON.detail);
                            });
                        }
                    }
                }
                TheColourGame.setHighScoresInfo();
                TheColourGame.setHighScoresPlay();
            }, function (r) {
                TheColourGame.setHighScoresInfo();
                TheColourGame.setHighScoresPlay();
            });
        };
        User.prototype.updateIQ = function (iq) {
            this.iq += iq;
            this.userInit();
        };
        User.prototype.showLoginOverlay = function () {
            $('.login-overlay').removeClass('disappear').removeClass('fade-in-overlay');
        };
        User.prototype.showLeaderboardOverlay = function () {
            $('.leaderboard-overlay').removeClass('disappear').removeClass('fade-in-overlay');
        };
        User.prototype.loginOverlayFunctions = function () {
            var _$loginOverlay = $('.login-overlay');
            var self = this;
            _$loginOverlay.off('click').on('click', function () {
                $('.login-button-wrapper').addClass('active');
                _$loginOverlay.find('.login-overlay-wrapper').addClass('show');
                _$loginOverlay.addClass('show');
                if (self.anonymous) {
                    self.loginModalFunctions(function () {
                    });
                    $('.login-wrapper-modal').removeClass('not-visible');
                    $('body').addClass('modal-open');
                }
                else {
                    self.accountModalFunction();
                    setTimeout(function () {
                        var modal = $('.account');
                        $('.account-wrapper-modal').removeClass('not-visible');
                        $('body').addClass('modal-open');
                    }, 500);
                }
            });
        };
        User.prototype.leaderboardModalFunctions = function () {
            var self = this;
            var $_leaderboardWrapperModal = $('.leaderboard-wrapper-modal');
            $_leaderboardWrapperModal.find('.close-modal').off('click').on('click', function (e) {
                $_leaderboardWrapperModal.find('.leaderboard-button').removeClass('active');
                $_leaderboardWrapperModal.find('.global-button-wrapper').addClass('active');
                self.closeLeaderboard();
            });
            var $friendsButton = $('.friends-button-wrapper');
            var $globalsButton = $('.global-button-wrapper');
            var $leaderboardModalWrapper = $('.leaderboard-modal-wrapper-sides');
            $friendsButton.off('click').on('click', function (e) {
                $(e.currentTarget).addClass('active');
                $globalsButton.removeClass('active');
                $leaderboardModalWrapper.addClass('effect-on').removeClass('show-right').addClass('show-front');
            });
            $globalsButton.off('click').on('click', function (e) {
                $(e.currentTarget).addClass('active');
                $friendsButton.removeClass('active');
                $leaderboardModalWrapper.addClass('effect-on').removeClass('show-front').addClass('show-right');
            });
            var globalsPagination = 1;
            var canAskForMoreGlobals = true;
            var globalTimeout;
            $('.leaderboard-modal-globals-users-wrapper').off('scroll').on('scroll', function (e) {
                if (canAskForMoreGlobals) {
                    clearTimeout(globalTimeout);
                    globalTimeout = setTimeout(function () {
                        if ($(e.currentTarget).scrollTop() + $(e.currentTarget).height() > $(e.currentTarget).height() - 100) {
                            // if ((<any>navigator).connection.type !== 'none') {
                            globalsPagination++;
                            TheColourGame.accountHandler.serviceHandler.getGlobalHighScoreLeaderboard(globalsPagination, function (r) {
                                if (r.users.length !== 0) {
                                    var data = TheColourGame.accountHandler.leaderboardGlobalCompiler.source(r);
                                    var htmlData = $(data);
                                    var usersToAppend = htmlData.find('div.leaderboard-modal-globals-inner-wrapper');
                                    TheColourGame.accountHandler.leaderboardGlobalCompiler.container
                                        .find('div.leaderboard-modal-globals-inner-wrapper')
                                        .append(usersToAppend.html());
                                }
                                else {
                                    canAskForMoreGlobals = false;
                                }
                            }, function (r) {
                                console.error(r.responseJSON.detail);
                            });
                        }
                    }, 1000);
                }
            });
            var friendsPagination = 1;
            var canAskForMoreFriends = true;
            var friendsTimeout;
            $('.leaderboard-modal-friends-users-wrapper').off('scroll').on('scroll', function (e) {
                if (canAskForMoreFriends) {
                    clearTimeout(friendsTimeout);
                    friendsTimeout = setTimeout(function () {
                        if ($(e.currentTarget).scrollTop() + $(e.currentTarget).height() > $(e.currentTarget).height() - 100) {
                            if (navigator.connection.type !== 'none') {
                                window.facebookConnectPlugin.getLoginStatus(function (r) {
                                    if (r.status == 'connected') {
                                        window.facebookConnectPlugin.api('/me/friends', '', function (r) {
                                            friendsPagination++;
                                            TheColourGame.accountHandler.serviceHandler.getFriendsHighScoreLeaderboard(friendsPagination, r.data, function (r) {
                                                if (r.users.length !== 0) {
                                                    var data = TheColourGame.accountHandler.leaderboardFriendsCompiler.source(r);
                                                    var htmlData = $(data);
                                                    var usersToAppend = htmlData.find('div.leaderboard-modal-friends-inner-wrapper');
                                                    TheColourGame.accountHandler.leaderboardFriendsCompiler.container
                                                        .find('div.leaderboard-modal-friends-inner-wrapper')
                                                        .append(usersToAppend.html());
                                                }
                                                else {
                                                    canAskForMoreFriends = false;
                                                }
                                            }, function (r) {
                                                console.error(r.responseJSON);
                                            });
                                        });
                                    }
                                });
                            }
                            else {
                                Fractal.Helpers.showPopup("There is no internet connection. Connect to the internet to load the leaderboard.");
                            }
                        }
                    }, 1000);
                }
            });
        };
        User.prototype.socialFacebookLogin = function ($elem) {
            var self = this;
            $elem.off('click').on('click', function () {
                if (navigator.connection.type !== 'none') {
                    window.facebookConnectPlugin.login(["email", "public_profile", "user_friends"], function (response) {
                        var accessToken = response.authResponse["accessToken"];
                        Fractal.Helpers.rotateUntilCallback(function () {
                            self.serviceHandler.facebookLogin(accessToken, function (r) {
                                self.logUserIn(r);
                                Fractal.Helpers.removeLoader();
                            }, function (response) {
                                console.warn(response);
                                Fractal.Helpers.removeLoader();
                                Fractal.Helpers.showPopup(response.responseJSON.detail);
                            });
                        });
                    }, function (response) {
                        console.warn(response);
                        Fractal.Helpers.removeLoader();
                        Fractal.Helpers.showPopup(response.errorMessage);
                    });
                }
                else {
                    Fractal.Helpers.removeLoader();
                    Fractal.Helpers.showPopup("Please check your internet connection.");
                }
            });
        };
        User.prototype.leaderboardModalRequest = function () {
            var self = this;
            var _$leaderboardModal = $('.leaderboard-wrapper-modal');
            var _$friendsButton = $('.friends-button-wrapper');
            if (navigator.connection.type !== 'none') {
                Fractal.Helpers.rotateUntilCallback(function () {
                });
                _$friendsButton.addClass('active');
                Fractal.TemplateCompilerController.globalCurrentPlace = 1;
                Fractal.TemplateCompilerController.friendsCurrentPlace = 1;
                TheColourGame.accountHandler.serviceHandler.getGlobalHighScoreLeaderboard(1, function (r) {
                    TheColourGame.accountHandler.leaderboardGlobalCompiler.compileData(r);
                    self.leaderboardModalFunctions();
                    Fractal.Helpers.removeLoader();
                    _$leaderboardModal.removeClass('not-visible');
                }, function (r) {
                    Fractal.Helpers.removeLoader();
                });
                window.facebookConnectPlugin.getLoginStatus(function (r) {
                    if (r.status == 'connected') {
                        window.facebookConnectPlugin.api('/me/friends', '', function (r) {
                            // console.log(r)
                            TheColourGame.accountHandler.serviceHandler.getFriendsHighScoreLeaderboard(1, r.data, function (r) {
                                TheColourGame.accountHandler.leaderboardFriendsCompiler.compileData(r);
                                _$leaderboardModal.find('.leaderboard-modal-friends-users-wrapper').removeClass('no-facebook-connect').addClass('facebook-connect');
                            }, function (r) {
                                TheColourGame.accountHandler.leaderboardFriendsCompiler.compileData('');
                                _$leaderboardModal.find('.leaderboard-modal-friends-users-wrapper').removeClass('facebook-connect').addClass('no-facebook-connect');
                                self.socialFacebookLogin(_$leaderboardModal.find('.socialaccount_provider_facebook_button'));
                            });
                        }, function (r) {
                            Fractal.Helpers.removeLoader();
                            TheColourGame.accountHandler.leaderboardFriendsCompiler.compileData('');
                            console.log(r);
                        });
                    }
                    else {
                        Fractal.Helpers.removeLoader();
                        TheColourGame.accountHandler.leaderboardFriendsCompiler.compileData('');
                        _$leaderboardModal.find('.leaderboard-modal-friends-users-wrapper').removeClass('facebook-connect').addClass('no-facebook-connect');
                        self.socialFacebookLogin(_$leaderboardModal.find('.socialaccount_provider_facebook_button'));
                    }
                }, function (r) {
                    Fractal.Helpers.removeLoader();
                    console.log(r);
                });
            }
            else {
                Fractal.Helpers.showPopup("There is no internet connection. Please connect to the internet to view the leaderboard.");
            }
        };
        User.prototype.leaderboardOverlayFunctions = function () {
            var self = this;
            var _$leaderboardOverlay = $('.leaderboard-overlay');
            var _$leaderboardModal = $('.leaderboard-wrapper-modal');
            var _$buttons = $('.leaderboard-button');
            var _$friendsButton = $('.friends-button-wrapper');
            _$buttons.removeClass('active');
            _$leaderboardOverlay.off('click').on('click', function () {
                self.leaderboardModalRequest();
            });
        };
        User.prototype.loginModalFunctions = function (callback) {
            var $loginSignupTabs = $('#login-signup-tabs');
            var currentClearIntervalUsername;
            var currentClearIntervalEmail;
            var currentClearIntervalPassword;
            var self = this;
            var currentLoginState = 'initial';
            $('.login-overlay-popup-message-button,.login-overlay-popup-message-header-close').off('click').on('click', function () {
                $('.login-overlay-message').removeClass('show-popup');
            });
            $('#forgotten-email-form').find('#email').off('input').on('input', function (elT) {
                var el = $(elT.currentTarget);
                var val = el.val();
                el.removeClass('valid');
                clearTimeout(currentClearIntervalEmail);
                el.parent().find('span').css('display', 'none');
                el.parent().find('div').css('display', 'block');
                if (val !== '') {
                    if (!Fractal.Helpers.hasWhiteSpace(val) && Fractal.Helpers.validateEmail(val)) {
                        currentClearIntervalEmail = setTimeout(function () {
                            self.serviceHandler.checkEmail(val, function (r) {
                                if (r.free) {
                                    el.parent().find('span').html('&#x2716;');
                                    el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                                    el.parent().find('div').css('display', 'none');
                                }
                                else {
                                    el.addClass('valid');
                                    el.parent().find('span').html('&#10004;');
                                    el.parent().find('span').css({ 'display': 'block', 'color': '#0b0' });
                                    el.parent().find('div').css('display', 'none');
                                }
                            });
                        }, 2000);
                    }
                    else {
                        el.parent().find('span').html('&#x2716;');
                        el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                        el.parent().find('div').css('display', 'none');
                    }
                }
                else {
                    el.parent().find('div').css('display', 'none');
                }
            });
            $('.email-signup').off('input').on('input', function (elT) {
                var el = $(elT.currentTarget);
                var val = el.val();
                el.removeClass('valid');
                clearTimeout(currentClearIntervalEmail);
                el.parent().find('span').css('display', 'none');
                el.parent().find('div').css('display', 'block');
                if (val !== '') {
                    if (!Fractal.Helpers.hasWhiteSpace(val) && Fractal.Helpers.validateEmail(val)) {
                        currentClearIntervalEmail = setTimeout(function () {
                            self.serviceHandler.checkEmail(val, function (r) {
                                if (r.free) {
                                    el.addClass('valid');
                                    el.parent().find('span').html('&#10004;');
                                    el.parent().find('span').css({ 'display': 'block', 'color': '#0b0' });
                                    el.parent().find('div').css('display', 'none');
                                }
                                else {
                                    el.parent().find('span').html('&#x2716;');
                                    el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                                    el.parent().find('div').css('display', 'none');
                                }
                            });
                        }, 2000);
                    }
                    else {
                        el.parent().find('span').html('&#x2716;');
                        el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                        el.parent().find('div').css('display', 'none');
                    }
                }
                else {
                    el.parent().find('div').css('display', 'none');
                }
            });
            $('.username-signup').off('input').on('input', function (e) {
                var el = $(e.currentTarget);
                var val = el.val();
                el.removeClass('valid');
                clearTimeout(currentClearIntervalUsername);
                el.parent().find('span').css('display', 'none');
                el.parent().find('div').css('display', 'block');
                if (val !== '') {
                    if (!Fractal.Helpers.hasWhiteSpace(val)) {
                        currentClearIntervalUsername = setTimeout(function () {
                            self.serviceHandler.checkUsername(val, function (r) {
                                if (r.free) {
                                    el.addClass('valid');
                                    el.parent().find('span').html('&#10004;');
                                    el.parent().find('span').css({ 'display': 'block', 'color': '#0b0' });
                                    el.parent().find('div').css('display', 'none');
                                }
                                else {
                                    el.parent().find('span').html('&#x2716;');
                                    el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                                    el.parent().find('div').css('display', 'none');
                                }
                            });
                        }, 2000);
                    }
                    else {
                        el.parent().find('span').html('&#x2716;');
                        el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                        el.parent().find('div').css('display', 'none');
                    }
                }
                else {
                    el.parent().find('div').css('display', 'none');
                }
            });
            $('.password-signup').off('input').on('input', function (e) {
                var el = $(e.currentTarget);
                var val = el.val();
                el.removeClass('valid');
                clearTimeout(currentClearIntervalPassword);
                el.parent().find('span').css('display', 'none');
                el.parent().find('div').css('display', 'block');
                if (val !== '') {
                    if (!Fractal.Helpers.hasWhiteSpace(val)) {
                        currentClearIntervalPassword = setTimeout(function () {
                            //checkUsername(val,(r) =>{
                            if (val.length >= 6) {
                                el.addClass('valid');
                                el.parent().find('span').html('&#10004;');
                                el.parent().find('span').css({ 'display': 'block', 'color': '#0b0' });
                                el.parent().find('div').css('display', 'none');
                            }
                            else {
                                el.parent().find('span').html('&#x2716;');
                                el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                                el.parent().find('div').css('display', 'none');
                            }
                            //});
                        }, 2000);
                    }
                    else {
                        el.parent().find('span').html('&#x2716;');
                        el.parent().find('span').css({ 'display': 'block', 'color': 'red' });
                        el.parent().find('div').css('display', 'none');
                    }
                }
                else {
                    el.parent().find('div').css('display', 'none');
                }
            });
            // Button manage
            var sendForgottenPassword = true;
            $('.send-button').off('click').on('click', function () {
                if (navigator.connection.type !== 'none') {
                    if (sendForgottenPassword) {
                        self.serviceHandler.forgottenPassword($('.email').val(), function (r) {
                            //debugger;
                            // console.log(r);
                            if (r !== '') {
                                Fractal.Helpers.showPopup('Please check if the entered email is valid.');
                            }
                            else {
                                sendForgottenPassword = false;
                                setTimeout(function () {
                                    sendForgottenPassword = true;
                                }, 15000);
                                Fractal.Helpers.showPopup('Please check your email.');
                            }
                        }, function () {
                        });
                    }
                    else {
                        Fractal.Helpers.showPopup('Please wait few seconds and try again :)');
                    }
                }
                else {
                    Fractal.Helpers.showPopup('Please check your internet connection.');
                }
            });
            $('.login-button').off('click').on('click', function () {
                if (navigator.connection.type !== 'none') {
                    Fractal.Helpers.rotateUntilCallback(function () {
                        //debugger;
                        var username = $('.username').val();
                        var password = $('.password').val();
                        self.serviceHandler.insertToken();
                        self.serviceHandler.login(username, password, function (response) {
                            self.logUserIn(response);
                            Fractal.Helpers.removeLoader();
                        }, function () {
                            Fractal.Helpers.showPopup('The username or password were incorrect.');
                            Fractal.Helpers.removeLoader();
                        });
                    });
                }
                else {
                    Fractal.Helpers.showPopup('Please check your internet connection.');
                }
            });
            $('.signup-button').off('click').on('click', function () {
                if (navigator.connection.type !== 'none') {
                    //debugger;
                    var pwdVal = $('.password-signup');
                    var usrVal = $('.username-signup');
                    var emailVal = $('.email-signup');
                    var sendFlag = true;
                    var errors = '';
                    if (emailVal.val() === '') {
                        errors += 'Please enter your email.';
                        sendFlag = false;
                    }
                    else {
                        if (!Fractal.Helpers.validateEmail(emailVal.val())) {
                            errors += 'Email is not valid.';
                            sendFlag = false;
                        }
                    }
                    if (usrVal.val() === '') {
                        errors += 'Please enter your username.';
                        sendFlag = false;
                    }
                    else {
                        if (!usrVal.hasClass('valid')) {
                            errors += 'Username is already taken.';
                            sendFlag = false;
                        }
                    }
                    if (pwdVal.val() === '') {
                        errors += 'Please enter your password.';
                        sendFlag = false;
                    }
                    else {
                        if (!pwdVal.hasClass('valid')) {
                            errors += 'Password must be at least 6 characters.';
                            sendFlag = false;
                        }
                    }
                    if (sendFlag) {
                        self.serviceHandler.registration(emailVal.val(), usrVal.val(), pwdVal.val(), function (response) {
                            if (response.status !== 200 && !response.token) {
                            }
                            else {
                                self.saveUser(response);
                            }
                        }, function () {
                        });
                    }
                    else {
                        Fractal.Helpers.showPopup(errors);
                    }
                }
                else {
                    Fractal.Helpers.showPopup('Please check your internet connection.');
                }
            });
            if ($loginSignupTabs.length > 0) {
                $loginSignupTabs.removeClass('transition-toggle');
                TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);
            }
            $('.login-button-wrapper').off('click').on('click', function (e) {
                currentLoginState = 'initial';
                $('.signup-button-wrapper').removeClass('active');
                $loginSignupTabs.addClass('transition-toggle');
                $(e.currentTarget).addClass('active');
                TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);
            });
            $('.signup-button-wrapper').off('click').on('click', function (e) {
                currentLoginState = 'left';
                $('.login-button-wrapper').removeClass('active');
                $loginSignupTabs.addClass('transition-toggle');
                $(e.currentTarget).addClass('active');
                TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);
            });
            $('.forgotten-password-button').off('click').on('click', function (e) {
                currentLoginState = 'right';
                $loginSignupTabs.addClass('transition-toggle');
                TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);
            });
            self.socialFacebookLogin($('.socialaccount_provider_facebook'));
            $(".socialaccount_provider_google").off("click").on("click", function () {
                if (navigator.connection.type !== 'none') {
                    window.plugins.googleplus.login({}, function (obj) {
                        Fractal.Helpers.rotateUntilCallback(function () {
                            self.serviceHandler.googleLogin(obj, function (r) {
                                self.logUserIn(r);
                                Fractal.Helpers.removeLoader();
                            }, function (r) {
                                Fractal.Helpers.showPopup("Social account's email is already registered.");
                                Fractal.Helpers.removeLoader();
                            });
                        });
                    }, function (msg) {
                        Fractal.Helpers.showPopup("Something went wrong :( Please try again.");
                    });
                }
                else {
                    Fractal.Helpers.showPopup("Please check your internet connection.");
                }
            });
            $('.login-modal').find('.close-modal').off('click').on('click', function () {
                self.closeLogin();
            });
            TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);
            callback();
        };
        User.prototype.logUserIn = function (response) {
            var self = this;
            self.saveUser(response);
        };
        User.prototype.convertImgToBase64URL = function (url, callback, outputFormat) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function () {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                var dataURL;
                canvas.height = img.height;
                canvas.width = img.width;
                ctx.drawImage(img, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL);
                canvas = null;
                img = null;
            };
            img.src = url;
        };
        User.prototype.saveUserToLocalStorage = function () {
            var userToString = {
                username: this.username,
                total_iq: this.iq,
                level: this.level,
                next_level_iq: this.nextLevelIq,
                next_level_name: this.nextLevelName,
                prev_level_iq: this.prevLevelIq,
                prev_level_name: this.prevLevelName,
                user_avatar_url: this.user_avatar
            };
            this.convertImgToBase64URL(userToString.user_avatar_url, function (base64Img) {
                userToString.user_avatar_url = base64Img;
                var stringifyResponse = JSON.stringify(userToString);
                localStorage.setItem('userInfo', stringifyResponse);
            }, 'png');
        };
        User.prototype.saveUser = function (user) {
            var _this = this;
            var self = this;
            var objToSave = {
                token: user.token
            };
            var stringifyResponse = JSON.stringify(objToSave);
            localStorage.setItem('userCred', stringifyResponse);
            self.serviceHandler.initalize();
            self.serviceHandler.shortAccountInfo(function (r) {
                self.updateUser(r.user);
                self.saveUserToLocalStorage();
                _this.getHighScores();
                _this.convertImgToBase64URL(self.user_avatar, function (base64Img) {
                    self.user_avatar = base64Img;
                    self.userInit();
                    self.closeLogin();
                    self.closeLeaderboard();
                }, 'png');
            }, function (r) {
                // console.log(r);
            });
        };
        User.prototype.accountModalFunction = function () {
            var permitted = false;
            window.requestPermissions.requestCamera(function (r) {
                console.log("%c" + r, "color:green");
                permitted = true;
            }, function (r) {
                console.log("%c" + r, "color:red");
                permitted = false;
            }, "checkCameraPermission");
            $("#file").off('click').on('click', function (e) {
                if (!permitted) {
                    window.requestPermissions.requestCamera(function (r) {
                        permitted = true;
                        return false;
                    }, function (r) {
                        window.requestPermissions.requestCamera(function (r) {
                            permitted = true;
                            console.log("%c" + r, "color:green");
                            return false;
                        }, function (r) {
                            console.log("%c" + r, "color:red");
                            Fractal.Helpers.showPopup("You have not granted permission for photos. If you want to change your avatar you should allow it.");
                            return false;
                        }, "requestCameraPermission");
                        return false;
                    }, "checkCameraPermission");
                    if (!permitted) {
                        e.preventDefault();
                    }
                }
            });
            var self = this;
            var currentState = 'initial';
            // Saving account info ajax request
            $('.profile-edit-save').off('click').on('click', function (e) {
                if (navigator.connection.type !== 'none') {
                    var currentAvatar = $('.current-avatar');
                    var accountUsername = $('#username-edit');
                    var imageB64String = '';
                    var container_1 = $(e.currentTarget);
                    if (currentAvatar.attr('changed') == '' || accountUsername.val() != '') {
                        container_1.find('.loading-rect').addClass('show-loading-g');
                        if (currentAvatar.attr('changed') == '') {
                            imageB64String = currentAvatar.attr('src');
                            imageB64String = imageB64String.toString();
                            imageB64String = imageB64String.replace('data:image/png;base64,', '');
                        }
                        var obj_1 = {
                            username: accountUsername.val(),
                            image_base64: imageB64String
                        };
                        Fractal.Helpers.rotateUntilCallback(function (cubeInterval) {
                            self.serviceHandler.changeInfo(obj_1, function (r) {
                                var newImg = new Image();
                                newImg.src = r.user.user_avatar_url;
                                newImg.onload = function () {
                                    self.updateUser(r.user);
                                    self.convertImgToBase64URL(self.user_avatar, function (base64Img) {
                                        self.user_avatar = base64Img;
                                        self.closeAccount();
                                        self.userInit();
                                        Fractal.Helpers.removeLoader();
                                        container_1.find('.loading-rect').removeClass('show-loading-g');
                                        newImg = null;
                                    }, 'png');
                                };
                            }, function (r) {
                                var msg = r.responseText === '' ? "Please check your internet connection." : r.responseText;
                                self.closeAccount();
                                Fractal.Helpers.showPopup(msg);
                                Fractal.Helpers.removeLoader();
                                container_1.find('.loading-rect').removeClass('show-loading-g');
                            });
                        });
                    }
                    else {
                        if (currentAvatar.attr('changed') != '') {
                            currentAvatar.addClass('no-red-required');
                        }
                        if (accountUsername.val() == '') {
                            accountUsername.addClass('required');
                        }
                    }
                }
                else {
                    Fractal.Helpers.showPopup("Please check your internet connection.");
                }
            });
            $('.profile-logout').off('click').on('click', function () {
                localStorage.removeItem('userInfo');
                localStorage.removeItem('userCred');
                localStorage.removeItem('highScoreTarget');
                localStorage.removeItem('highScoreSurvival');
                localStorage.removeItem('highScoreCountdown');
                localStorage.removeItem('highScoreShootdown');
                if (TheColourGame.storeReady) {
                    localStorage.setItem('productBought', window.store.get('com.fractalgames.thecolourgame.noads.extramodes').owned);
                }
                else {
                    localStorage.removeItem('productBought');
                }
                self.updateUser();
                // (<DailyChallengeUser>self).updateUI();
                self.userInit();
                self.serviceHandler.removeToken();
                self.closeAccount();
                $('.login-overlay-wrapper').addClass('show');
                TheColourGame.setHighScoresInfo();
                TheColourGame.setHighScoresPlay();
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var $resizeImageSelector = $('.resize-image');
                        $('.current-avatar').removeClass('no-red-required');
                        currentState = 'right';
                        $('.half-account-cube-sides').addClass('transition-toggle');
                        Fractal.setAccountTransformations($('#account-sections'), currentState);
                        $resizeImageSelector.attr('src', e.target.result);
                        resizeableImage($resizeImageSelector);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            ;
            $('#file').change(function () {
                readURL(this);
            });
            $(window).resize(function () {
                $('.half-account-cube-sides').removeClass('transition-toggle');
                Fractal.setAccountTransformations($('#account-sections'), currentState);
            });
            Fractal.setAccountTransformations($('#account-sections'), currentState);
            Fractal.Helpers.iq_box_progress_account(self.iq, self.nextLevelIq);
            var resizeableImage = function (imageTarget) {
                // Some letiable and settings
                var $container;
                var orig_src = new Image();
                var image_target = $(imageTarget).get(0);
                var resize_canvas = document.createElement('canvas');
                var oldPosition = {
                    x: 0,
                    y: 0
                };
                var transform = {
                    translate: { x: 0, y: 0 },
                    scale: 1,
                    angle: 0,
                    rx: 0,
                    ry: 0,
                    rz: 0
                };
                var lastX = 0, lastY = 0, lastScale = 1, lastRotation = 0;
                imageTarget = null;
                var deltaRotation = 0;
                var hamContainer;
                var updateElementTransform = function (el) {
                    var value = [
                        'translate3d(' + transform.translate.x + 'px, ' + transform.translate.y + 'px, 0)',
                        'scale(' + transform.scale + ', ' + transform.scale + ')',
                        'rotate3d(' + transform.rx + ',' + transform.ry + ',' + transform.rz + ',' + transform.angle + 'deg)'
                    ];
                    value = value.join(" ");
                    el.style.webkitTransform = value;
                    el.style.mozTransform = value;
                    el.style.transform = value;
                };
                var init = function () {
                    var image_div = $('.resize-image');
                    $('.cropping').append(image_div);
                    $('.resize-container').remove();
                    // When resizing, we will always use this copy of the original as the base
                    orig_src.src = image_target.src;
                    // Wrap the image with the container and add resize handles
                    $(image_target).wrap('<div class="resize-container" style="visibility:hidden"></div>')
                        .before('<span class="resize-handle resize-handle-nw"></span>')
                        .before('<span class="resize-handle resize-handle-ne"></span>')
                        .after('<span class="resize-handle resize-handle-se"></span>')
                        .after('<span class="resize-handle resize-handle-sw"></span>');
                    // Assign the container to a letiable
                    $container = $(image_target).parent('.resize-container');
                    updateElementTransform($container[0]);
                    // Pinch/Zoom, Rotate and Pan
                    hamContainer = new window.Hammer($container[0], {});
                    hamContainer.get('rotate').set({ enable: true });
                    hamContainer.get('pan').set({ direction: window.Hammer.DIRECTION_ALL });
                    hamContainer.get('pinch').set({ enable: true });
                    hamContainer.on('pan', function (ev) {
                        transform.translate = {
                            x: ev.deltaX + lastX,
                            y: ev.deltaY + lastY
                        };
                        updateElementTransform($container[0]);
                    });
                    hamContainer.on('panend', function (ev) {
                        lastX = ev.deltaX;
                        lastY = ev.deltaY;
                    });
                    hamContainer.on('pinch', function (ev) {
                        transform.scale = lastScale * ev.scale;
                        updateElementTransform($container[0]);
                    });
                    hamContainer.on('pinchend', function (ev) {
                        lastScale = transform.scale;
                    });
                    hamContainer.on('rotate', function (ev) {
                        transform.rz = 1;
                        deltaRotation = deltaRotation - ev.rotation;
                        transform.angle -= deltaRotation;
                        updateElementTransform($container[0]);
                        deltaRotation = ev.rotation;
                    });
                    hamContainer.on('rotatestart', function (ev) {
                        deltaRotation = ev.rotation;
                    });
                    // Add events
                    $('.js-crop').on('click', crop);
                    setTimeout(function () {
                        var $overlaySelector = $('.overlay');
                        var size = $overlaySelector.width();
                        resizeImage(size, size / orig_src.width * orig_src.height);
                        var overlay = $overlaySelector;
                        var imageSize = $('.resize-container');
                        setTimeout(function () {
                            imageSize.offset({
                                'left': (overlay.offset().left - ((imageSize.width() - overlay.width()) / 2)),
                                'top': (overlay.offset().top - ((imageSize.height() - overlay.height()) / 2))
                            });
                            $('.resize-container').css('visibility', 'visible');
                            oldPosition.x = $container.offset().left;
                            oldPosition.y = $container.offset().top;
                        }, 100);
                    }, 400);
                };
                var getMatrix = function (obj) {
                    return obj.css("-webkit-transform") ||
                        obj.css("-moz-transform") ||
                        obj.css("-ms-transform") ||
                        obj.css("-o-transform") ||
                        obj.css("transform");
                };
                var parseMatrix = function (_str) {
                    return _str.replace(/^matrix(3d)?\((.*)\)$/, '$2').split(/, /);
                };
                var getRotateDegrees = function (obj) {
                    var matrix = parseMatrix(getMatrix(obj)), rotationAngle = 0;
                    if (matrix[0] !== 'none') {
                        var a = matrix[0], b = matrix[1], c = matrix[2], d = matrix[3];
                        rotationAngle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
                    }
                    return rotationAngle;
                };
                var getScaleDegrees = function (obj) {
                    var matrix = parseMatrix(getMatrix(obj)), scale = 1;
                    if (matrix[0] !== 'none') {
                        var a = matrix[0], b = matrix[1];
                        scale = Math.sqrt(a * a + b * b);
                    }
                    return scale;
                };
                var resizeImage = function (width, height) {
                    resize_canvas.width = width;
                    resize_canvas.height = height;
                    resize_canvas.getContext('2d').drawImage(orig_src, 0, 0, width, height);
                    $(image_target).attr('src', resize_canvas.toDataURL('image/png'));
                };
                var drawRotatedImage = function (context, image, x, y, angle, width, height, scale, difference) {
                    context.save();
                    // Move registration point to the center of the canvas
                    context.translate(x + image.width / 2, y + image.height / 2);
                    context.rotate(angle * Math.PI / 180); // angle must be in radians
                    context.scale(scale, scale);
                    // Move registration point back to the top left corner of canvas
                    context.translate((-image.width) / 2, (-image.height) / 2);
                    context.drawImage(image, 0, 0);
                    context.restore();
                    //thx to dormouse
                };
                var crop = function () {
                    //Find the part of the image that is inside the crop box
                    var $overlaySelector = $('.overlay');
                    var imageScaleFactor = getScaleDegrees($container);
                    var imageDegrees = getRotateDegrees($container);
                    // console.log(imageDegrees);
                    var difference = {
                        x: oldPosition.x - $container.offset().left,
                        y: oldPosition.y - $container.offset().top
                    };
                    // }
                    var crop_canvas, left = -($overlaySelector.offset().left - transform.translate.x - oldPosition.x), top = -($overlaySelector.offset().top - transform.translate.y - oldPosition.y), width = $overlaySelector.width(), height = $overlaySelector.height();
                    crop_canvas = document.createElement('canvas');
                    var context = crop_canvas.getContext('2d');
                    // context.save();
                    crop_canvas.width = width;
                    crop_canvas.height = height;
                    drawRotatedImage(context, image_target, left, top, imageDegrees, width, height, imageScaleFactor, difference);
                    $('.current-avatar').attr('src', crop_canvas.toDataURL('image/png')).attr('changed', '');
                    currentState = 'initial';
                    Fractal.setAccountTransformations($('#account-sections'), currentState);
                    hamContainer = null;
                };
                init();
            };
            $('input').off('keyup').on('keyup', function (e) {
                $(e.currentTarget).removeClass('required');
            });
            $('.account-modal').find('.close-modal').off('click').on('click', function () {
                self.closeAccount();
            });
        };
        User.prototype.closeLeaderboard = function () {
            var _$leaderboardWrapperModal = $('.leaderboard-wrapper-modal');
            var _$buttons = $('.leaderboard-button');
            _$buttons.removeClass('active');
            _$leaderboardWrapperModal.addClass('not-visible');
            _$leaderboardWrapperModal.find('.leaderboard-modal-wrapper-sides').removeClass('show-right').addClass('show-front');
            $('body').removeClass('modal-open');
        };
        User.prototype.closeAccount = function () {
            $('.loading-rect').removeClass('greated-index');
            $('.account-wrapper-modal').addClass('not-visible');
            $('body').removeClass('modal-open');
            $('.resize-container').css('visibility', 'hidden');
            $('.half-account-cube-sides').removeClass('transition-toggle');
            var currentState = 'initial';
            Fractal.setAccountTransformations($('#account-sections'), currentState);
            $('.account-wrapper input').each(function (index, el) {
                $(el).val('').removeClass('required').removeClass('no-red-required');
            });
            $('.login-overlay').removeClass('show');
        };
        User.prototype.closeLogin = function () {
            $('.missing-email').removeClass('active-form');
            $('#forgotten-email-form').removeClass('active-form');
            $('.tabs-3 li').removeClass('active');
            $('.login-wrapper-modal').addClass('not-visible');
            $('#login-signup-tabs').removeClass('transition-toggle');
            var currentState = 'initial';
            $('body').removeClass('modal-open');
            TheColourGame.setLoginTransformations($('#login-signup-tabs'), currentState);
            $('.login-wrapper input').each(function (index, el) {
                $(el).val('').removeClass('required').removeClass('no-red-required');
            });
            $('.login-overlay').removeClass('show');
        };
        User.isLoggedIn = function () {
            var jsonUserInfo = localStorage.getItem('userCred');
            return jsonUserInfo != null;
        };
        User.getSavedCred = function () {
            var jsonUserInfo = localStorage.getItem('userCred');
            if (jsonUserInfo != null) {
                jsonUserInfo = JSON.parse(jsonUserInfo);
                return jsonUserInfo;
            }
            return null;
        };
        User.getSavedInfo = function () {
            var jsonUserInfo = localStorage.getItem('userInfo');
            if (jsonUserInfo != null) {
                jsonUserInfo = JSON.parse(jsonUserInfo);
                return jsonUserInfo;
            }
            return null;
        };
        return User;
    }());
    Fractal.User = User;
})(Fractal || (Fractal = {}));
