///<reference path='../../Lib/typings/jquery/jquery.d.ts'/>
///<reference path='../helpers.ts'/>
///<reference path="../cube.ts"/>
///<reference path="../accountScript.ts"/>
///<reference path="../HBcompiler/compilerController.ts"/>
///<reference path="../Service/requestsHandler.ts"/>
///<reference path="../../Lib/Custom/splashScripts.ts"/>
///<reference path="../../Lib/Custom/mainReady.ts"/>

module Fractal {
    export class User {
        anonymous:boolean;
        username:string;
        user_avatar:string;
        iq:number;
        level:string;
        prevLevelIq:number;
        prevLevelName:string;
        nextLevelIq:number;
        nextLevelName:string;
        serviceHandler:Fractal.RequestsHandler;
        loginCompiler:Fractal.TemplateCompilerController;
        accountCompiler:Fractal.TemplateCompilerController;

        constructor(anonymous:boolean, serviceHandler:Fractal.RequestsHandler, loginCompiler:Fractal.TemplateCompilerController,
                    accountCompiler:Fractal.TemplateCompilerController, username:string, user_avatar:string, iq:number,
                    level:string, prevLevelIq:number, prevLevelName:string, nextLevelIq:number, nextLevelName:string) {
            this.anonymous = anonymous;
            this.serviceHandler = serviceHandler;
            this.loginCompiler = loginCompiler;
            this.accountCompiler = accountCompiler;
            this.username = username;
            this.user_avatar = user_avatar;
            this.iq = iq;
            this.level = level;
            this.prevLevelIq = prevLevelIq;
            this.prevLevelName = prevLevelName;
            this.nextLevelIq = nextLevelIq;
            this.nextLevelName = nextLevelName
        }

        updateUser(data?) {
            if (data) {
                this.anonymous = false;
                this.username = data.username;
                this.user_avatar = data.user_avatar_url;
                this.iq = data.total_iq;
                this.level = data.level;
                this.prevLevelIq = data.prev_level_iq;
                this.prevLevelName = data.prev_level_name;
                this.nextLevelIq = data.next_level_iq;
                this.nextLevelName = data.next_level_name;
                this.saveUserToLocalStorage();
            }
            else {
                this.anonymous = true;
                this.username = null;
                this.user_avatar = null;
                this.iq = 0;
                this.level = 'Kindergarten';
                this.prevLevelIq = 0;
                this.prevLevelName = '';
                this.nextLevelIq = 1000;
                this.nextLevelName = 'Pre-school';
            }
        }


        userInit() {
            this.loginCompiler.compileData(this);
            //Init jquery bullshits
            this.loginOverlayFunctions();
            this.leaderboardOverlayFunctions();
            this.accountCompiler.compileData(this);
            if (TheColourGame.storeReady) {
                TheColourGame.storeCG.products[0].handleProServiceCheck();
            }
            if (!this.anonymous) {
                this.saveUserToLocalStorage();
            }
        }

        clearHighScores() {
            localStorage.removeItem('highScoreSurvival');
            localStorage.removeItem('highScoreTarget');
            localStorage.removeItem('highScoreShootdown');
            localStorage.removeItem('highScoreCountdown');
        }

        getHighScores() {
            let self = this;
            this.serviceHandler.getHighScores((r)=> {
                // this.clearHighScores();
                let mode_scores = r.modes_scores;
                for (let key in mode_scores) {
                    if (mode_scores.hasOwnProperty(key)) {
                        let value = mode_scores[key];
                        let capitalisedKey = key.replace(/\w/, key.match(/\w/)[0].toUpperCase());
                        let currentScore = parseInt(localStorage.getItem("highScore" + capitalisedKey));

                        if (isNaN(currentScore) || currentScore <= value) {
                            localStorage.setItem("highScore" + capitalisedKey, value);
                        }
                        else {
                            TheColourGame.accountHandler.serviceHandler.sendHighScore(currentScore.toString(), key, (r)=> {

                            }, (r)=> {
                                console.error(r.responseJSON.detail);
                            })
                        }
                    }

                }
                TheColourGame.setHighScoresInfo();
                TheColourGame.setHighScoresPlay();
            }, (r)=> {
                TheColourGame.setHighScoresInfo();
                TheColourGame.setHighScoresPlay();
            })
        }

        updateIQ(iq:number) {
            this.iq += iq;
            this.userInit();
        }

        showLoginOverlay() {
            $('.login-overlay').removeClass('disappear').removeClass('fade-in-overlay');
        }

        showLeaderboardOverlay() {
            $('.leaderboard-overlay').removeClass('disappear').removeClass('fade-in-overlay');
        }

        loginOverlayFunctions() {
            let _$loginOverlay = $('.login-overlay');
            let self = this;
            _$loginOverlay.off('click').on('click', ()=> {
                $('.login-button-wrapper').addClass('active');
                _$loginOverlay.find('.login-overlay-wrapper').addClass('show');
                _$loginOverlay.addClass('show');
                if (self.anonymous) {
                    self.loginModalFunctions(()=> {
                    });
                    $('.login-wrapper-modal').removeClass('not-visible');
                    $('body').addClass('modal-open');
                }
                else {
                    self.accountModalFunction();
                    setTimeout(() => {
                        let modal = $('.account');
                        $('.account-wrapper-modal').removeClass('not-visible');
                        $('body').addClass('modal-open');
                    }, 500);
                }
            })
        }

        leaderboardModalFunctions() {
            let self = this;
            let $_leaderboardWrapperModal = $('.leaderboard-wrapper-modal');
            $_leaderboardWrapperModal.find('.close-modal').off('click').on('click', (e)=> {
                $_leaderboardWrapperModal.find('.leaderboard-button').removeClass('active');
                $_leaderboardWrapperModal.find('.global-button-wrapper').addClass('active');
                self.closeLeaderboard();
            });
            let $friendsButton = $('.friends-button-wrapper');
            let $globalsButton = $('.global-button-wrapper');
            let $leaderboardModalWrapper = $('.leaderboard-modal-wrapper-sides');

            $friendsButton.off('click').on('click', (e)=> {
                $(e.currentTarget).addClass('active');
                $globalsButton.removeClass('active');
                $leaderboardModalWrapper.addClass('effect-on').removeClass('show-right').addClass('show-front');
            });

            $globalsButton.off('click').on('click', (e)=> {
                $(e.currentTarget).addClass('active');
                $friendsButton.removeClass('active');
                $leaderboardModalWrapper.addClass('effect-on').removeClass('show-front').addClass('show-right');
            });
            let globalsPagination = 1;
            let canAskForMoreGlobals = true;
            let globalTimeout:number;
            $('.leaderboard-modal-globals-users-wrapper').off('scroll').on('scroll', (e)=> {
                if (canAskForMoreGlobals) {
                    clearTimeout(globalTimeout);
                    globalTimeout = setTimeout(()=> {
                        if ($(e.currentTarget).scrollTop() + $(e.currentTarget).height() > $(e.currentTarget).height() - 100) {
                            // if ((<any>navigator).connection.type !== 'none') {
                                globalsPagination++;
                                TheColourGame.accountHandler.serviceHandler.getGlobalHighScoreLeaderboard(globalsPagination, function (r) {
                                    if (r.users.length !== 0) {
                                        let data = TheColourGame.accountHandler.leaderboardGlobalCompiler.source(r);
                                        let htmlData = $(data);
                                        let usersToAppend = htmlData.find('div.leaderboard-modal-globals-inner-wrapper');
                                        TheColourGame.accountHandler.leaderboardGlobalCompiler.container
                                            .find('div.leaderboard-modal-globals-inner-wrapper')
                                            .append(usersToAppend.html());
                                    }
                                    else {
                                        canAskForMoreGlobals = false;
                                    }
                                }, function (r) {
                                    console.error(r.responseJSON.detail);
                                })
                            // }
                            // else {
                            //     Fractal.Helpers.showPopup("There is no internet connection. Connect to the internet to load the leaderboard.")
                            // }
                        }
                    }, 1000);
                }
            });
            let friendsPagination = 1;
            let canAskForMoreFriends = true;
            let friendsTimeout:number;
            $('.leaderboard-modal-friends-users-wrapper').off('scroll').on('scroll', (e)=> {
                if (canAskForMoreFriends) {
                    clearTimeout(friendsTimeout);
                    friendsTimeout = setTimeout(()=> {
                        if ($(e.currentTarget).scrollTop() + $(e.currentTarget).height() > $(e.currentTarget).height() - 100) {
                            if ((<any>navigator).connection.type !== 'none') {
                                (<any>window).facebookConnectPlugin.getLoginStatus((r) => {
                                    if (r.status == 'connected') {
                                        (<any>window).facebookConnectPlugin.api('/me/friends', '', (r)=> {
                                            friendsPagination++;
                                            TheColourGame.accountHandler.serviceHandler.getFriendsHighScoreLeaderboard(friendsPagination, r.data, (r)=> {
                                                if (r.users.length !== 0) {
                                                    let data = TheColourGame.accountHandler.leaderboardFriendsCompiler.source(r);
                                                    let htmlData = $(data);
                                                    let usersToAppend = htmlData.find('div.leaderboard-modal-friends-inner-wrapper');
                                                    TheColourGame.accountHandler.leaderboardFriendsCompiler.container
                                                        .find('div.leaderboard-modal-friends-inner-wrapper')
                                                        .append(usersToAppend.html());
                                                }
                                                else {
                                                    canAskForMoreFriends = false;
                                                }
                                            }, function (r) {
                                                console.error(r.responseJSON);
                                            })
                                        });
                                    }
                                });
                            }
                            else {
                                Fractal.Helpers.showPopup("There is no internet connection. Connect to the internet to load the leaderboard.")
                            }
                        }
                    }, 1000);

                }
            });
        }

        socialFacebookLogin($elem) {
            let self = this;
            $elem.off('click').on('click', ()=> {
                if ((<any>navigator).connection.type !== 'none') {
                    (<any>window).facebookConnectPlugin.login(["email", "public_profile", "user_friends"], (response) => {
                        var accessToken = response.authResponse["accessToken"];
                        Fractal.Helpers.rotateUntilCallback(()=> {
                            self.serviceHandler.facebookLogin(accessToken, (r) => {
                                self.logUserIn(r);
                                Fractal.Helpers.removeLoader();
                            }, (response) => {
                                console.warn(response);
                                Fractal.Helpers.removeLoader();
                                Fractal.Helpers.showPopup(response.responseJSON.detail);
                            })
                        })
                    }, (response) => {
                        console.warn(response);
                        Fractal.Helpers.removeLoader();
                        Fractal.Helpers.showPopup(response.errorMessage);
                    });
                }
                else {
                    Fractal.Helpers.removeLoader();
                    Fractal.Helpers.showPopup("Please check your internet connection.");
                }

            })
        }

        leaderboardModalRequest() {
            let self = this;
            let _$leaderboardModal = $('.leaderboard-wrapper-modal');
            let _$friendsButton = $('.friends-button-wrapper');

            if ((<any>navigator).connection.type !== 'none') {
                Fractal.Helpers.rotateUntilCallback(()=> {
                });
                _$friendsButton.addClass('active');
                Fractal.TemplateCompilerController.globalCurrentPlace = 1;
                Fractal.TemplateCompilerController.friendsCurrentPlace = 1;
                TheColourGame.accountHandler.serviceHandler.getGlobalHighScoreLeaderboard(1, (r)=> {
                    TheColourGame.accountHandler.leaderboardGlobalCompiler.compileData(r);
                    self.leaderboardModalFunctions();
                    Fractal.Helpers.removeLoader();
                    _$leaderboardModal.removeClass('not-visible');
                }, (r)=> {
                    Fractal.Helpers.removeLoader();
                });
                (<any>window).facebookConnectPlugin.getLoginStatus((r) => {
                    if (r.status == 'connected') {
                        (<any>window).facebookConnectPlugin.api('/me/friends', '', (r)=> {
                            // console.log(r)
                            TheColourGame.accountHandler.serviceHandler.getFriendsHighScoreLeaderboard(1, r.data, (r)=> {
                                TheColourGame.accountHandler.leaderboardFriendsCompiler.compileData(r);
                                _$leaderboardModal.find('.leaderboard-modal-friends-users-wrapper').removeClass('no-facebook-connect').addClass('facebook-connect');
                            }, (r)=> {
                                TheColourGame.accountHandler.leaderboardFriendsCompiler.compileData('');
                                _$leaderboardModal.find('.leaderboard-modal-friends-users-wrapper').removeClass('facebook-connect').addClass('no-facebook-connect');
                                self.socialFacebookLogin(_$leaderboardModal.find('.socialaccount_provider_facebook_button'));
                            })

                        }, (r)=> {
                            Fractal.Helpers.removeLoader();
                            TheColourGame.accountHandler.leaderboardFriendsCompiler.compileData('');
                            console.log(r)
                        });
                    }
                    else {
                        Fractal.Helpers.removeLoader();
                        TheColourGame.accountHandler.leaderboardFriendsCompiler.compileData('');
                        _$leaderboardModal.find('.leaderboard-modal-friends-users-wrapper').removeClass('facebook-connect').addClass('no-facebook-connect');
                        self.socialFacebookLogin(_$leaderboardModal.find('.socialaccount_provider_facebook_button'));
                    }
                }, (r) => {
                    Fractal.Helpers.removeLoader();
                    console.log(r);
                });
            }
            else {
                Fractal.Helpers.showPopup("There is no internet connection. Please connect to the internet to view the leaderboard.")
            }

        }

        leaderboardOverlayFunctions() {
            let self = this;
            let _$leaderboardOverlay = $('.leaderboard-overlay');
            let _$leaderboardModal = $('.leaderboard-wrapper-modal');
            let _$buttons = $('.leaderboard-button');
            let _$friendsButton = $('.friends-button-wrapper');
            _$buttons.removeClass('active');
            _$leaderboardOverlay.off('click').on('click', ()=> {
                self.leaderboardModalRequest();
            });


        }

        loginModalFunctions(callback) {
            let $loginSignupTabs = $('#login-signup-tabs');
            let currentClearIntervalUsername;
            let currentClearIntervalEmail;
            let currentClearIntervalPassword;
            let self = this;
            let currentLoginState = 'initial';

            $('.login-overlay-popup-message-button,.login-overlay-popup-message-header-close').off('click').on('click', () => {
                $('.login-overlay-message').removeClass('show-popup');
            });

            $('#forgotten-email-form').find('#email').off('input').on('input', (elT) => {
                let el = $(elT.currentTarget);
                let val = el.val();
                el.removeClass('valid');
                clearTimeout(currentClearIntervalEmail);
                el.parent().find('span').css('display', 'none');
                el.parent().find('div').css('display', 'block');
                if (val !== '') {
                    if (!Fractal.Helpers.hasWhiteSpace(val) && Fractal.Helpers.validateEmail(val)) {
                        currentClearIntervalEmail = setTimeout(() => {
                            self.serviceHandler.checkEmail(val, (r) => {
                                if (r.free) {
                                    el.parent().find('span').html('&#x2716;');
                                    el.parent().find('span').css({'display': 'block', 'color': 'red'});
                                    el.parent().find('div').css('display', 'none');
                                }
                                else {
                                    el.addClass('valid');
                                    el.parent().find('span').html('&#10004;');
                                    el.parent().find('span').css({'display': 'block', 'color': '#0b0'});
                                    el.parent().find('div').css('display', 'none');
                                }
                            });
                        }, 2000);
                    }
                    else {
                        el.parent().find('span').html('&#x2716;');
                        el.parent().find('span').css({'display': 'block', 'color': 'red'});
                        el.parent().find('div').css('display', 'none');
                    }
                }
                else {
                    el.parent().find('div').css('display', 'none');
                }
            });

            $('.email-signup').off('input').on('input', (elT) => {
                let el = $(elT.currentTarget);
                let val = el.val();
                el.removeClass('valid');
                clearTimeout(currentClearIntervalEmail);
                el.parent().find('span').css('display', 'none');
                el.parent().find('div').css('display', 'block');
                if (val !== '') {
                    if (!Fractal.Helpers.hasWhiteSpace(val) && Fractal.Helpers.validateEmail(val)) {
                        currentClearIntervalEmail = setTimeout(() => {
                            self.serviceHandler.checkEmail(val, (r) => {
                                if (r.free) {
                                    el.addClass('valid');
                                    el.parent().find('span').html('&#10004;');
                                    el.parent().find('span').css({'display': 'block', 'color': '#0b0'});
                                    el.parent().find('div').css('display', 'none');
                                }
                                else {
                                    el.parent().find('span').html('&#x2716;');
                                    el.parent().find('span').css({'display': 'block', 'color': 'red'});
                                    el.parent().find('div').css('display', 'none');
                                }
                            });
                        }, 2000);
                    }
                    else {
                        el.parent().find('span').html('&#x2716;');
                        el.parent().find('span').css({'display': 'block', 'color': 'red'});
                        el.parent().find('div').css('display', 'none');
                    }
                }
                else {
                    el.parent().find('div').css('display', 'none');
                }
            });

            $('.username-signup').off('input').on('input', (e) => {
                let el = $(e.currentTarget);
                let val = el.val();
                el.removeClass('valid');
                clearTimeout(currentClearIntervalUsername);
                el.parent().find('span').css('display', 'none');
                el.parent().find('div').css('display', 'block');
                if (val !== '') {
                    if (!Fractal.Helpers.hasWhiteSpace(val)) {
                        currentClearIntervalUsername = setTimeout(() => {
                            self.serviceHandler.checkUsername(val, (r) => {
                                if (r.free) {
                                    el.addClass('valid');
                                    el.parent().find('span').html('&#10004;');
                                    el.parent().find('span').css({'display': 'block', 'color': '#0b0'});
                                    el.parent().find('div').css('display', 'none');
                                }
                                else {
                                    el.parent().find('span').html('&#x2716;');
                                    el.parent().find('span').css({'display': 'block', 'color': 'red'});
                                    el.parent().find('div').css('display', 'none');
                                }
                            });
                        }, 2000);
                    }
                    else {
                        el.parent().find('span').html('&#x2716;');
                        el.parent().find('span').css({'display': 'block', 'color': 'red'});
                        el.parent().find('div').css('display', 'none');
                    }
                }
                else {
                    el.parent().find('div').css('display', 'none');
                }
            });
            $('.password-signup').off('input').on('input', (e) => {
                let el = $(e.currentTarget);
                let val = el.val();
                el.removeClass('valid');
                clearTimeout(currentClearIntervalPassword);
                el.parent().find('span').css('display', 'none');
                el.parent().find('div').css('display', 'block');
                if (val !== '') {
                    if (!Fractal.Helpers.hasWhiteSpace(val)) {
                        currentClearIntervalPassword = setTimeout(() => {
                            //checkUsername(val,(r) =>{
                            if (val.length >= 6) {
                                el.addClass('valid');
                                el.parent().find('span').html('&#10004;');
                                el.parent().find('span').css({'display': 'block', 'color': '#0b0'});
                                el.parent().find('div').css('display', 'none');
                            }
                            else {
                                el.parent().find('span').html('&#x2716;');
                                el.parent().find('span').css({'display': 'block', 'color': 'red'});
                                el.parent().find('div').css('display', 'none');
                            }
                            //});
                        }, 2000);
                    }
                    else {
                        el.parent().find('span').html('&#x2716;');
                        el.parent().find('span').css({'display': 'block', 'color': 'red'});
                        el.parent().find('div').css('display', 'none');
                    }

                }
                else {
                    el.parent().find('div').css('display', 'none');
                }
            });
            // Button manage
            let sendForgottenPassword = true;
            $('.send-button').off('click').on('click', () => {
                if ((<any>navigator).connection.type !== 'none') {
                    if (sendForgottenPassword) {
                        self.serviceHandler.forgottenPassword($('.email').val(), (r) => {
                            //debugger;
                            // console.log(r);
                            if (r !== '') {
                                Fractal.Helpers.showPopup('Please check if the entered email is valid.');
                            }
                            else {
                                sendForgottenPassword = false;
                                setTimeout(() => {
                                    sendForgottenPassword = true;
                                }, 15000);
                                Fractal.Helpers.showPopup('Please check your email.');
                            }
                        }, () => {

                        });
                    }
                    else {
                        Fractal.Helpers.showPopup('Please wait few seconds and try again :)');
                    }
                }
                else {
                    Fractal.Helpers.showPopup('Please check your internet connection.');
                }
            });

            $('.login-button').off('click').on('click', () => {
                if ((<any>navigator).connection.type !== 'none') {
                    Fractal.Helpers.rotateUntilCallback(()=> {
                        //debugger;
                        let username = $('.username').val();
                        let password = $('.password').val();
                        self.serviceHandler.insertToken();

                        self.serviceHandler.login(username, password, (response) => {
                            self.logUserIn(response);
                            Fractal.Helpers.removeLoader();
                        }, ()=> {
                            Fractal.Helpers.showPopup('The username or password were incorrect.');
                            Fractal.Helpers.removeLoader();
                        });

                    })
                } else {
                    Fractal.Helpers.showPopup('Please check your internet connection.');
                }
            });

            $('.signup-button').off('click').on('click', () => {
                if ((<any>navigator).connection.type !== 'none') {
                    //debugger;
                    let pwdVal = $('.password-signup');
                    let usrVal = $('.username-signup');
                    let emailVal = $('.email-signup');
                    let sendFlag = true;
                    let errors = '';

                    if (emailVal.val() === '') {
                        errors += 'Please enter your email.';
                        sendFlag = false;
                    }
                    else {
                        if (!Fractal.Helpers.validateEmail(emailVal.val())) {
                            errors += 'Email is not valid.';
                            sendFlag = false;
                        }
                    }

                    if (usrVal.val() === '') {
                        errors += 'Please enter your username.';
                        sendFlag = false;
                    }
                    else {
                        if (!usrVal.hasClass('valid')) {
                            errors += 'Username is already taken.';
                            sendFlag = false;
                        }
                    }

                    if (pwdVal.val() === '') {
                        errors += 'Please enter your password.';
                        sendFlag = false;
                    }
                    else {
                        if (!pwdVal.hasClass('valid')) {
                            errors += 'Password must be at least 6 characters.';
                            sendFlag = false;
                        }
                    }
                    if (sendFlag) {

                        self.serviceHandler.registration(emailVal.val(), usrVal.val(), pwdVal.val(), (response) => {
                            if (response.status !== 200 && !response.token) {
                            }
                            else {
                                self.saveUser(response);
                            }
                        }, () => {

                        });
                    }
                    else {
                        Fractal.Helpers.showPopup(errors);
                    }
                }
                else {
                    Fractal.Helpers.showPopup('Please check your internet connection.');
                }
            });

            if ($loginSignupTabs.length > 0) {
                $loginSignupTabs.removeClass('transition-toggle');
                TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);
            }

            $('.login-button-wrapper').off('click').on('click', (e) => {
                currentLoginState = 'initial';
                $('.signup-button-wrapper').removeClass('active');
                $loginSignupTabs.addClass('transition-toggle');
                $(e.currentTarget).addClass('active');
                TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);
            });

            $('.signup-button-wrapper').off('click').on('click', (e) => {
                currentLoginState = 'left';
                $('.login-button-wrapper').removeClass('active');
                $loginSignupTabs.addClass('transition-toggle');
                $(e.currentTarget).addClass('active');
                TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);
            });

            $('.forgotten-password-button').off('click').on('click', (e) => {
                currentLoginState = 'right';
                $loginSignupTabs.addClass('transition-toggle');
                TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);
            });

            self.socialFacebookLogin($('.socialaccount_provider_facebook'));


            $(".socialaccount_provider_google").off("click").on("click", () => {
                if ((<any>navigator).connection.type !== 'none') {
                    (<any>window).plugins.googleplus.login(
                        {},
                        (obj)=> {
                            Fractal.Helpers.rotateUntilCallback(()=> {
                                self.serviceHandler.googleLogin(obj, (r) => {
                                    self.logUserIn(r);
                                    Fractal.Helpers.removeLoader();
                                }, (r) => {
                                    Fractal.Helpers.showPopup("Social account's email is already registered.");
                                    Fractal.Helpers.removeLoader();
                                });
                            })
                        },
                        (msg) => {
                            Fractal.Helpers.showPopup("Something went wrong :( Please try again.");
                        }
                    );
                } else {
                    Fractal.Helpers.showPopup("Please check your internet connection.");
                }
            });

            $('.login-modal').find('.close-modal').off('click').on('click', () => {
                self.closeLogin();
            });

            TheColourGame.setLoginTransformations($loginSignupTabs, currentLoginState);

            callback();

        }

        logUserIn(response) {
            let self = this;
            self.saveUser(response)
        }

        convertImgToBase64URL(url, callback, outputFormat) {
            let img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = () => {
                let canvas = document.createElement('CANVAS');
                let ctx = (<any>canvas).getContext('2d');
                let dataURL;
                (<any>canvas).height = img.height;
                (<any>canvas).width = img.width;
                ctx.drawImage(img, 0, 0);
                dataURL = (<any>canvas).toDataURL(outputFormat);
                callback(dataURL);
                canvas = null;
                img = null;
            };
            img.src = url;
        }

        saveUserToLocalStorage() {
            let userToString = {
                username: this.username,
                total_iq: this.iq,
                level: this.level,
                next_level_iq: this.nextLevelIq,
                next_level_name: this.nextLevelName,
                prev_level_iq: this.prevLevelIq,
                prev_level_name: this.prevLevelName,
                user_avatar_url: this.user_avatar,
            };
            this.convertImgToBase64URL(userToString.user_avatar_url, (base64Img) => {
                userToString.user_avatar_url = base64Img;
                let stringifyResponse = JSON.stringify(userToString);
                localStorage.setItem('userInfo', stringifyResponse);
            }, 'png');
        }

        saveUser(user) {
            let self = this;
            let objToSave = {
                token: user.token
            };
            let stringifyResponse = JSON.stringify(objToSave);
            localStorage.setItem('userCred', stringifyResponse);

            self.serviceHandler.initalize();

            self.serviceHandler.shortAccountInfo((r)=> {
                self.updateUser(r.user);
                self.saveUserToLocalStorage();
                this.getHighScores();
                this.convertImgToBase64URL(self.user_avatar, (base64Img) => {
                    self.user_avatar = base64Img;
                    self.userInit();
                    self.closeLogin();
                    self.closeLeaderboard();
                }, 'png');
            }, (r)=> {
                // console.log(r);
            });
        }

        accountModalFunction() {
            let permitted = false;

            (<any>window).requestPermissions.requestCamera(function (r) {
                console.log("%c" + r, "color:green");
                permitted = true;
            }, function (r) {
                console.log("%c" + r, "color:red");
                permitted = false;
            }, "checkCameraPermission");

            $("#file").off('click').on('click', function (e) {
                if (!permitted) {
                    (<any>window).requestPermissions.requestCamera(function (r) {
                        permitted = true;
                        return false;
                    }, function (r) {
                        (<any>window).requestPermissions.requestCamera(function (r) {
                            permitted = true;
                            console.log("%c" + r, "color:green");
                            return false;
                        }, function (r) {
                            console.log("%c" + r, "color:red");
                            Fractal.Helpers.showPopup("You have not granted permission for photos. If you want to change your avatar you should allow it.");
                            return false;
                        }, "requestCameraPermission");
                        return false;
                    }, "checkCameraPermission");
                    if (!permitted) {
                        e.preventDefault();
                    }
                }
            });
            let self = this;
            let currentState = 'initial';
            // Saving account info ajax request
            $('.profile-edit-save').off('click').on('click', (e) => {
                if ((<any>navigator).connection.type !== 'none') {
                    let currentAvatar = $('.current-avatar');
                    let accountUsername = $('#username-edit');
                    let imageB64String = '';
                    let container = $(e.currentTarget);
                    if (currentAvatar.attr('changed') == '' || accountUsername.val() != '') {
                        container.find('.loading-rect').addClass('show-loading-g');
                        if (currentAvatar.attr('changed') == '') {
                            imageB64String = currentAvatar.attr('src');
                            imageB64String = imageB64String.toString();
                            imageB64String = imageB64String.replace('data:image/png;base64,', '');
                        }

                        let obj = {
                            username: accountUsername.val(),
                            image_base64: imageB64String
                        };
                        Fractal.Helpers.rotateUntilCallback((cubeInterval)=> {
                            self.serviceHandler.changeInfo(obj, (r)=> {
                                let newImg = new Image();
                                newImg.src = r.user.user_avatar_url;
                                newImg.onload = () => {
                                    self.updateUser(r.user);
                                    self.convertImgToBase64URL(self.user_avatar, (base64Img) => {
                                        self.user_avatar = base64Img;
                                        self.closeAccount();
                                        self.userInit();
                                        Fractal.Helpers.removeLoader();
                                        container.find('.loading-rect').removeClass('show-loading-g');
                                        newImg = null;
                                    }, 'png');

                                };

                            }, (r)=> {
                                let msg = r.responseText === '' ? "Please check your internet connection." : r.responseText;
                                self.closeAccount();
                                Fractal.Helpers.showPopup(msg);
                                Fractal.Helpers.removeLoader();
                                container.find('.loading-rect').removeClass('show-loading-g');
                            })
                        })
                    }
                    else {
                        if (currentAvatar.attr('changed') != '') {
                            currentAvatar.addClass('no-red-required');
                        }
                        if (accountUsername.val() == '') {
                            accountUsername.addClass('required');
                        }
                    }
                }
                else {
                    Fractal.Helpers.showPopup("Please check your internet connection.");
                }
            });

            $('.profile-logout').off('click').on('click', () => {
                localStorage.removeItem('userInfo');
                localStorage.removeItem('userCred');
                localStorage.removeItem('highScoreTarget');
                localStorage.removeItem('highScoreSurvival');
                localStorage.removeItem('highScoreCountdown');
                localStorage.removeItem('highScoreShootdown');
                if (TheColourGame.storeReady) {
                    localStorage.setItem('productBought', (<any>window).store.get('com.fractalgames.thecolourgame.noads.extramodes').owned);
                }
                else {
                    localStorage.removeItem('productBought');
                }
                self.updateUser();
                // (<DailyChallengeUser>self).updateUI();
                self.userInit();
                self.serviceHandler.removeToken();
                self.closeAccount();
                $('.login-overlay-wrapper').addClass('show');
                TheColourGame.setHighScoresInfo();
                TheColourGame.setHighScoresPlay();
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    let reader = new FileReader();
                    reader.onload = (e) => {
                        let $resizeImageSelector = $('.resize-image');
                        $('.current-avatar').removeClass('no-red-required');
                        currentState = 'right';
                        $('.half-account-cube-sides').addClass('transition-toggle');
                        setAccountTransformations($('#account-sections'), currentState);
                        $resizeImageSelector.attr('src', (<any>e).target.result);
                        resizeableImage($resizeImageSelector);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            };

            $('#file').change(function () {
                readURL(this);
            });


            $(window).resize(() => {
                $('.half-account-cube-sides').removeClass('transition-toggle');
                setAccountTransformations($('#account-sections'), currentState)
            });

            setAccountTransformations($('#account-sections'), currentState);
            Fractal.Helpers.iq_box_progress_account(self.iq, self.nextLevelIq);

            let resizeableImage = (imageTarget) => {
                // Some letiable and settings
                let $container;
                let orig_src = new Image();
                let image_target = $(imageTarget).get(0);

                let resize_canvas = document.createElement('canvas');
                let oldPosition:{x:number,y:number} = {
                    x: 0,
                    y: 0
                };
                let transform = {
                    translate: {x: 0, y: 0},
                    scale: 1,
                    angle: 0,
                    rx: 0,
                    ry: 0,
                    rz: 0
                };
                let lastX = 0, lastY = 0, lastScale = 1, lastRotation = 0;
                imageTarget = null;
                let deltaRotation = 0;
                let hamContainer;
                let updateElementTransform = (el) => {
                    let value = [
                        'translate3d(' + transform.translate.x + 'px, ' + transform.translate.y + 'px, 0)',
                        'scale(' + transform.scale + ', ' + transform.scale + ')',
                        'rotate3d(' + transform.rx + ',' + transform.ry + ',' + transform.rz + ',' + transform.angle + 'deg)'
                    ];

                    value = (<any>value).join(" ");
                    el.style.webkitTransform = value;
                    el.style.mozTransform = value;
                    el.style.transform = value;
                };

                let init = () => {
                    let image_div = $('.resize-image');
                    $('.cropping').append(image_div);
                    $('.resize-container').remove();
                    // When resizing, we will always use this copy of the original as the base
                    orig_src.src = (<any>image_target).src;

                    // Wrap the image with the container and add resize handles
                    $((<any>image_target)).wrap('<div class="resize-container" style="visibility:hidden"></div>')
                        .before('<span class="resize-handle resize-handle-nw"></span>')
                        .before('<span class="resize-handle resize-handle-ne"></span>')
                        .after('<span class="resize-handle resize-handle-se"></span>')
                        .after('<span class="resize-handle resize-handle-sw"></span>');

                    // Assign the container to a letiable
                    $container = $((<any>image_target)).parent('.resize-container');


                    updateElementTransform($container[0]);
                    // Pinch/Zoom, Rotate and Pan
                    hamContainer = new (<any>window).Hammer($container[0], {});

                    hamContainer.get('rotate').set({enable: true});
                    hamContainer.get('pan').set({direction: (<any>window).Hammer.DIRECTION_ALL});
                    hamContainer.get('pinch').set({enable: true});

                    hamContainer.on('pan', (ev)=> {
                        transform.translate = {
                            x: ev.deltaX + lastX,
                            y: ev.deltaY + lastY
                        };
                        updateElementTransform($container[0]);
                    });

                    hamContainer.on('panend', (ev) => {
                        lastX = ev.deltaX;
                        lastY = ev.deltaY;
                    });

                    hamContainer.on('pinch', (ev)=> {
                        transform.scale = lastScale * ev.scale;
                        updateElementTransform($container[0]);
                    });

                    hamContainer.on('pinchend', (ev) => {
                        lastScale = transform.scale;
                    });


                    hamContainer.on('rotate', (ev)=> {
                        transform.rz = 1;
                        deltaRotation = deltaRotation - ev.rotation;
                        transform.angle -= deltaRotation;
                        updateElementTransform($container[0]);

                        deltaRotation = ev.rotation;
                    });

                    hamContainer.on('rotatestart', (ev) => {
                        deltaRotation = ev.rotation;
                    });


                    // Add events
                    $('.js-crop').on('click', crop);

                    setTimeout(() => {
                        let $overlaySelector = $('.overlay');
                        let size = $overlaySelector.width();
                        resizeImage(size, size / orig_src.width * orig_src.height);
                        let overlay = $overlaySelector;
                        let imageSize = $('.resize-container');
                        setTimeout(() => {
                            imageSize.offset({
                                'left': (overlay.offset().left - ((imageSize.width() - overlay.width()) / 2)),
                                'top': (overlay.offset().top - ((imageSize.height() - overlay.height()) / 2))
                            });
                            $('.resize-container').css('visibility', 'visible');
                            oldPosition.x = $container.offset().left;
                            oldPosition.y = $container.offset().top;
                        }, 100);

                    }, 400);

                };

                let getMatrix = (obj) => {
                    return obj.css("-webkit-transform") ||
                        obj.css("-moz-transform") ||
                        obj.css("-ms-transform") ||
                        obj.css("-o-transform") ||
                        obj.css("transform");
                };

                let parseMatrix = (_str) => {
                    return _str.replace(/^matrix(3d)?\((.*)\)$/, '$2').split(/, /);
                };

                let getRotateDegrees = (obj) => {
                    var matrix = parseMatrix(getMatrix(obj)),
                        rotationAngle = 0;

                    if (matrix[0] !== 'none') {
                        var a = matrix[0],
                            b = matrix[1],
                            c = matrix[2],
                            d = matrix[3];
                        rotationAngle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
                    }

                    return rotationAngle;
                };

                let getScaleDegrees = (obj) => {
                    var matrix = parseMatrix(getMatrix(obj)),
                        scale = 1;

                    if (matrix[0] !== 'none') {
                        var a = matrix[0],
                            b = matrix[1];
                        scale = Math.sqrt(a * a + b * b);
                    }

                    return scale;
                };

                let resizeImage = (width, height) => {
                    resize_canvas.width = width;
                    resize_canvas.height = height;
                    resize_canvas.getContext('2d').drawImage(orig_src, 0, 0, width, height);
                    $((<any>image_target)).attr('src', resize_canvas.toDataURL('image/png'));
                };

                let drawRotatedImage = (context, image, x, y, angle, width, height, scale, difference) => {

                    context.save();

                    // Move registration point to the center of the canvas
                    context.translate(x + image.width / 2, y + image.height / 2);
                    context.rotate(angle * Math.PI / 180);// angle must be in radians
                    context.scale(scale, scale);
                    // Move registration point back to the top left corner of canvas
                    context.translate((-image.width) / 2, (-image.height) / 2);

                    context.drawImage(image, 0, 0);

                    context.restore();
                    //thx to dormouse
                };

                let crop = () => {
                    //Find the part of the image that is inside the crop box
                    let $overlaySelector = $('.overlay');

                    let imageScaleFactor = getScaleDegrees($container);
                    let imageDegrees = getRotateDegrees($container);

                    // console.log(imageDegrees);
                    let difference:{x:number,y:number} = {
                        x: oldPosition.x - $container.offset().left,
                        y: oldPosition.y - $container.offset().top
                    };

                    // }
                    let crop_canvas, left = -($overlaySelector.offset().left - transform.translate.x - oldPosition.x),
                        top = -($overlaySelector.offset().top - transform.translate.y - oldPosition.y),
                        width = $overlaySelector.width(),
                        height = $overlaySelector.height();

                    crop_canvas = document.createElement('canvas');
                    let context = crop_canvas.getContext('2d');
                    // context.save();

                    crop_canvas.width = width;
                    crop_canvas.height = height;

                    drawRotatedImage(context, (<any>image_target), left, top, imageDegrees, width, height, imageScaleFactor, difference);

                    $('.current-avatar').attr('src', crop_canvas.toDataURL('image/png')).attr('changed', '');

                    currentState = 'initial';
                    setAccountTransformations($('#account-sections'), currentState);
                    hamContainer = null;
                };

                init();
            };
            $('input').off('keyup').on('keyup', (e) => {
                $(e.currentTarget).removeClass('required');
            });

            $('.account-modal').find('.close-modal').off('click').on('click', () => {
                self.closeAccount();
            });


        }

        closeLeaderboard() {
            let _$leaderboardWrapperModal = $('.leaderboard-wrapper-modal')
            let _$buttons = $('.leaderboard-button');
            _$buttons.removeClass('active');
            _$leaderboardWrapperModal.addClass('not-visible');
            _$leaderboardWrapperModal.find('.leaderboard-modal-wrapper-sides').removeClass('show-right').addClass('show-front');
            $('body').removeClass('modal-open');
        }

        closeAccount() {
            $('.loading-rect').removeClass('greated-index');
            $('.account-wrapper-modal').addClass('not-visible');
            $('body').removeClass('modal-open');

            $('.resize-container').css('visibility', 'hidden');
            $('.half-account-cube-sides').removeClass('transition-toggle');
            let currentState = 'initial';
            setAccountTransformations($('#account-sections'), currentState);
            $('.account-wrapper input').each((index, el) => {
                $(el).val('').removeClass('required').removeClass('no-red-required')
            });
            $('.login-overlay').removeClass('show');
        }

        closeLogin() {
            $('.missing-email').removeClass('active-form');
            $('#forgotten-email-form').removeClass('active-form');
            $('.tabs-3 li').removeClass('active');
            $('.login-wrapper-modal').addClass('not-visible');
            $('#login-signup-tabs').removeClass('transition-toggle');
            let currentState = 'initial';
            $('body').removeClass('modal-open');
            TheColourGame.setLoginTransformations($('#login-signup-tabs'), currentState);
            $('.login-wrapper input').each((index, el) => {
                $(el).val('').removeClass('required').removeClass('no-red-required')
            });
            $('.login-overlay').removeClass('show');
        }


        static isLoggedIn() {
            let jsonUserInfo = localStorage.getItem('userCred');
            return jsonUserInfo != null;
        }

        static getSavedCred() {
            let jsonUserInfo = localStorage.getItem('userCred');
            if (jsonUserInfo != null) {
                jsonUserInfo = JSON.parse(jsonUserInfo);
                return jsonUserInfo;
            }
            return null;
        }

        static getSavedInfo() {
            let jsonUserInfo = localStorage.getItem('userInfo');
            if (jsonUserInfo != null) {
                jsonUserInfo = JSON.parse(jsonUserInfo);
                return jsonUserInfo;
            }
            return null;
        }
    }
}
