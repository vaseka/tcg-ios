///<reference path="../../Lib/Typings/jquery/jquery.d.ts"/>
///<reference path="../../Lib/Custom/mainReady.ts"/>
module Fractal {
    export class Product {
        id:string;
        alias:string;
        productType:string;

        constructor(id:string, alias:string, productType:string) {
            this.id = id;
            this.alias = alias;
            this.productType = productType;

            this.register();
            this.onBuyAndError();
        }

        register() {
            let self = this;
            (<any>window).store.register({
                id: self.id,
                alias: self.alias,
                type: self.productType
            });

            (<any>window).store.when(self.id).approved(function (product) {
                product.finish();
            });

        }

        onBuyAndError() {
            let self = this;
            (<any>window).store.when(self.id).owned(function (product) {
                // if ((<any>navigator).connection.type !== 'none') {
                    self.unlockAndSave();
                // };
                self.handleProServiceCheck(product);
            });
            (<any>window).store.when(self.id).error(function (product) {
                console.error(product);
            })
        }

        handleProServiceCheck(nonConProduct?) {
            if (!nonConProduct) {
                nonConProduct = (<any>window).store.get('noadsextramodes');
            }
            let self = this;
            let serviceTransactionSaved = JSON.parse(localStorage.getItem('serviceTransactionSaved'));
            if (TheColourGame.accountHandler.user && !TheColourGame.accountHandler.user.anonymous) {
                TheColourGame.accountHandler.serviceHandler.checkServiceProBought(TheColourGame.GAME_IDENTIFIER, (r)=> {
                    let is_pro = r.is_pro;
                    if (is_pro) {
                        self.unlockAndSave();
                    }
                    else {
                        self.lockAndSave();
                        if (nonConProduct.owned && nonConProduct.transaction) {
                            let transactionInfo = JSON.parse(nonConProduct.transaction.receipt);
                            TheColourGame.accountHandler.serviceHandler.buyNonConsumableItem(TheColourGame.GAME_IDENTIFIER,
                                transactionInfo.productId, transactionInfo.purchaseToken,
                                (r)=> {
                                    self.unlockAndSave();
                                    console.info(r);
                                },
                                (r)=> {
                                    self.lockAndSave();
                                    console.error(r);
                                });
                        }
                    }
                }, (r)=> {
                    console.error(r);
                });
            }
            else {
                if (self.checkBought()) {
                    self.unlockAndSave();
                }
                else {
                    self.lockAndSave();
                }
            }
        }

        lockAndSave() {
            let self = this;
            let modeButtons = $('.mode-buttons');
            modeButtons.eq(2).removeClass('unlocked');
            modeButtons.eq(3).removeClass('unlocked');
            localStorage.setItem('productBought', 'false');
        }

        unlockAndSave() {
            let self = this;
            $('.buy-extra-mods-wrapper').removeClass('show-buy');
            $('.mode-buttons').addClass('unlocked');
            localStorage.setItem('productBought', 'true');
        }

        checkBought() {
            let self = this;
            if (TheColourGame.storeReady) {
                if (JSON.parse(localStorage.getItem("productBought"))){
                    return JSON.parse(localStorage.getItem("productBought"));
                }
                return (<any>window).store.get(self.id).owned;
            }
            else {
                return JSON.parse(localStorage.getItem("productBought"));
            }
        }

        buy() {
            (<any>window).store.order(this.id);
        }
    }
}