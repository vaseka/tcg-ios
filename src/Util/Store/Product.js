///<reference path="../../Lib/Typings/jquery/jquery.d.ts"/>
///<reference path="../../Lib/Custom/mainReady.ts"/>
var Fractal;
(function (Fractal) {
    var Product = (function () {
        function Product(id, alias, productType) {
            this.id = id;
            this.alias = alias;
            this.productType = productType;
            this.register();
            this.onBuyAndError();
        }
        Product.prototype.register = function () {
            var self = this;
            window.store.register({
                id: self.id,
                alias: self.alias,
                type: self.productType
            });
            window.store.when(self.id).approved(function (product) {
                product.finish();
            });
        };
        Product.prototype.onBuyAndError = function () {
            var self = this;
            window.store.when(self.id).owned(function (product) {
                // if ((<any>navigator).connection.type !== 'none') {
                self.unlockAndSave();
                // };
                self.handleProServiceCheck(product);
            });
            window.store.when(self.id).error(function (product) {
                console.error(product);
            });
        };
        Product.prototype.handleProServiceCheck = function (nonConProduct) {
            if (!nonConProduct) {
                nonConProduct = window.store.get('noadsextramodes');
            }
            var self = this;
            var serviceTransactionSaved = JSON.parse(localStorage.getItem('serviceTransactionSaved'));
            if (TheColourGame.accountHandler.user && !TheColourGame.accountHandler.user.anonymous) {
                TheColourGame.accountHandler.serviceHandler.checkServiceProBought(TheColourGame.GAME_IDENTIFIER, function (r) {
                    var is_pro = r.is_pro;
                    if (is_pro) {
                        self.unlockAndSave();
                    }
                    else {
                        self.lockAndSave();
                        if (nonConProduct.owned && nonConProduct.transaction) {
                            var transactionInfo = JSON.parse(nonConProduct.transaction.receipt);
                            TheColourGame.accountHandler.serviceHandler.buyNonConsumableItem(TheColourGame.GAME_IDENTIFIER, transactionInfo.productId, transactionInfo.purchaseToken, function (r) {
                                self.unlockAndSave();
                                console.info(r);
                            }, function (r) {
                                self.lockAndSave();
                                console.error(r);
                            });
                        }
                    }
                }, function (r) {
                    console.error(r);
                });
            }
            else {
                if (self.checkBought()) {
                    self.unlockAndSave();
                }
                else {
                    self.lockAndSave();
                }
            }
        };
        Product.prototype.lockAndSave = function () {
            var self = this;
            var modeButtons = $('.mode-buttons');
            modeButtons.eq(2).removeClass('unlocked');
            modeButtons.eq(3).removeClass('unlocked');
            localStorage.setItem('productBought', 'false');
        };
        Product.prototype.unlockAndSave = function () {
            var self = this;
            $('.buy-extra-mods-wrapper').removeClass('show-buy');
            $('.mode-buttons').addClass('unlocked');
            localStorage.setItem('productBought', 'true');
        };
        Product.prototype.checkBought = function () {
            var self = this;
            if (TheColourGame.storeReady) {
                if (JSON.parse(localStorage.getItem("productBought"))) {
                    return JSON.parse(localStorage.getItem("productBought"));
                }
                return window.store.get(self.id).owned;
            }
            else {
                return JSON.parse(localStorage.getItem("productBought"));
            }
        };
        Product.prototype.buy = function () {
            window.store.order(this.id);
        };
        return Product;
    }());
    Fractal.Product = Product;
})(Fractal || (Fractal = {}));
