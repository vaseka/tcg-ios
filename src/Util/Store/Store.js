/**
 * Created by Ivaylo Hristov on 17.9.2015 �..
 * bahti boga
 */
///<reference path="Product.ts"/>
///<reference path="../../Lib/Custom/mainReady.ts"/>
var Fractal;
(function (Fractal) {
    var Store = (function () {
        function Store() {
            window.store.ready(function () {
                TheColourGame.storeReady = true;
                console.info('Store ready \\m/');
            });
            window.store.error(function (error) {
                if (error.code === 7) {
                    navigator.notification.alert("This item has already been purchased on this device. To buy it again please change the google account on your phone.", null, 'Buy Extra Modes and Remove Ads.', 'OK');
                }
                else if (navigator.connection.type !== 'none') {
                    navigator.notification.alert("An error occurred. Please try again later.", null, 'Buy Extra Modes and Remove Ads.', 'OK');
                }
            });
            this.products = [];
        }
        Store.prototype.addProduct = function (id, alias, type) {
            var product = new Fractal.Product(id, alias, type);
            this.products.push(product);
        };
        Store.prototype.getProduct = function (id) {
            var self = this;
            var product = null;
            for (var i = 0; i < self.products.length; i++) {
                if (self.products[i].id === id) {
                    product = self.products[i];
                }
            }
            return product;
        };
        Store.prototype.load = function () {
            window.store.refresh();
        };
        return Store;
    }());
    Fractal.Store = Store;
})(Fractal || (Fractal = {}));
