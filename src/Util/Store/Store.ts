/**
 * Created by Ivaylo Hristov on 17.9.2015 �..
 * bahti boga
 */
///<reference path="Product.ts"/>
///<reference path="../../Lib/Custom/mainReady.ts"/>
module Fractal {
    export class Store {
        products:Array<Product>;

        constructor() {
            (<any>window).store.ready(function () {
                TheColourGame.storeReady = true;
                console.info('Store ready \\m/');
            });
            (<any>window).store.error(function (error) {
                if (error.code === 7) {
                    (<any>navigator).notification.alert("This item has already been purchased on this device. To buy it again please change the google account on your phone.", null, 'Buy Extra Modes and Remove Ads.', 'OK');
                }

                else if ((<any>navigator).connection.type !== 'none') {
                    (<any>navigator).notification.alert("An error occurred. Please try again later.", null, 'Buy Extra Modes and Remove Ads.', 'OK');
                }
            });
            this.products = [];
        }

        addProduct(id:string, alias:string, type:string) {
            let product = new Fractal.Product(id, alias, type);
            this.products.push(product);
        }

        getProduct(id:string) {
            let self = this;
            let product = null;
            for (let i = 0; i < self.products.length; i++) {
                if (self.products[i].id === id) {
                    product = self.products[i];
                }
            }
            return product;
        }

        load() {
            (<any>window).store.refresh();
        }
    }
}