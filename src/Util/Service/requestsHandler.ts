///<reference path='../../Lib/typings/jquery/jquery.d.ts'/>
///<reference path='../User/fractalUser.ts'/>
///<reference path='../constants.ts'/>

module Fractal {
    export class RequestsHandler {
        token:string;

        constructor() {
        }

        initalize() {
            let user = Fractal.User.getSavedCred();
            if (user) {
                $.ajaxSetup(<JQueryAjaxSettings>{
                    headers: {
                        'Authorization': 'Token ' + user.token
                    }
                });
                this.token = user.token;
                console.log("User Token: " + user.token);
            }
            else {
                console.warn('User is not logged in. Token not put in ajax request.')
            }
        }

        removeToken() {
            this.token = null;
            delete $.ajaxSettings.headers["Authorization"];
            // $.ajaxSetup(<JQueryAjaxSettings>{
            //     headers: {
            //         Authorization: ""
            //     }
            // });
        }

        insertToken() {
            this.initalize();
        }


        login(username, password, successCallback, errorCallback) {
            let requestData = {
                username: username,
                password: password
            };

            $.ajax({
                type: 'POST',
                //crossDomain: true,
                url: Fractal.MAIN_URL + Fractal.LOGIN_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: (response) => {
                    successCallback(response);
                },
                error: (response) => {
                    errorCallback(response);
                }
            })
        }

        shortAccountInfo(successCallback, errorCallback) {
            $.ajax({
                type: 'GET',
                url: Fractal.MAIN_URL + Fractal.SHORT_ACCOUNT_INFO_URL,
                contentType: 'application/json; charset=utf-8',
                success: (response) => {
                    successCallback(response);
                },
                error: (response) => {
                    errorCallback(response);
                }
            })
        }

        registration(email, username, password, successCallback, errorCallback) {
            let requestData = {
                email: email,
                username: username,
                password1: password,
                password2: password
            };
            //console.log(JSON.stringify(requestData));
            $.ajax({

                type: 'POST',
                crossDomain: true,
                url: Fractal.MAIN_URL + Fractal.REGISTRATION_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: (response) => {
                    successCallback(response);
                },
                error: (response) => {
                    errorCallback(response);
                }
            });
        }

        forgottenPassword(email, successCallback, errorCallback) {
            let requestData = {
                email: email
            };
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: Fractal.MAIN_URL + Fractal.FORGOTTEN_PASSWORD_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: (response) => {
                    successCallback(response);
                },
                error: (response) => {
                    errorCallback(response);
                }
            })
        }

        changeInfo(obj, successCallback, errorCallback) {
            $.ajax({
                type: 'PUT',
                url: Fractal.MAIN_URL + CHANGE_INFO,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(obj),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                },
                dataType: ''
            })
        }

        checkUsername(username, callback) {
            let obj = {
                username: username
            };

            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.CHECK_USERNAME_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(obj),
                success: (response) => {
                    callback(response);
                },
                error: (response) => {
                    callback(response);
                },
                dataType: ''
            });
        }

        checkEmail(email, callback) {
            let obj = {
                email: email
            };

            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.CHECK_EMAIL_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(obj),
                success: (response) => {
                    callback(response);
                },
                error: (response) => {
                    callback(response);
                },
                dataType: ''
            });
        }

        sendScore(score:string, successCallback, errorCallback) {
            console.info('Sending score...');
            let encryptedData = (<any>window).encrypt(score, (<any>window).CryptoJS.enc.Utf8.parse(this.token.substring(0, 16)));
            let data = {
                enc: encryptedData,
                game_identifier: 'the-colour-game'
            };
            $.ajax({
                type: 'POST',
                url: MAIN_URL + POST_SCORE,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                // timeout: 5000,
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            })
        }

        sendHighScore(score:string, modeIdentifier:string, successCallback, errorCallback) {
            console.info('Sending high score...');
            let encryptedData = (<any>window).encrypt(score, (<any>window).CryptoJS.enc.Utf8.parse(this.token.substring(0, 16)));
            let data = {
                enc: encryptedData,
                game_identifier: TheColourGame.GAME_IDENTIFIER,
                mode_identifier: modeIdentifier
            };
            $.ajax({
                type: 'POST',
                url: MAIN_URL + POST_HIGH_SCORE,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                // timeout: 5000,
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            })
        }

        getHighScores(successCallback, errorCallback) {
            let data = {
                game_identifier: TheColourGame.GAME_IDENTIFIER,
            };
            $.ajax({
                type: 'POST',
                url: MAIN_URL + GET_HIGH_SCORES,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            })
        }

        getGlobalHighScoreLeaderboard(page:number, successCallback:Function, errorCallback:Function) {
            // console.info('Getting global Colour Game high scores ...');
            let data = {
                "page": page,
                "game_identifier": TheColourGame.GAME_IDENTIFIER

            };
            $.ajax({
                type: 'POST',
                url: MAIN_URL + GLOBAL_HIGH_SCORES,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                success: (r) => {
                    // console.info("Recieved global high score leaderboard.");
                    successCallback(r);
                },
                error: (r) => {
                    console.error("There was an error getting the global high scores.");
                    errorCallback(r);
                }
            })
        }

        getFriendsHighScoreLeaderboard(page:number, friendsList:Array<Object>, successCallback:Function, errorCallback:Function) {
            // console.info('Getting friends Colour Game high scores ...');
            let data = {
                "page": page,
                "friends_list": friendsList,
                "game_identifier": TheColourGame.GAME_IDENTIFIER

            };
            $.ajax({
                type: 'POST',
                url: MAIN_URL + FRIENDS_HIGH_SCORES,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                success: (r) => {
                    successCallback(r);
                },
                error: (r) => {
                    errorCallback(r);
                }
            })
        }

        checkUserInfo(successCallback, errorCallback) {
            $.ajax({
                type: 'GET',
                crossDomain: true,
                url: Fractal.MAIN_URL + USER_INFO,
                contentType: 'application/json; charset=utf-8',
                success: (response) => {
                    successCallback(response);
                },
                error: (response) => {
                    errorCallback(response);
                },
                statusCode: {
                    401: (response) => {
                        errorCallback(response);
                    }
                }
            })
        }

        facebookLogin(accessToken, successCallback, errorCallback) {
            let requestData = {
                access_token: accessToken
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.FACEBOOK_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: function (response) {
                    successCallback(response);
                    //}, 'image/png');
                },

                error: function (response) {
                    errorCallback(response);
                }
            });
        }

        googleLogin(requestData, successCallback, errorCallback) {
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.GOOGLE_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        }

        buyNonConsumableItem(gameIdentifier, productId, token, successCallback, errorCallback) {
            let data = {
                game_identifier: gameIdentifier,
                package_name: TheColourGame.PACKAGE_NAME,
                product_id: productId,
                token: token
            };
            $.ajax({
                type: "POST",
                url: MAIN_URL + BUY_GAME_URL,
                headers: {
                    "Authorization": "Token " + this.token
                },
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            })
        }

        checkServiceProBought(gameIdentifier, successCallback, errorCallback) {
            let data = {
                game_identifier: gameIdentifier,
            };
            $.ajax({
                type: "POST",
                url: MAIN_URL + CHECK_GAME_BOUGHT_URL,
                headers: {
                    "Authorization": "Token " + this.token
                },
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            })
        }

    }
}
