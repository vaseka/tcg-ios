///<reference path='../../Lib/typings/jquery/jquery.d.ts'/>
///<reference path='../User/fractalUser.ts'/>
///<reference path='../constants.ts'/>
var Fractal;
(function (Fractal) {
    var RequestsHandler = (function () {
        function RequestsHandler() {
        }
        RequestsHandler.prototype.initalize = function () {
            var user = Fractal.User.getSavedCred();
            if (user) {
                $.ajaxSetup({
                    headers: {
                        'Authorization': 'Token ' + user.token
                    }
                });
                this.token = user.token;
                console.log("User Token: " + user.token);
            }
            else {
                console.warn('User is not logged in. Token not put in ajax request.');
            }
        };
        RequestsHandler.prototype.removeToken = function () {
            this.token = null;
            delete $.ajaxSettings.headers["Authorization"];
            // $.ajaxSetup(<JQueryAjaxSettings>{
            //     headers: {
            //         Authorization: ""
            //     }
            // });
        };
        RequestsHandler.prototype.insertToken = function () {
            this.initalize();
        };
        RequestsHandler.prototype.login = function (username, password, successCallback, errorCallback) {
            var requestData = {
                username: username,
                password: password
            };
            $.ajax({
                type: 'POST',
                //crossDomain: true,
                url: Fractal.MAIN_URL + Fractal.LOGIN_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.shortAccountInfo = function (successCallback, errorCallback) {
            $.ajax({
                type: 'GET',
                url: Fractal.MAIN_URL + Fractal.SHORT_ACCOUNT_INFO_URL,
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.registration = function (email, username, password, successCallback, errorCallback) {
            var requestData = {
                email: email,
                username: username,
                password1: password,
                password2: password
            };
            //console.log(JSON.stringify(requestData));
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: Fractal.MAIN_URL + Fractal.REGISTRATION_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.forgottenPassword = function (email, successCallback, errorCallback) {
            var requestData = {
                email: email
            };
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: Fractal.MAIN_URL + Fractal.FORGOTTEN_PASSWORD_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.changeInfo = function (obj, successCallback, errorCallback) {
            $.ajax({
                type: 'PUT',
                url: Fractal.MAIN_URL + Fractal.CHANGE_INFO,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(obj),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                },
                dataType: ''
            });
        };
        RequestsHandler.prototype.checkUsername = function (username, callback) {
            var obj = {
                username: username
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.CHECK_USERNAME_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(obj),
                success: function (response) {
                    callback(response);
                },
                error: function (response) {
                    callback(response);
                },
                dataType: ''
            });
        };
        RequestsHandler.prototype.checkEmail = function (email, callback) {
            var obj = {
                email: email
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.CHECK_EMAIL_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(obj),
                success: function (response) {
                    callback(response);
                },
                error: function (response) {
                    callback(response);
                },
                dataType: ''
            });
        };
        RequestsHandler.prototype.sendScore = function (score, successCallback, errorCallback) {
            console.info('Sending score...');
            var encryptedData = window.encrypt(score, window.CryptoJS.enc.Utf8.parse(this.token.substring(0, 16)));
            var data = {
                enc: encryptedData,
                game_identifier: 'the-colour-game'
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.POST_SCORE,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                // timeout: 5000,
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.sendHighScore = function (score, modeIdentifier, successCallback, errorCallback) {
            console.info('Sending high score...');
            var encryptedData = window.encrypt(score, window.CryptoJS.enc.Utf8.parse(this.token.substring(0, 16)));
            var data = {
                enc: encryptedData,
                game_identifier: TheColourGame.GAME_IDENTIFIER,
                mode_identifier: modeIdentifier
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.POST_HIGH_SCORE,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                // timeout: 5000,
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.getHighScores = function (successCallback, errorCallback) {
            var data = {
                game_identifier: TheColourGame.GAME_IDENTIFIER
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.GET_HIGH_SCORES,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.getGlobalHighScoreLeaderboard = function (page, successCallback, errorCallback) {
            // console.info('Getting global Colour Game high scores ...');
            var data = {
                "page": page,
                "game_identifier": TheColourGame.GAME_IDENTIFIER
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.GLOBAL_HIGH_SCORES,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                success: function (r) {
                    // console.info("Recieved global high score leaderboard.");
                    successCallback(r);
                },
                error: function (r) {
                    console.error("There was an error getting the global high scores.");
                    errorCallback(r);
                }
            });
        };
        RequestsHandler.prototype.getFriendsHighScoreLeaderboard = function (page, friendsList, successCallback, errorCallback) {
            // console.info('Getting friends Colour Game high scores ...');
            var data = {
                "page": page,
                "friends_list": friendsList,
                "game_identifier": TheColourGame.GAME_IDENTIFIER
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.FRIENDS_HIGH_SCORES,
                headers: {
                    'Authorization': 'Token ' + this.token
                },
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                success: function (r) {
                    successCallback(r);
                },
                error: function (r) {
                    errorCallback(r);
                }
            });
        };
        RequestsHandler.prototype.checkUserInfo = function (successCallback, errorCallback) {
            $.ajax({
                type: 'GET',
                crossDomain: true,
                url: Fractal.MAIN_URL + Fractal.USER_INFO,
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                },
                statusCode: {
                    401: function (response) {
                        errorCallback(response);
                    }
                }
            });
        };
        RequestsHandler.prototype.facebookLogin = function (accessToken, successCallback, errorCallback) {
            var requestData = {
                access_token: accessToken
            };
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.FACEBOOK_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: function (response) {
                    successCallback(response);
                    //}, 'image/png');
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.googleLogin = function (requestData, successCallback, errorCallback) {
            $.ajax({
                type: 'POST',
                url: Fractal.MAIN_URL + Fractal.GOOGLE_URL,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(requestData),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.buyNonConsumableItem = function (gameIdentifier, productId, token, successCallback, errorCallback) {
            var data = {
                game_identifier: gameIdentifier,
                package_name: TheColourGame.PACKAGE_NAME,
                product_id: productId,
                token: token
            };
            $.ajax({
                type: "POST",
                url: Fractal.MAIN_URL + Fractal.BUY_GAME_URL,
                headers: {
                    "Authorization": "Token " + this.token
                },
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        RequestsHandler.prototype.checkServiceProBought = function (gameIdentifier, successCallback, errorCallback) {
            var data = {
                game_identifier: gameIdentifier
            };
            $.ajax({
                type: "POST",
                url: Fractal.MAIN_URL + Fractal.CHECK_GAME_BOUGHT_URL,
                headers: {
                    "Authorization": "Token " + this.token
                },
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                success: function (response) {
                    successCallback(response);
                },
                error: function (response) {
                    errorCallback(response);
                }
            });
        };
        return RequestsHandler;
    }());
    Fractal.RequestsHandler = RequestsHandler;
})(Fractal || (Fractal = {}));
