///<reference path="../Objects/Board.ts"/>
///<reference path="../Objects/UI.ts"/>
///<reference path="../../Lib/Typings/phaser/phaser.d.ts"/>
///<reference path="../../ColourGame.ts"/>
///<reference path="../../Util/constants.ts"/>
///<reference path="../../Util/helpers.ts"/>
///<reference path="../../Lib/Custom/mainReady.ts"/>
///<reference path="../../Util/POFG.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TheColourGame;
(function (TheColourGame) {
    var Play = (function (_super) {
        __extends(Play, _super);
        function Play() {
            _super.apply(this, arguments);
        }
        Play.prototype.init = function (gameMode) {
            this.gameMode = gameMode;
        };
        Play.prototype.preload = function () {
            this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.game.stage.disableVisibilityChange = true;
            this.game.time.advancedTiming = false;
            this.game.forceSingleUpdate = true;
            this.game.load.image("gameLogoSprite", "Assets/ingameSprites/gameLogoSprite.png");
            this.game.load.image("refreshSprite", "Assets/ingameSprites/refreshSprite.png");
            this.game.load.image("shadowSprite", "Assets/ingameSprites/shadowSprite.png");
            this.game.load.image("tickSprite", "Assets/ingameSprites/tickSprite.png");
            this.game.load.image("timeSprite", "Assets/ingameSprites/timeSprite.png");
            this.game.load.image("xSprite", "Assets/ingameSprites/xSprite.png");
        };
        ;
        Play.prototype.create = function () {
            var _this = this;
            this.POFG = new TheColourGame.POFG(this.game);
            this.orientationChangeLocked = true;
            this.game.time.events.add(TheColourGame.ORIENTATION_CHANGE_MINIMAL_INTERVAL, function () {
                _this.orientationChangeLocked = false;
            });
            if (window.innerHeight > window.innerWidth) {
                this.orientation = TheColourGame.ORIENTATIONS.PORTRAIT;
            }
            else {
                this.orientation = TheColourGame.ORIENTATIONS.LANDSCAPE;
            }
            this.timePassed = 0;
            this.board = new TheColourGame.Board(this.game);
            this.loadGameObjects();
            this.UI = new TheColourGame.UI(this.game, this.timer, this.pointsTrackerStartingValue, this.board.boardImage.width, this.gameMode, this);
            this.levelIsRepeating = false;
            this.addInputSprites();
            window.onresize = function () {
                _this.game.paused = false;
                if (!_this.orientationChangeLocked) {
                    _this.orientationChangeLocked = true;
                    _this.game.time.events.add(TheColourGame.ORIENTATION_CHANGE_MINIMAL_INTERVAL, function () {
                        _this.orientationChangeLocked = false;
                    });
                    _this.game.time.events.add(50, function () {
                        if ((window.innerHeight > window.innerWidth && _this.orientation === TheColourGame.ORIENTATIONS.LANDSCAPE) || (window.innerHeight < window.innerWidth && _this.orientation === TheColourGame.ORIENTATIONS.PORTRAIT)) {
                            _this.changeOrientation();
                        }
                    });
                }
            };
            this.game.showContainer();
        };
        ;
        Play.prototype.update = function () {
            this.POFG.update();
        };
        Play.prototype.destroyState = function () {
            this.board.destroy();
            this.UI.destroy();
            this.score = null;
            this.level = null;
            this.timer = null;
            if (this.timersUpdater) {
                this.timersUpdater.destroy();
                this.timersUpdater = null;
            }
            this.correctInputSprite.destroy();
            this.correctInputSprite = null;
            this.inputSprite.destroy();
            this.inputSprite = null;
            this.levelIsRepeating = null;
            this.target = null;
            this.pointsTrackerStartingValue = null;
            this.progressOnTarget = null;
            this.pointsTrackerStartingValue = null;
            this.wrongClickCounter = null;
            this.gameMode = null;
            window.onresize = function () {
            };
            window.onresize = null;
            this.timePassed = null;
            this.POFG.destroy();
            this.POFG = null;
        };
        ;
        Play.prototype.restartGame = function () {
            var stateToStart = this.gameMode;
            this.destroyState();
            this.game.paused = false;
            this.game.state.start(this.game.state.current, true, false, stateToStart);
        };
        ;
        Play.prototype.sendHighScores = function () {
            if (navigator.connection.type !== 'none') {
                var modes = ['highScoreSurvival', 'highScoreShootdown', 'highScoreTarget', 'highScoreCountdown'];
                for (var i = 0; i < modes.length; i++) {
                    var modeScore = localStorage.getItem(modes[i]);
                    if (modeScore) {
                        var modeIdentifier = modes[i].replace('highScore', '');
                        modeIdentifier = modeIdentifier.charAt(0).toLowerCase() + modeIdentifier.slice(1);
                        TheColourGame.accountHandler.serviceHandler.sendHighScore(modeScore.toString(), modeIdentifier, function (r) {
                        }, function (r) {
                            console.error(r.responseJSON.detail);
                        });
                    }
                }
            }
        };
        Play.prototype.quitGame = function () {
            var self = this;
            this.game.paused = true;
            this.game.time.removeAll();
            var currentMode = this.gameMode.charAt(0).toUpperCase() + this.gameMode.slice(1);
            var currentHighScore = parseInt(localStorage.getItem("highScore" + currentMode));
            var score = 0;
            var _isHighScoreMade = false;
            if (TheColourGame.adCounter === TheColourGame.randomAdShow) {
                TheColourGame.adCounter = 0;
                TheColourGame.randomAdShow = Math.floor(Math.random() * 2) + 1;
                if (!TheColourGame.storeCG.products[0].checkBought()) {
                    TheColourGame.heyzap.showInterstitialAd();
                }
            }
            else {
                TheColourGame.adCounter++;
            }
            if (!currentHighScore || this.score > currentHighScore) {
                localStorage.setItem("highScore" + currentMode, this.score.toString());
                _isHighScoreMade = true;
            }
            if (TheColourGame.accountHandler) {
                if (!TheColourGame.accountHandler.user.anonymous) {
                    var previousScore = parseInt(localStorage.getItem('previousScore'));
                    if (isNaN(previousScore)) {
                        previousScore = 0;
                    }
                    score = previousScore + self.score;
                    TheColourGame.accountHandler.user.updateIQ(score);
                    if (navigator.connection.type !== 'none') {
                        if (_isHighScoreMade) {
                            this.sendHighScores();
                        }
                        TheColourGame.accountHandler.serviceHandler.sendScore(score.toString(), function (r) {
                        }, function (r) {
                        });
                    }
                    else {
                        localStorage.setItem('previousScore', score.toString());
                    }
                }
            }
            // TheColourGame.showOverlays();
            $('.gameOver').addClass("blocked");
            $('.gameOverLayout').addClass("blocked");
            $('.pointsNumber').html(this.score.toString());
            $('.timerNumber').html(Math.round(this.timePassed).toString());
            $('.wrongNumber').html(this.wrongClickCounter.toString());
            $('.iqPoints').html(this.score.toString());
            $('.back').off().on("click", function () {
                $('.gameOver').removeClass("blocked");
                self.game.hideContainer(function () {
                    $('.gameOverLayout').removeClass("blocked");
                    self.destroyState();
                });
            });
            $('.leaderboard').off().on("click", function () {
                TheColourGame.accountHandler.user.leaderboardModalRequest();
            });
            $(".again").off().on("click", function () {
                $('.gameOver').removeClass("blocked");
                $('.gameOverLayout').removeClass("blocked");
                self.restartGame();
                // TheColourGame.hideOverlays();
            });
            $(".share").off().on('click', function () {
                window.plugins.socialsharing.share("I scored " + self.score + " points playing The Colour Game. Try and beat me!", "Fractal Games The Colour Game", null, 'www.fractalgames.com');
            });
        };
        ;
        Play.prototype.addInputSprites = function () {
            var _this = this;
            this.inputSprite = this.game.add.sprite(this.board.boardImage.x, this.board.boardImage.y);
            this.inputSprite.width = this.board.boardImage.width;
            this.inputSprite.height = this.board.boardImage.height;
            this.inputSprite.anchor.set(0.5);
            this.inputSprite.alpha = 0;
            this.inputSprite.inputEnabled = true;
            this.inputSprite.input.priorityID = 0;
            this.inputSprite.events.onInputDown.add(function () {
                _this.handleWrongInput();
            });
            this.correctInputSprite = this.game.add.sprite(this.board.chosenTilePosition.x, this.board.chosenTilePosition.y);
            this.correctInputSprite.width = this.board.tileSideLength;
            this.correctInputSprite.height = this.board.tileSideLength;
            this.correctInputSprite.anchor.set(0.5);
            this.correctInputSprite.alpha = 0;
            this.correctInputSprite.inputEnabled = true;
            this.correctInputSprite.input.priorityID = 1;
            this.correctInputSprite.events.onInputDown.add(function () {
                _this.changeLevel();
            });
        };
        ;
        Play.prototype.handleWrongInput = function () {
            if (!this.timersUpdater) {
                this.createCountDownTimer();
            }
            this.wrongClickCounter++;
            if (this.gameMode === "shootdown") {
                this.restartGame();
            }
            else if (this.gameMode === "survival") {
                this.timer -= 2;
                this.UI.playPenaltyAnimation();
                if (this.timer <= 0) {
                    this.timer = 0;
                    this.quitGame();
                    return;
                }
                this.UI.updateTimerText(this.timer, this.gameMode);
            }
        };
        ;
        Play.prototype.changeLevel = function () {
            if (!this.timersUpdater) {
                this.createCountDownTimer();
            }
            if (this.level >= TheColourGame.HIGHEST_LEVEL_INDEX) {
                this.level++;
                this.updateGameObjects(true);
            }
            else {
                if (TheColourGame.LEVELS_TO_REPEAT.indexOf(this.level) === -1) {
                    this.level++;
                    this.board.resize(this.board.tilesPerSide + 1);
                    this.updateGameObjects(true);
                }
                else {
                    if (this.levelIsRepeating) {
                        this.levelIsRepeating = false;
                        this.level++;
                        this.board.resize(this.board.tilesPerSide + 1);
                        this.updateGameObjects(true);
                    }
                    else {
                        this.levelIsRepeating = true;
                        this.updateGameObjects(false);
                    }
                }
            }
        };
        ;
        Play.prototype.adjustInputSprite = function () {
            if (this.inputSprite.x !== this.board.boardImage.x || this.inputSprite.y !== this.board.boardImage.y) {
                this.inputSprite.x = this.board.boardImage.x;
                this.inputSprite.y = this.board.boardImage.y;
            }
            this.correctInputSprite.x = this.board.chosenTilePosition.x;
            this.correctInputSprite.y = this.board.chosenTilePosition.y;
            if (this.correctInputSprite.height > this.board.tileSideLength) {
                this.correctInputSprite.height = this.board.tileSideLength;
                this.correctInputSprite.width = this.board.tileSideLength;
            }
        };
        ;
        Play.prototype.updateTimers = function () {
            this.POFG.forceUnlock(false);
            if (this.gameMode !== "survival") {
                this.timer--;
                this.timePassed++;
            }
            else {
                this.timer -= 0.1;
                this.timePassed += 0.1;
            }
            if (this.timer <= 0) {
                this.timer = 0;
                this.UI.updateTimerText(this.timer, this.gameMode);
                this.inputSprite.inputEnabled = false;
                this.correctInputSprite.inputEnabled = false;
                this.quitGame();
                return;
            }
            else {
                this.UI.updateTimerText(this.timer, this.gameMode);
            }
        };
        ;
        Play.prototype.updateGameObjects = function (shouldUpdateChosenTileAlpha) {
            this.board.drawTiles(shouldUpdateChosenTileAlpha, false);
            this.adjustInputSprite();
            this.score++;
            if (this.gameMode === "countdown") {
                this.UI.updatePointsTracker(this.score);
            }
            else if (this.gameMode === "shootdown") {
                this.progressOnTarget--;
                this.UI.updatePointsTracker(this.progressOnTarget);
                if (this.progressOnTarget === 0) {
                    if (!localStorage.getItem(TheColourGame.SHOOTDOWN_STORAGE_TARGET_KEY)) {
                        localStorage.setItem(TheColourGame.SHOOTDOWN_STORAGE_TARGET_KEY, (TheColourGame.SHOOTDOWN_DEFAULT_TARGET + 1).toString());
                    }
                    else {
                        localStorage.setItem(TheColourGame.SHOOTDOWN_STORAGE_TARGET_KEY, (parseInt(localStorage.getItem(TheColourGame.SHOOTDOWN_STORAGE_TARGET_KEY)) + 1).toString());
                    }
                    this.quitGame();
                    return;
                }
                var progress = (this.target - this.progressOnTarget) / this.target;
                if (progress <= 0.2) {
                    this.timer = 5;
                }
                else if (progress > 0.2 && progress <= 0.4) {
                    this.timer = 4;
                }
                else {
                    this.timer = 3;
                }
                this.UI.updateTimerText(this.timer, this.gameMode);
                this.refreshTimer();
            }
            else if (this.gameMode === "target") {
                this.progressOnTarget--;
                this.UI.updatePointsTracker(this.progressOnTarget);
                if (this.progressOnTarget === 0) {
                    if (!localStorage.getItem(TheColourGame.TARGET_STORAGE_TARGET_KEY)) {
                        localStorage.setItem(TheColourGame.TARGET_STORAGE_TARGET_KEY, (TheColourGame.TARGET_DEFAULT_TARGET + 1).toString());
                    }
                    else {
                        localStorage.setItem(TheColourGame.TARGET_STORAGE_TARGET_KEY, (parseInt(localStorage.getItem(TheColourGame.TARGET_STORAGE_TARGET_KEY)) + 1).toString());
                    }
                    this.quitGame();
                    return;
                }
            }
            else if (this.gameMode === "survival") {
                if (this.score <= 10) {
                    this.timer += 1.5;
                    this.UI.playAddBonusAnimation(1.5);
                }
                else if (this.score > 10 && this.score <= 20) {
                    this.timer += 1;
                    this.UI.playAddBonusAnimation(1);
                }
                else if (this.score > 20 && this.score <= 30) {
                    this.timer += 0.7;
                    this.UI.playAddBonusAnimation(0.7);
                }
                else {
                    this.timer += 0.5;
                    this.UI.playAddBonusAnimation(0.5);
                }
                this.refreshTimer();
                this.UI.updateTimerText(this.timer, this.gameMode);
                this.UI.updatePointsTracker(this.score);
            }
        };
        Play.prototype.changeOrientation = function () {
            if (this.orientation === TheColourGame.ORIENTATIONS.LANDSCAPE) {
                this.orientation = TheColourGame.ORIENTATIONS.PORTRAIT;
            }
            else {
                this.orientation = TheColourGame.ORIENTATIONS.LANDSCAPE;
            }
            this.POFG.forceUnlock(true);
            var newWidth = this.game.height;
            var newHeight = this.game.width;
            this.game.scale.setGameSize(newWidth, newHeight);
            this.board.redrawWithNewOrientation();
            this.UI.redrawWithNewOrientation(this.timer, this.score, this.board.boardImage.width, this.gameMode, this);
            this.adjustInputSprite();
        };
        ;
        Play.prototype.loadGameObjects = function () {
            this.score = 0;
            this.level = 0;
            this.wrongClickCounter = 0;
            if (this.gameMode === "countdown") {
                this.timer = TheColourGame.COUNTDOWN_STARTING_TIMER;
                this.pointsTrackerStartingValue = this.score;
            }
            else if (this.gameMode === "shootdown") {
                this.timer = TheColourGame.SHOOTDOWN_STARTING_TIMER;
                this.target = parseInt(localStorage.getItem(TheColourGame.SHOOTDOWN_STORAGE_TARGET_KEY)) || TheColourGame.SHOOTDOWN_DEFAULT_TARGET;
                this.progressOnTarget = this.target;
                this.pointsTrackerStartingValue = this.target;
            }
            else if (this.gameMode === "target") {
                this.target = parseInt(localStorage.getItem(TheColourGame.TARGET_STORAGE_TARGET_KEY)) || TheColourGame.TARGET_DEFAULT_TARGET;
                this.timer = Math.ceil(this.target / TheColourGame.TARGET_TIME_RATIO_TO_TARGET);
                this.progressOnTarget = this.target;
                this.pointsTrackerStartingValue = this.target;
            }
            else if (this.gameMode === "survival") {
                this.timer = TheColourGame.SURVIVAL_DEFAULT_TIMER;
                this.pointsTrackerStartingValue = this.score;
            }
        };
        ;
        Play.prototype.createCountDownTimer = function () {
            var _this = this;
            var delay = 0;
            if (this.gameMode !== "survival") {
                delay = Phaser.Timer.SECOND;
            }
            else {
                delay = Phaser.Timer.SECOND * 0.1;
            }
            this.timersUpdater = this.game.time.create();
            this.timersUpdater.loop(delay, function () {
                _this.updateTimers();
            }, null, null);
            this.timersUpdater.start();
        };
        ;
        Play.prototype.refreshTimer = function () {
            var _this = this;
            var delay = 0;
            if (this.gameMode !== "survival") {
                delay = Phaser.Timer.SECOND;
            }
            else {
                delay = 100;
            }
            this.timersUpdater.destroy();
            this.timersUpdater = this.game.time.create();
            this.timersUpdater.loop(delay, function () {
                _this.updateTimers();
            }, null, null);
            this.timersUpdater.start();
        };
        ;
        return Play;
    }(Phaser.State));
    TheColourGame.Play = Play;
})(TheColourGame || (TheColourGame = {}));
