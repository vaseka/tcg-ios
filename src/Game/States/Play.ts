///<reference path="../Objects/Board.ts"/>
///<reference path="../Objects/UI.ts"/>
///<reference path="../../Lib/Typings/phaser/phaser.d.ts"/>
///<reference path="../../ColourGame.ts"/>
///<reference path="../../Util/constants.ts"/>
///<reference path="../../Util/helpers.ts"/>
///<reference path="../../Lib/Custom/mainReady.ts"/>
///<reference path="../../Util/POFG.ts"/>

module TheColourGame {

    export class Play extends Phaser.State {

        private board:Board;
        private UI:UI;
        private score:number;
        private level:number;
        private timer:number;
        private timersUpdater:Phaser.Timer;
        private correctInputSprite:Phaser.Sprite;
        private inputSprite:Phaser.Sprite;
        private levelIsRepeating:boolean;
        private target:number;
        private progressOnTarget:number;
        private pointsTrackerStartingValue:number;
        private wrongClickCounter:number;
        private POFG:POFG;
        private orientationChangeLocked:boolean;
        private orientation:ORIENTATIONS;
        private timePassed:number;

        gameMode:string;

        init(gameMode:string) {
            this.gameMode = gameMode;
        }

        preload() {
            this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.game.stage.disableVisibilityChange = true;
            this.game.time.advancedTiming = false;
            this.game.forceSingleUpdate = true;

            this.game.load.image("gameLogoSprite", "Assets/ingameSprites/gameLogoSprite.png");
            this.game.load.image("refreshSprite", "Assets/ingameSprites/refreshSprite.png");
            this.game.load.image("shadowSprite", "Assets/ingameSprites/shadowSprite.png");
            this.game.load.image("tickSprite", "Assets/ingameSprites/tickSprite.png");
            this.game.load.image("timeSprite", "Assets/ingameSprites/timeSprite.png");
            this.game.load.image("xSprite", "Assets/ingameSprites/xSprite.png");
        };

        create():void {
            this.POFG = new POFG(this.game);

            this.orientationChangeLocked = true;
            this.game.time.events.add(ORIENTATION_CHANGE_MINIMAL_INTERVAL, () => {
                this.orientationChangeLocked = false;
            });

            if (window.innerHeight > window.innerWidth) {
                this.orientation = ORIENTATIONS.PORTRAIT;
            }
            else {
                this.orientation = ORIENTATIONS.LANDSCAPE;
            }

            this.timePassed = 0;

            this.board = new Board(this.game);
            this.loadGameObjects();

            this.UI = new UI(this.game, this.timer, this.pointsTrackerStartingValue, this.board.boardImage.width, this.gameMode, this);
            this.levelIsRepeating = false;
            this.addInputSprites();

            window.onresize = () => {
                this.game.paused = false;
                if (!this.orientationChangeLocked) {
                    this.orientationChangeLocked = true;

                    this.game.time.events.add(ORIENTATION_CHANGE_MINIMAL_INTERVAL, () => {
                        this.orientationChangeLocked = false;
                    });

                    this.game.time.events.add(50, () => {
                        if ((window.innerHeight > window.innerWidth && this.orientation === ORIENTATIONS.LANDSCAPE) || (
                            window.innerHeight < window.innerWidth && this.orientation === ORIENTATIONS.PORTRAIT)) {
                            this.changeOrientation();
                        }
                    });

                }
            };

            (<ColourGame>this.game).showContainer();
        };

        update():void {
            this.POFG.update();
        }

        destroyState():void {
            this.board.destroy();
            this.UI.destroy();
            this.score = null;
            this.level = null;
            this.timer = null;

            if (this.timersUpdater) {
                this.timersUpdater.destroy();
                this.timersUpdater = null;
            }

            this.correctInputSprite.destroy();
            this.correctInputSprite = null;
            this.inputSprite.destroy();
            this.inputSprite = null;
            this.levelIsRepeating = null;
            this.target = null;
            this.pointsTrackerStartingValue = null;
            this.progressOnTarget = null;
            this.pointsTrackerStartingValue = null;
            this.wrongClickCounter = null;
            this.gameMode = null;
            window.onresize = () => {
            };
            window.onresize = null;

            this.timePassed = null;
            this.POFG.destroy();
            this.POFG = null;
        };

        restartGame():void {
            let stateToStart = this.gameMode;
            this.destroyState();
            this.game.paused = false;
            this.game.state.start(this.game.state.current, true, false, stateToStart);
        };

        sendHighScores() {
            if ((<any>navigator).connection.type !== 'none') {
                let modes = ['highScoreSurvival', 'highScoreShootdown', 'highScoreTarget', 'highScoreCountdown'];
                for (let i = 0; i < modes.length; i++) {
                    let modeScore = localStorage.getItem(modes[i]);
                    if (modeScore) {
                        let modeIdentifier = modes[i].replace('highScore', '');
                        modeIdentifier = modeIdentifier.charAt(0).toLowerCase() + modeIdentifier.slice(1);

                        accountHandler.serviceHandler.sendHighScore(modeScore.toString(), modeIdentifier, (r)=> {

                        }, (r)=> {
                            console.error(r.responseJSON.detail);
                        })
                    }
                }
            }

        }

        quitGame():void {
            let self = this;
            this.game.paused = true;
            this.game.time.removeAll();
            let currentMode = this.gameMode.charAt(0).toUpperCase() + this.gameMode.slice(1);
            let currentHighScore = parseInt(localStorage.getItem("highScore" + currentMode));
            let score = 0;
            let _isHighScoreMade = false;

            if (TheColourGame.adCounter === TheColourGame.randomAdShow) {
                TheColourGame.adCounter = 0;
                TheColourGame.randomAdShow = Math.floor(Math.random() * 2) + 1;
                if (!TheColourGame.storeCG.products[0].checkBought()) {
                    TheColourGame.heyzap.showInterstitialAd();
                }

            }
            else {
                TheColourGame.adCounter++;
            }
            if (!currentHighScore || this.score > currentHighScore) {
                localStorage.setItem("highScore" + currentMode, this.score.toString());
                _isHighScoreMade = true;
            }

            if (accountHandler) {
                if (!accountHandler.user.anonymous) {
                    let previousScore = parseInt(localStorage.getItem('previousScore'));
                    if (isNaN(previousScore)) {
                        previousScore = 0;
                    }
                    score = previousScore + self.score;
                    accountHandler.user.updateIQ(score);
                    if ((<any>navigator).connection.type !== 'none') {
                        if (_isHighScoreMade) {
                            this.sendHighScores();
                        }
                        accountHandler.serviceHandler.sendScore(score.toString(), (r)=> {
                        }, (r)=> {
                        })
                    }
                    else {
                        localStorage.setItem('previousScore', score.toString());
                    }
                }
            }

            // TheColourGame.showOverlays();

            $('.gameOver').addClass("blocked");
            $('.gameOverLayout').addClass("blocked");

            $('.pointsNumber').html(this.score.toString());

            $('.timerNumber').html(Math.round(this.timePassed).toString());

            $('.wrongNumber').html(this.wrongClickCounter.toString());
            $('.iqPoints').html(this.score.toString());

            $('.back').off().on("click", () => {
                $('.gameOver').removeClass("blocked");
                (<ColourGame>self.game).hideContainer(()=> {
                    $('.gameOverLayout').removeClass("blocked");
                    self.destroyState();
                });
            });

            $('.leaderboard').off().on("click", ()=> {
                TheColourGame.accountHandler.user.leaderboardModalRequest();
            });

            $(".again").off().on("click", () => {
                $('.gameOver').removeClass("blocked");
                $('.gameOverLayout').removeClass("blocked");
                self.restartGame();
                // TheColourGame.hideOverlays();
            });
            $(".share").off().on('click', ()=> {
                (<any>window).plugins.socialsharing.share("I scored " + self.score + " points playing The Colour Game. Try and beat me!", "Fractal Games The Colour Game", null, 'www.fractalgames.com')
            });
        };

        private addInputSprites() {
            this.inputSprite = this.game.add.sprite(this.board.boardImage.x, this.board.boardImage.y);
            this.inputSprite.width = this.board.boardImage.width;
            this.inputSprite.height = this.board.boardImage.height;
            this.inputSprite.anchor.set(0.5);
            this.inputSprite.alpha = 0;
            this.inputSprite.inputEnabled = true;
            this.inputSprite.input.priorityID = 0;
            this.inputSprite.events.onInputDown.add(() => {
                this.handleWrongInput();
            });

            this.correctInputSprite = this.game.add.sprite(this.board.chosenTilePosition.x, this.board.chosenTilePosition.y);
            this.correctInputSprite.width = this.board.tileSideLength;
            this.correctInputSprite.height = this.board.tileSideLength;
            this.correctInputSprite.anchor.set(0.5);
            this.correctInputSprite.alpha = 0;
            this.correctInputSprite.inputEnabled = true;
            this.correctInputSprite.input.priorityID = 1;
            this.correctInputSprite.events.onInputDown.add(() => {
                this.changeLevel();
            });
        };

        private handleWrongInput():void {
            if (!this.timersUpdater) {
                this.createCountDownTimer();
            }

            this.wrongClickCounter++;

            if (this.gameMode === "shootdown") {
                this.restartGame();
            }
            else if (this.gameMode === "survival") {
                this.timer -= 2;
                this.UI.playPenaltyAnimation();

                if (this.timer <= 0) {
                    this.timer = 0;
                    this.quitGame();
                    return;
                }

                this.UI.updateTimerText(this.timer, this.gameMode);
            }
        };

        private changeLevel():void {
            if (!this.timersUpdater) {
                this.createCountDownTimer();
            }

            if (this.level >= HIGHEST_LEVEL_INDEX) {
                this.level++;
                this.updateGameObjects(true);
            }
            else {
                if (LEVELS_TO_REPEAT.indexOf(this.level) === -1) {
                    this.level++;
                    this.board.resize(this.board.tilesPerSide + 1);
                    this.updateGameObjects(true);
                }
                else {
                    if (this.levelIsRepeating) {
                        this.levelIsRepeating = false;
                        this.level++;
                        this.board.resize(this.board.tilesPerSide + 1);
                        this.updateGameObjects(true);
                    }
                    else {
                        this.levelIsRepeating = true;
                        this.updateGameObjects(false);
                    }
                }
            }
        };

        private adjustInputSprite() {
            if (this.inputSprite.x !== this.board.boardImage.x || this.inputSprite.y !== this.board.boardImage.y) {
                this.inputSprite.x = this.board.boardImage.x;
                this.inputSprite.y = this.board.boardImage.y;
            }

            this.correctInputSprite.x = this.board.chosenTilePosition.x;
            this.correctInputSprite.y = this.board.chosenTilePosition.y;

            if (this.correctInputSprite.height > this.board.tileSideLength) {
                this.correctInputSprite.height = this.board.tileSideLength;
                this.correctInputSprite.width = this.board.tileSideLength;
            }
        };

        private updateTimers():void {
            this.POFG.forceUnlock(false);

            if (this.gameMode !== "survival") {
                this.timer--;
                this.timePassed++;
            }
            else {
                this.timer -= 0.1;
                this.timePassed += 0.1;
            }

            if (this.timer <= 0) {
                this.timer = 0;
                this.UI.updateTimerText(this.timer, this.gameMode);
                this.inputSprite.inputEnabled = false;
                this.correctInputSprite.inputEnabled = false;
                this.quitGame();
                return
            }
            else {
                this.UI.updateTimerText(this.timer, this.gameMode);
            }
        };

        private updateGameObjects(shouldUpdateChosenTileAlpha:boolean):void {
            this.board.drawTiles(shouldUpdateChosenTileAlpha, false);
            this.adjustInputSprite();
            this.score++;

            if (this.gameMode === "countdown") {
                this.UI.updatePointsTracker(this.score);
            }
            else if (this.gameMode === "shootdown") {
                this.progressOnTarget--;
                this.UI.updatePointsTracker(this.progressOnTarget);

                if (this.progressOnTarget === 0) {
                    if (!localStorage.getItem(SHOOTDOWN_STORAGE_TARGET_KEY)) {
                        localStorage.setItem(SHOOTDOWN_STORAGE_TARGET_KEY, (SHOOTDOWN_DEFAULT_TARGET + 1).toString());
                    }
                    else {
                        localStorage.setItem(SHOOTDOWN_STORAGE_TARGET_KEY, (parseInt(localStorage.getItem(SHOOTDOWN_STORAGE_TARGET_KEY)) + 1).toString());
                    }
                    this.quitGame();
                    return;
                }

                var progress = (this.target - this.progressOnTarget) / this.target;

                if (progress <= 0.2) {
                    this.timer = 5;
                }
                else if (progress > 0.2 && progress <= 0.4) {
                    this.timer = 4;
                }
                else {
                    this.timer = 3;
                }

                this.UI.updateTimerText(this.timer, this.gameMode);
                this.refreshTimer();
            }
            else if (this.gameMode === "target") {
                this.progressOnTarget--;
                this.UI.updatePointsTracker(this.progressOnTarget);

                if (this.progressOnTarget === 0) {
                    if (!localStorage.getItem(TARGET_STORAGE_TARGET_KEY)) {
                        localStorage.setItem(TARGET_STORAGE_TARGET_KEY, (TARGET_DEFAULT_TARGET + 1).toString());
                    }
                    else {
                        localStorage.setItem(TARGET_STORAGE_TARGET_KEY, (parseInt(localStorage.getItem(TARGET_STORAGE_TARGET_KEY)) + 1).toString());
                    }
                    this.quitGame();
                    return;
                }
            }
            else if (this.gameMode === "survival") {
                if (this.score <= 10) {
                    this.timer += 1.5;
                    this.UI.playAddBonusAnimation(1.5);
                }
                else if (this.score > 10 && this.score <= 20) {
                    this.timer += 1;
                    this.UI.playAddBonusAnimation(1);
                }
                else if (this.score > 20 && this.score <= 30) {
                    this.timer += 0.7;
                    this.UI.playAddBonusAnimation(0.7);
                }
                else {
                    this.timer += 0.5;
                    this.UI.playAddBonusAnimation(0.5);
                }

                this.refreshTimer();
                this.UI.updateTimerText(this.timer, this.gameMode);
                this.UI.updatePointsTracker(this.score);
            }
        }

        private changeOrientation():void {
            if (this.orientation === ORIENTATIONS.LANDSCAPE) {
                this.orientation = ORIENTATIONS.PORTRAIT;
            }
            else {
                this.orientation = ORIENTATIONS.LANDSCAPE;
            }

            this.POFG.forceUnlock(true);
            var newWidth = this.game.height;
            var newHeight = this.game.width;
            this.game.scale.setGameSize(newWidth, newHeight);
            this.board.redrawWithNewOrientation();
            this.UI.redrawWithNewOrientation(this.timer, this.score, this.board.boardImage.width, this.gameMode, this);
            this.adjustInputSprite();
        };

        private loadGameObjects():void {
            this.score = 0;
            this.level = 0;
            this.wrongClickCounter = 0;

            if (this.gameMode === "countdown") {
                this.timer = COUNTDOWN_STARTING_TIMER;
                this.pointsTrackerStartingValue = this.score;
            }
            else if (this.gameMode === "shootdown") {
                this.timer = SHOOTDOWN_STARTING_TIMER;
                this.target = parseInt(localStorage.getItem(SHOOTDOWN_STORAGE_TARGET_KEY)) || SHOOTDOWN_DEFAULT_TARGET;
                this.progressOnTarget = this.target;
                this.pointsTrackerStartingValue = this.target;
            }
            else if (this.gameMode === "target") {
                this.target = parseInt(localStorage.getItem(TARGET_STORAGE_TARGET_KEY)) || TARGET_DEFAULT_TARGET;
                this.timer = Math.ceil(this.target / TARGET_TIME_RATIO_TO_TARGET);
                this.progressOnTarget = this.target;
                this.pointsTrackerStartingValue = this.target;
            }
            else if (this.gameMode === "survival") {
                this.timer = SURVIVAL_DEFAULT_TIMER;
                this.pointsTrackerStartingValue = this.score;
            }
        };

        private createCountDownTimer() {
            var delay:number = 0;
            if (this.gameMode !== "survival") {
                delay = Phaser.Timer.SECOND;
            }
            else {
                delay = Phaser.Timer.SECOND * 0.1;
            }

            this.timersUpdater = this.game.time.create();
            this.timersUpdater.loop(delay, () => {
                this.updateTimers();
            }, null, null);
            this.timersUpdater.start();
        };

        private refreshTimer():void {
            var delay:number = 0;
            if (this.gameMode !== "survival") {
                delay = Phaser.Timer.SECOND;
            }
            else {
                delay = 100;
            }

            this.timersUpdater.destroy();
            this.timersUpdater = this.game.time.create();
            this.timersUpdater.loop(delay, () => {
                this.updateTimers();
            }, null, null);
            this.timersUpdater.start();
        };
    }
}