///<reference path="../States/Play.ts"/>
var TheColourGame;
(function (TheColourGame) {
    var UI = (function () {
        function UI(game, time, score, boardImageWidth, gameMode, playState) {
            this.game = game;
            if (window.innerHeight > window.innerWidth) {
                this.drawPortraitUI(time, score, gameMode, playState);
            }
            else {
                this.drawLandscapeUI(time, score, boardImageWidth, gameMode, playState);
            }
        }
        UI.prototype.destroy = function () {
            this.refreshButton.destroy();
            this.refreshButton = null;
            this.modeText.destroy();
            this.modeText = null;
            this.exitButton.destroy();
            this.exitButton = null;
            if (this.shadowSprite) {
                this.shadowSprite.destroy();
                this.shadowSprite = null;
            }
            this.pointsTrackerSprite.destroy();
            this.pointsTrackerSprite = null;
            this.pointsText.destroy();
            this.pointsText = null;
            this.timeSprite.destroy();
            this.timeSprite = null;
            this.timeText.destroy();
            this.timeText = null;
            if (this.gameLogo) {
                this.gameLogo.destroy();
                this.gameLogo = null;
            }
            this.numbersStyle = null;
            if (this.timeTextDecimal) {
                this.timeTextDecimal.destroy();
                this.timeTextDecimal = null;
            }
            if (this.survivalModeBonusTime) {
                this.survivalModeBonusTime.destroy();
                this.survivalModeBonusTime = null;
                this.survivalModePenaltyPenaltyTime.destroy();
                this.survivalModePenaltyPenaltyTime = null;
                this.bonusTween = null;
                this.penaltyTween = null;
            }
        };
        ;
        UI.prototype.updateTimerText = function (time, mode) {
            if (mode !== "survival") {
                this.timeText.setText(time.toString());
            }
            else {
                this.timeText.setText(Math.floor(time).toFixed(0).toString());
                this.timeTextDecimal.setText((time - Math.floor(time)).toFixed(1).toString().substr(1, 2)); //.replace("0.", "."));
                if (this.timeTextDecimal.x - this.timeText.x !== this.timeText.width * 0.5) {
                    this.timeTextDecimal.x = this.timeText.x + this.timeText.width * 0.5;
                }
            }
        };
        ;
        UI.prototype.updatePointsTracker = function (scoreValue) {
            this.pointsText.setText(scoreValue.toString());
        };
        ;
        UI.prototype.redrawWithNewOrientation = function (time, score, boardImageWidth, gameMode, playState) {
            this.destroy();
            if (window.innerHeight > window.innerWidth) {
                this.drawPortraitUI(time, score, gameMode, playState);
            }
            else {
                this.drawLandscapeUI(time, score, boardImageWidth, gameMode, playState);
            }
        };
        ;
        UI.prototype.playAddBonusAnimation = function (timeToAdd) {
            if (this.bonusTween && this.bonusTween.isRunning) {
                this.bonusTween.stop();
                this.survivalModeBonusTime.alpha = 0;
                this.survivalModeBonusTime.x = this.timeSprite.x;
                this.survivalModeBonusTime.y = this.timeSprite.y;
            }
            var targetPositionX = 0;
            var targetPositionY = 0;
            if (window.innerHeight > window.innerWidth) {
                targetPositionX = this.survivalModeBonusTime.x + this.game.height * TheColourGame.SURVIVAL_PORTRAIT_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
                targetPositionY = this.survivalModeBonusTime.y - this.game.height * TheColourGame.SURVIVAL_PORTRAIT_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
            }
            else {
                targetPositionX = this.survivalModeBonusTime.x + this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
                targetPositionY = this.survivalModeBonusTime.y - this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
                if (this.checkIfDisplayedOnTablet) {
                    targetPositionX -= this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_TABLET_FLOATING_NUMBERS_TWEEN_DISTANCE_REDUCTION_MULTIPLIER;
                    targetPositionY -= this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_TABLET_FLOATING_NUMBERS_TWEEN_DISTANCE_REDUCTION_MULTIPLIER;
                }
            }
            this.survivalModeBonusTime.setText("+" + timeToAdd.toString());
            this.bonusTween = this.game.add.tween(this.survivalModeBonusTime).to({
                alpha: 1,
                x: targetPositionX,
                y: targetPositionY
            }, TheColourGame.SURVIVAL_BONUS_PENALTY_TWEENS_DURATION, Phaser.Easing.Default, true, 0, 0, true);
        };
        ;
        UI.prototype.playPenaltyAnimation = function () {
            if (this.penaltyTween && this.penaltyTween.isRunning) {
                this.penaltyTween.stop();
                this.survivalModePenaltyPenaltyTime.alpha = 0;
                this.survivalModePenaltyPenaltyTime.x = this.timeSprite.x;
                this.survivalModePenaltyPenaltyTime.y = this.timeSprite.y;
            }
            var targetPositionX = 0;
            var targetPositionY = 0;
            if (window.innerHeight > window.innerWidth) {
                targetPositionX = this.survivalModePenaltyPenaltyTime.x + this.game.height * TheColourGame.SURVIVAL_PORTRAIT_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
                targetPositionY = this.survivalModePenaltyPenaltyTime.y + this.game.height * TheColourGame.SURVIVAL_PORTRAIT_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
            }
            else {
                targetPositionX = this.survivalModePenaltyPenaltyTime.x + this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
                targetPositionY = this.survivalModePenaltyPenaltyTime.y + this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_BONUS_PENALTY_TRAVELING_DISTANCE_MULTIPLIER;
                if (this.checkIfDisplayedOnTablet) {
                    targetPositionX -= this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_TABLET_FLOATING_NUMBERS_TWEEN_DISTANCE_REDUCTION_MULTIPLIER * 0.5;
                    targetPositionY -= this.game.width * TheColourGame.SURVIVAL_LANDSCAPE_TABLET_FLOATING_NUMBERS_TWEEN_DISTANCE_REDUCTION_MULTIPLIER * 0.5;
                }
            }
            this.penaltyTween = this.game.add.tween(this.survivalModePenaltyPenaltyTime).to({
                alpha: 1,
                x: targetPositionX,
                y: targetPositionY
            }, TheColourGame.SURVIVAL_BONUS_PENALTY_TWEENS_DURATION, Phaser.Easing.Default, true, 0, 0, true);
        };
        ;
        UI.prototype.drawPortraitUI = function (time, score, gameMode, playState) {
            this.numbersStyle = {
                font: 'Oswald-Regular',
                fontSize: this.game.height * TheColourGame.UI_PORTRAIT_NUMBERS_FONT_SIZE_MULTIPLIER,
                fill: "#ffffff"
            };
            var shadowY = this.game.height * (200 / 1920);
            var shadowHeight = this.game.height * (60 / 1920);
            this.shadowSprite = this.game.add.sprite(0, shadowY, "shadowSprite");
            this.shadowSprite.width = this.game.width;
            this.shadowSprite.height = shadowHeight;
            var upperElementsY = this.shadowSprite.y * 0.5;
            var upperElementsOffsetFromEnd = this.game.width * (90 / 1080);
            var upperElementsSideLength = this.game.height * (120 / 1920);
            this.refreshButton = this.game.add.button(upperElementsOffsetFromEnd, upperElementsY, "refreshSprite", function () {
                playState.restartGame();
            });
            this.refreshButton.anchor.set(0.5);
            this.refreshButton.width = upperElementsSideLength;
            this.refreshButton.scale.y = this.refreshButton.scale.x;
            // there is a shadow that is 25% the height of the entire sprite, thus we lower the refresh and exit buttons by an additional 12.5% of their height
            this.refreshButton.y += this.refreshButton.height * TheColourGame.UI_QUIT_REFRESH_Y_OFFSET_MULTIPLIER;
            this.modeText = this.game.add.text(this.game.width * 0.5, upperElementsY, gameMode.toUpperCase(), {
                font: "Oswald-Light",
                fontSize: this.game.height * TheColourGame.UI_PORTRAIT_NUMBERS_FONT_SIZE_MULTIPLIER,
                fill: "#ffffff"
            });
            this.modeText.anchor.set(0.5, 0.5);
            this.exitButton = this.game.add.button(this.game.width - upperElementsOffsetFromEnd, upperElementsY, "xSprite", function () {
                playState.quitGame();
            });
            this.exitButton.anchor.set(0.5);
            this.exitButton.width = upperElementsSideLength;
            this.exitButton.scale.y = this.refreshButton.scale.x;
            this.exitButton.y += this.exitButton.height * TheColourGame.UI_QUIT_REFRESH_Y_OFFSET_MULTIPLIER;
            var lowerElementsY = this.game.height * (290 / 1920);
            var lowerElementsOffsetFromMiddle = this.game.width * (220 / 1080);
            var lowerElementsSideLength = this.game.height * (75 / 1920);
            this.timeSprite = this.game.add.sprite(this.game.width * 0.5 + lowerElementsOffsetFromMiddle, lowerElementsY, "timeSprite");
            this.timeSprite.width = lowerElementsSideLength;
            this.timeSprite.height = lowerElementsSideLength;
            this.timeSprite.anchor.set(0.5);
            var numbersYPosition = this.timeSprite.y + this.timeSprite.height * TheColourGame.UI_ELEMENTS_VERTICAL_OFFSET_MULTIPLIER;
            if (!this.checkIfDisplayedOnTablet()) {
                numbersYPosition *= TheColourGame.UI_PHONE_NUMBERS_ADDITIONAL_OFFSET_MULTIPLIER;
            }
            this.timeText = this.game.add.text(this.timeSprite.x, numbersYPosition, time.toFixed(0).toString(), this.numbersStyle);
            this.timeText.anchor.set(0.5);
            if (gameMode === "survival") {
                this.addSurvivalSpecificObjects(time);
            }
            this.pointsTrackerSprite = this.game.add.sprite(this.game.width * 0.5 - lowerElementsOffsetFromMiddle, lowerElementsY, "tickSprite");
            this.pointsTrackerSprite.width = lowerElementsSideLength;
            this.pointsTrackerSprite.height = lowerElementsSideLength;
            this.pointsTrackerSprite.anchor.set(0.5);
            this.pointsText = this.game.add.text(this.pointsTrackerSprite.x, numbersYPosition, score.toString(), this.numbersStyle);
            this.pointsText.anchor.set(0.5);
        };
        ;
        UI.prototype.drawLandscapeUI = function (time, score, boardImageWidth, gameMode, playState) {
            this.numbersStyle = {
                font: 'Oswald-Regular',
                fontSize: this.game.width * TheColourGame.UI_LANDSCAPE_NUMBERS_FONT_SIZE_MULTIPLIER,
                fill: "#ffffff"
            };
            var logoX = (this.game.width - boardImageWidth) * 0.25;
            var logoY = this.game.height * (75 / 375);
            this.gameLogo = this.game.add.sprite(logoX, logoY, "gameLogoSprite");
            this.gameLogo.anchor.set(0.5);
            this.gameLogo.width = this.game.width * (110 / 670);
            this.gameLogo.scale.y = this.gameLogo.scale.x;
            this.modeText = this.game.add.text(this.gameLogo.x, this.gameLogo.y + this.gameLogo.height * TheColourGame.UI_ELEMENTS_VERTICAL_OFFSET_MULTIPLIER, gameMode.toUpperCase(), {
                font: "Oswald-Light",
                fontSize: this.game.height * TheColourGame.UI_PORTRAIT_NUMBERS_FONT_SIZE_MULTIPLIER,
                fill: "#ffffff"
            });
            this.modeText.anchor.set(0.5);
            // for ze iPad
            if (this.checkIfDisplayedOnTablet()) {
                this.gameLogo.width *= TheColourGame.UI_LANDSCAPE_TABLET_LOGO_REDUCTION_MULTIPLIER;
                this.gameLogo.scale.y = this.gameLogo.scale.x;
                this.numbersStyle.fontSize *= 0.8;
                this.modeText.fontSize *= TheColourGame.UI_LANDSCAPE_TABLET_MODE_TEXT_REDUCTION_MULTIPLIER;
            }
            var leftSideElementsWidth = this.game.width * (50 / 735);
            var refreshButtonY = this.game.height * (275 / 415);
            this.refreshButton = this.game.add.button(this.gameLogo.x, refreshButtonY, "refreshSprite", function () {
                playState.restartGame();
            });
            this.refreshButton.anchor.set(0.5);
            this.refreshButton.width = leftSideElementsWidth;
            this.refreshButton.scale.y = this.refreshButton.scale.x;
            this.exitButton = this.game.add.button(this.gameLogo.x, this.refreshButton.y + this.refreshButton.height * 1.25, "xSprite", function () {
                playState.quitGame();
            });
            this.exitButton.anchor.set(0.5);
            this.exitButton.width = leftSideElementsWidth;
            this.exitButton.scale.y = this.exitButton.scale.x;
            var rightSideElementsWidth = this.game.width * (42 / 735);
            var pointsTrackerSpriteX = this.game.width - logoX;
            //var pointsTrackerSpriteY = this.game.height * (100 / 415);
            var pointsTrackerSpriteY = this.game.height * TheColourGame.UI_LANDSCAPE_RIGHT_ELEMENTS_VOFFSET_MULTIPLIER;
            this.pointsTrackerSprite = this.game.add.sprite(pointsTrackerSpriteX, pointsTrackerSpriteY, "tickSprite");
            this.pointsTrackerSprite.anchor.set(0.5);
            this.pointsTrackerSprite.width = rightSideElementsWidth;
            this.pointsTrackerSprite.scale.y = this.pointsTrackerSprite.scale.x;
            this.pointsText = this.game.add.text(this.pointsTrackerSprite.x, this.pointsTrackerSprite.y + this.pointsTrackerSprite.height * TheColourGame.UI_ELEMENTS_VERTICAL_OFFSET_MULTIPLIER, score.toString(), this.numbersStyle);
            this.pointsText.anchor.set(0.5);
            var timeSpriteY = this.game.height * (0.5 + TheColourGame.UI_LANDSCAPE_RIGHT_ELEMENTS_VOFFSET_MULTIPLIER);
            this.timeSprite = this.game.add.sprite(this.pointsTrackerSprite.x, timeSpriteY, "timeSprite");
            this.timeSprite.anchor.set(0.5);
            this.timeSprite.width = rightSideElementsWidth;
            this.timeSprite.scale.y = this.timeSprite.scale.x;
            this.timeText = this.game.add.text(this.timeSprite.x, this.timeSprite.y + this.timeSprite.height * TheColourGame.UI_ELEMENTS_VERTICAL_OFFSET_MULTIPLIER, time.toFixed(0).toString(), this.numbersStyle);
            this.timeText.anchor.set(0.5);
            if (gameMode === "survival") {
                if (this.checkIfDisplayedOnTablet()) {
                    this.timeText.x -= this.game.width * TheColourGame.UI_SURVIVAL_TABLET_TIME_OFFSET_MULTIPLIER;
                }
                this.addSurvivalSpecificObjects(time);
            }
        };
        ;
        UI.prototype.addSurvivalSpecificObjects = function (time) {
            this.timeTextDecimal = this.game.add.text(this.timeText.x + this.timeText.width * 0.5, this.timeText.y - this.timeText.height * 0.4, (time - Math.floor(time)).toFixed(1).toString().substr(1, 2), this.numbersStyle);
            //this.timeTextDecimal.anchor.set(0, 0);
            this.timeTextDecimal.fontSize *= TheColourGame.UI_DECIMAL_TIME_FONT_SIZE_REDUCTION_MULTIPLIER;
            this.survivalModeBonusTime = this.game.add.text(this.timeSprite.x, this.timeSprite.y, "5", {
                font: "Oswald",
                fill: "#00FF00",
                fontSize: this.numbersStyle.fontSize * TheColourGame.SURVIVAL_BONUS_PENALTY_FONT_SIZE_MULTIPLIER
            });
            this.survivalModeBonusTime.alpha = 0;
            this.survivalModeBonusTime.anchor.set(0.5);
            this.survivalModePenaltyPenaltyTime = this.game.add.text(this.timeSprite.x, this.timeSprite.y, "-2", {
                font: "Arial",
                fill: "#FF3333",
                fontSize: this.numbersStyle.fontSize * TheColourGame.SURVIVAL_BONUS_PENALTY_FONT_SIZE_MULTIPLIER
            });
            this.survivalModePenaltyPenaltyTime.alpha = 0;
            this.survivalModePenaltyPenaltyTime.anchor.set(0.5);
        };
        ;
        UI.prototype.checkIfDisplayedOnTablet = function () {
            if (window.innerHeight > window.innerWidth) {
                return (this.game.width / this.game.height) > TheColourGame.TABLET_RESOLUTION_RATIO_THRESHOLD;
            }
            else {
                return (this.game.height / this.game.width) > TheColourGame.TABLET_RESOLUTION_RATIO_THRESHOLD;
            }
        };
        ;
        return UI;
    }());
    TheColourGame.UI = UI;
})(TheColourGame || (TheColourGame = {}));
