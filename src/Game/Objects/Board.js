///<reference path="../../Lib/Typings/phaser/phaser.d.ts"/>
///<reference path="../../Util/constants.ts"/>
var TheColourGame;
(function (TheColourGame) {
    var Board = (function () {
        function Board(game) {
            this.game = game;
            this.tilesPerSide = TheColourGame.BOARD_STARTING_TILES;
            this.calculateBoardElements();
            this.alphasCollection = [];
            for (var i = TheColourGame.CHOSEN_TILE_MIN_ALPHA; i <= TheColourGame.CHOSEN_TILE_MAX_ALPHA; i++) {
                this.alphasCollection.push(i / 100);
            }
            this.currentAlphaIndex = 0;
            this.drawBackGroundSprite();
            this.calculateTileSideSize();
            this.drawTiles(false, false);
        }
        Board.prototype.resize = function (newSize) {
            this.tilesPerSide = newSize;
            this.calculateTileSideSize();
        };
        ;
        Board.prototype.redrawWithNewOrientation = function () {
            this.drawBackGroundSprite();
            this.calculateBoardElements();
            this.drawTiles(false, true);
        };
        ;
        Board.prototype.drawTiles = function (shouldUpdateChosenTileAlpha, drawingOnOrientationChange) {
            if (this.boardImage) {
                this.boardImage.destroy();
                this.drawBMD.clear();
                this.drawBMD.destroy();
            }
            if (shouldUpdateChosenTileAlpha && this.currentAlphaIndex < this.alphasCollection.length - 1) {
                this.currentAlphaIndex++;
            }
            if (!drawingOnOrientationChange) {
                this.chooseRandomColor(TheColourGame.MIN_RANDOM_COLOR, TheColourGame.MAX_RANDOM_COLOR);
                this.chosenTileIndex = Math.round(Math.random() * (this.tilesPerSide * this.tilesPerSide - 1));
            }
            this.drawBMD = this.game.add.bitmapData(10, 10, "tile", true);
            this.drawBMD.ctx.rect(0, 0, this.drawBMD.width, this.drawBMD.height);
            this.drawBMD.ctx.fillStyle = this.currentColor;
            this.drawBMD.ctx.fill();
            var tileSprite = this.game.make.sprite(0, 0, this.game.cache.getBitmapData("tile"));
            tileSprite.width = this.tileSideLength;
            tileSprite.height = this.tileSideLength;
            tileSprite.anchor.set(0.5);
            var chosenTileSprite = this.game.make.sprite(0, 0, this.game.cache.getBitmapData("tile"));
            chosenTileSprite.width = this.tileSideLength;
            chosenTileSprite.height = this.tileSideLength;
            chosenTileSprite.anchor.set(0.5);
            chosenTileSprite.alpha = this.alphasCollection[this.currentAlphaIndex];
            this.drawBMD = this.game.add.bitmapData(this.sideLength, this.sideLength, "allTiles", true);
            var counter = -1;
            for (var i = 0; i < this.tilesPerSide; i++) {
                for (var j = 0; j < this.tilesPerSide; j++) {
                    counter++;
                    if (counter !== this.chosenTileIndex) {
                        this.drawBMD.draw(tileSprite, this.lineWidth + this.tileSideLength * 0.5 + j * (this.tileSideLength + this.lineWidth), this.lineWidth + this.tileSideLength * 0.5 + i * (this.tileSideLength + this.lineWidth));
                    }
                    else {
                        var xPosition = this.lineWidth + this.tileSideLength * 0.5 + j * (this.tileSideLength + this.lineWidth);
                        var yPosition = this.lineWidth + this.tileSideLength * 0.5 + i * (this.tileSideLength + this.lineWidth);
                        this.drawBMD.draw(chosenTileSprite, xPosition, yPosition);
                        this.chosenTilePosition = new Phaser.Point(xPosition, yPosition);
                    }
                }
            }
            if (window.innerHeight > window.innerWidth) {
                this.boardImage = this.game.add.sprite(this.game.width * 0.5, this.game.height * TheColourGame.BOARD_PORTRAIT_HEIGHT_MULTIPLIER, this.game.cache.getBitmapData("allTiles"));
                // for ze iPad
                if (this.game.width / this.game.height > TheColourGame.TABLET_RESOLUTION_RATIO_THRESHOLD) {
                    this.boardImage.y = this.game.height - this.boardImage.height * 0.5;
                    this.backGroundSprite.y = this.game.height - this.boardImage.height * 0.5;
                }
                // correction after the proper placement of the board image
                this.chosenTilePosition.y += this.boardImage.y - this.boardImage.height * 0.5;
            }
            else {
                this.boardImage = this.game.add.sprite(this.game.width * 0.5, this.game.height * 0.5, this.game.cache.getBitmapData("allTiles"));
                this.chosenTilePosition.x += this.game.width * 0.5 - this.boardImage.width * 0.5;
            }
            this.boardImage.anchor.set(0.5);
            tileSprite.destroy();
            tileSprite = null;
            chosenTileSprite.destroy();
            chosenTileSprite = null;
        };
        Board.prototype.destroy = function () {
            this.sideLength = null;
            this.lineWidth = null;
            this.backGroundSprite.destroy();
            this.backGroundSprite = null;
            this.drawBMD.destroy();
            this.drawBMD = null;
            this.currentColor = null;
            this.alphasCollection = null;
            this.currentAlphaIndex = null;
            this.chosenTileIndex = null;
            this.boardImage.destroy();
            this.boardImage = null;
            this.tilesPerSide = null;
            this.chosenTilePosition = null;
            this.tileSideLength = null;
        };
        ;
        Board.prototype.calculateBoardElements = function () {
            if (window.innerHeight > window.innerWidth) {
                this.sideLength = this.game.width;
                this.lineWidth = this.game.width * TheColourGame.BOARD_PORTRAIT_LINE_WIDTH_MULTIPLIER;
            }
            else {
                this.sideLength = this.game.height;
                this.lineWidth = this.game.height * TheColourGame.BOARD_PORTRAIT_LINE_WIDTH_MULTIPLIER;
            }
        };
        ;
        Board.prototype.chooseRandomColor = function (min, max) {
            var red = min + Math.round(Math.random() * (max - min));
            var green = min + Math.round(Math.random() * (max - min));
            var blue = min + Math.round(Math.random() * (max - min));
            var color = Phaser.Color.createColor(red, green, blue, 1);
            this.currentColor = Phaser.Color.RGBtoString(parseInt(color.r), parseInt(color.g), parseInt(color.b), parseInt(color.a));
        };
        ;
        Board.prototype.drawBackGroundSprite = function () {
            if (this.backGroundSprite) {
                this.backGroundSprite.destroy();
            }
            else {
                this.drawBMD = this.game.add.bitmapData(5, 5, "backgroundSprite", true);
                this.drawBMD.ctx.rect(0, 0, this.drawBMD.width, this.drawBMD.height);
                this.drawBMD.ctx.fillStyle = "#ffffff";
                this.drawBMD.ctx.fill();
            }
            if (window.innerHeight > window.innerWidth) {
                this.backGroundSprite = this.game.add.sprite(this.game.width * 0.5, this.game.height * TheColourGame.BOARD_PORTRAIT_HEIGHT_MULTIPLIER, this.game.cache.getBitmapData("backgroundSprite"));
            }
            else {
                this.backGroundSprite = this.game.add.sprite(this.game.width * 0.5, this.game.height * 0.5, this.game.cache.getBitmapData("backgroundSprite"));
            }
            this.backGroundSprite.width = this.sideLength;
            this.backGroundSprite.height = this.sideLength;
            this.backGroundSprite.anchor.set(0.5);
        };
        ;
        Board.prototype.calculateTileSideSize = function () {
            this.tileSideLength = (this.sideLength - (this.tilesPerSide + 1) * this.lineWidth) / this.tilesPerSide;
        };
        ;
        return Board;
    }());
    TheColourGame.Board = Board;
})(TheColourGame || (TheColourGame = {}));
