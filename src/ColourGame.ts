﻿///<reference path="Game/Objects/Board.ts"/>
///<reference path="Lib/Typings/phaser/phaser.d.ts"/>
///<reference path="Game/States/Play.ts"/>
///<reference path="Lib/Typings/jquery/jquery.d.ts"/>
///<reference path="Lib/Custom/moreInfo.ts"/>

module TheColourGame {
    export class ColourGame extends Phaser.Game {
        constructor(width, height, name) {
            super(width, height, Phaser.AUTO, "game", true, true);
            this.state.add("Play", Play, false);
            this.state.start("Play", true, true, name);

        }

        showContainer() {
            let game = $(".game");
            game.addClass("blocked");
            $('.clickPreventorModes').addClass("preventOn");
            $(".chooseMode").addClass("slideAway");
            game.addClass("slideIn");
            Fractal.Helpers.removeLoader();
            setTimeout(function () {
                $(".color-game").removeClass("blocked");
                $('.clickPreventorModes').removeClass("preventOn");
            }, 1000);
        }

        hideContainer(callback) {
            let game = this;
            setHighScoresPlay();
            $(".clickPreventor").addClass("preventOn");
            $(".color-game").addClass("blocked");

            setTimeout(function () {
                $(".chooseMode").removeClass("slideAway");
                $(".game").removeClass("slideIn");
                TheColourGame.showOverlays();
                setTimeout(() => {
                    $('.clickPreventor').removeClass("preventOn");
                    callback();
                    // game.cache.destroy();
                    // game.world.destroy(true);
                    game.destroy();
                    Phaser.GAMES = []
                }, 1000);
            }, 1000);

        }
    }
}