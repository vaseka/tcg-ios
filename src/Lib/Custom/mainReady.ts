///<reference path="../Typings/jquery/jquery.d.ts"/>
///<reference path="menuScreen.ts"/>
///<reference path="../../Util/accountHandler.ts"/>
///<reference path="../../Util/Store/Store.ts"/>
///<reference path="../../Util/HeyZap.ts"/>
///<reference path="splashScripts.ts"/>
///<reference path="chooseMode.ts"/>

module TheColourGame {
    // import PRODUCTS = Fractal.PRODUCTS;

    document.addEventListener("deviceready", onDeviceReady, false);
    export let heyzap:Fractal.HeyzapController;
    export let storeCG = null;
    export let storeReady = false;
    export let accountHandler = null;
    export let adCounter = 0;
    export let randomAdShow = Math.floor(Math.random() * 2) + 2;

    let _$mainMenu = $('.mainMenu');
    let _$moreInfo = $('.moreInfo');
    let _$backInfoButton = $('.backInfoButton');
    let _$chooseMode = $('.chooseMode');
    let _$backMenuButton = $('.backMenuButton');
    let _$gameOver = $('.gameOver');
    let _$loginWrapperModal = $('.login-wrapper-modal');
    let _$accountWrapperModal = $('.account-wrapper-modal');
    let _$leaderboardWrapperModal = $('.leaderboard-wrapper-modal');
    let _backButtonFloodPrevention = true;
    let _$gameOverBackButton = $('.gameOverButtons').find('.back');

    function onBackButtonPressed() {
        if (_backButtonFloodPrevention) {
            _backButtonFloodPrevention = false;
            setTimeout(()=> {
                _backButtonFloodPrevention = true;
            }, 1500);
            if (!_$loginWrapperModal.hasClass('not-visible')) {
                _$loginWrapperModal.find('.close-modal').click();
                return;
            }
            if (!_$accountWrapperModal.hasClass('not-visible')) {
                _$accountWrapperModal.find('.close-modal').click();
                return;
            }
            if (!_$leaderboardWrapperModal.hasClass('not-visible')) {
                _$leaderboardWrapperModal.find('.close-modal').click();
                return;
            }
            if (!_$mainMenu.hasClass('moveToLeft') && !_$mainMenu.hasClass('moveToRight')) {
                (<any>navigator).app.exitApp();
                return;
            }
            if (_$moreInfo.hasClass('slideToRight')) {
                _$backInfoButton.click();
                return;
            }
            if (_$chooseMode.hasClass('slideToLeft') && !_$chooseMode.hasClass('slideAway')) {
                _$backMenuButton.click();
                return;
            }
            if (_$chooseMode.hasClass('slideAway') && !_$gameOver.hasClass('blocked')) {
                TheColourGame.colourGame.state.getCurrentState().quitGame();
                return;
            }
            if (_$gameOver.hasClass('blocked')) {
                _$gameOverBackButton.click();
                return;
            }

        }

    }

    function onDeviceReady() {

    // window.onload= ()=>{
        document.addEventListener("backbutton", onBackButtonPressed, false);

        accountHandler = new Fractal.accountHandler();

        heyzap = new Fractal.HeyzapController("523bd65c5584786ae70204da410980be");

        storeCG = new Fractal.Store();
        for (let i = 0; i < Fractal.PRODUCTS.length; i++) {
            //On store ready (after load)
            storeCG.addProduct(Fractal.PRODUCTS[i].id, Fractal.PRODUCTS[i].alias, (<any>window).store.NON_CONSUMABLE);
        }

        storeCG.load();
        (<any>navigator).splashScreen.hide();


        TheColourGame.splashScreen(function () {
            //Splash screen ended
            TheColourGame.startLogo(function () {

                setTimeout(function () {
                    var readyCheck:number = setInterval(() => {
                        if (document.readyState === 'complete') {
                            TheColourGame.startMainMenu(function () {
                                if (!storeCG.products[0].checkBought()) {
                                    heyzap.showInterstitialAd();
                                }
                                accountHandler.user.showLoginOverlay();
                                accountHandler.user.showLeaderboardOverlay();
                            });
                            clearInterval(readyCheck);
                        }
                    }, 100);
                }, 1000)
            });
        });

    }
}