///<reference path="../../Util/helpers.ts"/>
///<reference path="../../Util/constants.ts"/>

module TheColourGame {
    export function setLoginTransformations($item, transform) {
        let cubeWidth;
        cubeWidth = $item.find('section').width() / 2;
        //IE + Safari compatibility
        if (Fractal.Helpers.isInt(Fractal.Helpers.detectIE()) && Fractal.Helpers.detectIE() < 12) {
            switch (transform) {
                case 'initial':
                    $($item).find('.log-in').css({

                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.sign-up').css({

                        WebkitTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({

                        WebkitTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({

                        WebkitTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    break;
                case 'left':
                    $($item).find('.log-in').css({


                        WebkitTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        transform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)'
                    });
                    $($item).find('.sign-up').css({


                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({

                        WebkitTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({

                        WebkitTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)'
                    });

                    break;
                case 'right':
                    $($item).find('.log-in').css({


                        WebkitTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        transform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)'
                    });
                    $($item).find('.sign-up').css({

                        WebkitTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({


                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({

                        WebkitTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)'
                    });

                    break;
                case 'back':
                    $($item).find('.log-in').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        transform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)'
                    });
                    $($item).find('.sign-up').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });

                    break;

                default:
                    //console.log('Forgotten transform keyword');
                    return 0;
            }
            $($item).find('.half-cube-sides').css({
                WebkitTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                MozTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                OTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                msTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                transform: 'translateZ(-' + (cubeWidth) + 'px)'
            });
        }
        //Firefox,Chrome, Opera compatibility
        else {
            switch (transform) {
                case 'initial':
                    $($item).find('.log-in').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.sign-up').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    break;
                case 'left':
                    $($item).find('.log-in').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)',
                        transform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + cubeWidth + 'px)'
                    });
                    $($item).find('.sign-up').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(90deg) translateZ(' + (cubeWidth) + 'px)'
                    });

                    break;
                case 'right':
                    $($item).find('.log-in').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)',
                        transform: 'rotateX(0deg) rotateY(90deg) translateZ(' + cubeWidth + 'px)'
                    });
                    $($item).find('.sign-up').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(180deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(270deg) translateZ(' + (cubeWidth) + 'px)'
                    });

                    break;
                case 'back':
                    $($item).find('.log-in').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)',
                        transform: 'rotateX(0deg) rotateY(-180deg) translateZ(' + cubeWidth + 'px)'
                    });
                    $($item).find('.sign-up').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-90deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.forgotten-password').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(-270deg) translateZ(' + (cubeWidth) + 'px)'
                    });
                    $($item).find('.missing-email').css({
                        WebkitTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        MozTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        msTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        OTransform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)',
                        transform: 'rotateX(0deg) rotateY(0deg) translateZ(' + (cubeWidth) + 'px)'
                    });

                    break;

                default:
                    //console.log('Forgotten transform keyword');
                    return 0;
            }
            $($item).find('.half-cube-sides').css({
                WebkitTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                MozTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                OTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                msTransform: 'translateZ(-' + (cubeWidth) + 'px)',
                transform: 'translateZ(-' + (cubeWidth) + 'px)'
            });


        }
    }


    export function sinusoidalRotation($cube, transform, x_times, y_times) {
        let cubeWidth;
        cubeWidth = $cube.width() / 2;
        switch (transform) {
            case "first":
                $($cube).find(".left").css({
                    WebkitTransform: "rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(-36deg) rotateY(-46deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".front").css({
                    WebkitTransform: "rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(-36deg) rotateY(44deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".right").css({
                    WebkitTransform: "rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(-36deg) rotateY(134deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".top").css({
                    WebkitTransform: "rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(" + (cubeWidth ) + "px)",
                    msTransform: "rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(54deg) rotateY(0deg) rotateZ(46deg) translateZ(" + (cubeWidth ) + "px)"
                });
                $($cube).find(".bottom").css({
                    WebkitTransform: "rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(" + (cubeWidth ) + "px)",
                    msTransform: "rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(234deg) rotateY(0deg)  rotateZ(44deg)  translateZ(" + (cubeWidth ) + "px)"
                });
                $($cube).find(".back").css({
                    WebkitTransform: "rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(-36deg) rotateY(224deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });

                break;
            case "second":
                $($cube).find(".left").css({
                    WebkitTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (-46 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (-46 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (-46 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (-46 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (-46 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".front").css({
                    WebkitTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (44 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (44 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (44 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (44 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (44 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".right").css({
                    WebkitTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (134 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (134 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (134 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (134 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (134 + (360 * y_times)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                $($cube).find(".top").css({
                    WebkitTransform: "rotateX(" + (54 + (360 * x_times)) + "deg) rotateY(0deg) rotateZ(" + (46 - (y_times * 360)) + "deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(" + (54 + (360 * x_times)) + "deg) rotateY(0deg) rotateZ(" + (46 - (y_times * 360)) + "deg) translateZ(" + (cubeWidth ) + "px)",
                    msTransform: "rotateX(" + (54 + (360 * x_times)) + "deg) rotateY(0deg) rotateZ(" + (46 - (y_times * 360)) + "deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(" + (54 + (360 * x_times)) + "deg) rotateY(0deg) rotateZ(" + (46 - (y_times * 360)) + "deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(" + (54 + (360 * x_times)) + "deg) rotateY(0deg) rotateZ(" + (46 - (y_times * 360)) + "deg) translateZ(" + (cubeWidth ) + "px)"
                });
                $($cube).find(".bottom").css({
                    WebkitTransform: "rotateX(" + (234 + (360 * x_times)) + "deg) rotateY(0deg)  rotateZ(" + (44 + (y_times * 360)) + "deg)  translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(" + (234 + (360 * x_times)) + "deg) rotateY(0deg)  rotateZ(" + (44 + (y_times * 360)) + "deg)  translateZ(" + (cubeWidth ) + "px)",
                    msTransform: "rotateX(" + (234 + (360 * x_times)) + "deg) rotateY(0deg)  rotateZ(" + (44 + (y_times * 360)) + "deg)  translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(" + (234 + (360 * x_times)) + "deg) rotateY(0deg)  rotateZ(" + (44 + (y_times * 360)) + "deg)  translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(" + (234 + (360 * x_times)) + "deg) rotateY(0deg)  rotateZ(" + (44 + (y_times * 360)) + "deg)  translateZ(" + (cubeWidth ) + "px)"
                });
                $($cube).find(".back").css({
                    WebkitTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (224 + (y_times * 360)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    MozTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (224 + (y_times * 360)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    OTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (224 + (y_times * 360)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    msTransform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (224 + (y_times * 360)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)",
                    transform: "rotateX(" + (-36 + (360 * x_times)) + "deg) rotateY(" + (224 + (y_times * 360)) + "deg) rotateZ(0deg) translateZ(" + (cubeWidth) + "px)"
                });
                break;
            default:
                console.log('Forgotten transform keyword');
                return 0;
        }
    }

    export function initializeCube() {
        $('.splashscreen-cube-wrapper').addClass('animate');
    }

    export function splashScreen(callback) {
        let fractalGamesSplashscreen = $(".fractal-games-splashscreen");
        //   Cube initialize
        initializeCube();
        //Wait 2400 ms before transitioning the cube
        setTimeout(function () {
            fractalGamesSplashscreen.addClass("fractal-games-splashscreen-end");
            setTimeout(function () {
                fractalGamesSplashscreen.addClass("hidden");
                callback();
            }, Fractal.fractalEndTransitionTime + Fractal.waitTime);
        }, Fractal.waitTime * 2 + Fractal.cubeTransitionTime);
    }

    let logoScreen = $('.logoScreen');

    export function startLogo(callback) {
        logoScreen.addClass("blocked");
        callback();
    }

}