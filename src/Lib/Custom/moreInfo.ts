///<reference path="../Typings/jquery/jquery.d.ts"/>
module TheColourGame {
    export let moreInfo = $('.moreInfo');
    export let moreInfoButton = $('.menuMoreInfoButton');
    export let mainMenuScreen = $('.mainMenu');
    export let backButton = $('.backInfoButton');
    export let highSurvival = $('.survivalHighScore');
    export let survivalPlay = $('.survival').find('.button-highscore');
    export let highTarget = $('.targetHighScore');
    export let targetPlay = $('.target').find('.button-highscore');
    export let highCountdown = $('.countdownHighScore');
    export let countdownPlay = $('.countdown').find('.button-highscore');
    export let highShootdown = $('.shootdownHighScore');
    export let shootdownPlay = $('.shootdown').find('.button-highscore');
    export let $loginOverlay = $('.login-overlay');
    export let $leaderboardOverlay = $('.leaderboard-overlay');
    export let $clickPreventor = $('.clickPreventor');

    export function hideOverlays() {
        $loginOverlay.addClass('fade-in-overlay');
        $leaderboardOverlay.addClass('fade-in-overlay');
        setTimeout(()=> {
            $loginOverlay.addClass('hidden');
            $leaderboardOverlay.addClass('hidden');
        }, 700);
    }

    export function showOverlays() {
        $loginOverlay.removeClass('hidden');
        $leaderboardOverlay.removeClass('hidden');
        setTimeout(()=>{
            $loginOverlay.removeClass('fade-in-overlay');
            $leaderboardOverlay.removeClass('fade-in-overlay');
        },150);
    }

    moreInfoButton.on("click", function () {
        TheColourGame.hideOverlays();
        setHighScoresInfo();
        $clickPreventor.addClass("preventOn");
        moreInfo.addClass("blocked");
        setTimeout(function () {
            moreInfo.addClass("slideToRight");
            mainMenuScreen.addClass("moveToRight");
            $clickPreventor.removeClass("preventOn");
        }, 200);
    });

    backButton.on("click", function () {
        TheColourGame.showOverlays();
        $clickPreventor.addClass("preventOn");
        moreInfo.removeClass("slideToRight");
        mainMenuScreen.removeClass("moveToRight");
        setTimeout(function () {
            moreInfo.removeClass("blocked");
            $clickPreventor.removeClass("preventOn");
        }, 1000);
    });

    export function setHighScoresInfo() {
        if (localStorage.getItem("highScoreSurvival") !== null) {
            highSurvival.html(localStorage.getItem("highScoreSurvival"));
        }
        else {
            highSurvival.html("0");
        }

        if (localStorage.getItem("highScoreTarget") !== null) {
            highTarget.html(localStorage.getItem("highScoreTarget"));
        }
        else {
            highTarget.html("0");
        }

        if (localStorage.getItem("highScoreCountdown") !== null) {
            highCountdown.html(localStorage.getItem("highScoreCountdown"));
        }
        else {
            highCountdown.html("0");
        }

        if (localStorage.getItem("highScoreShootdown") !== null) {
            highShootdown.html(localStorage.getItem("highScoreShootdown"));
        }
        else {
            highShootdown.html("0");
        }
    }

    export function setHighScoresPlay() {
        if (localStorage.getItem("highScoreSurvival") !== null) {
            survivalPlay.html(localStorage.getItem("highScoreSurvival"));
        }
        else {
            survivalPlay.html("0");
        }

        if (localStorage.getItem("highScoreTarget") !== null) {
            targetPlay.html(localStorage.getItem("highScoreTarget"));
        }
        else {
            targetPlay.html("0");
        }

        if (localStorage.getItem("highScoreCountdown") !== null) {
            countdownPlay.html(localStorage.getItem("highScoreCountdown"));
        }
        else {
            countdownPlay.html("0");
        }

        if (localStorage.getItem("highScoreShootdown") !== null) {
            shootdownPlay.html(localStorage.getItem("highScoreShootdown"));
        }
        else {
            shootdownPlay.html("0");
        }
    }

}