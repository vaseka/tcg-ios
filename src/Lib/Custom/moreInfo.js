///<reference path="../Typings/jquery/jquery.d.ts"/>
var TheColourGame;
(function (TheColourGame) {
    TheColourGame.moreInfo = $('.moreInfo');
    TheColourGame.moreInfoButton = $('.menuMoreInfoButton');
    TheColourGame.mainMenuScreen = $('.mainMenu');
    TheColourGame.backButton = $('.backInfoButton');
    TheColourGame.highSurvival = $('.survivalHighScore');
    TheColourGame.survivalPlay = $('.survival').find('.button-highscore');
    TheColourGame.highTarget = $('.targetHighScore');
    TheColourGame.targetPlay = $('.target').find('.button-highscore');
    TheColourGame.highCountdown = $('.countdownHighScore');
    TheColourGame.countdownPlay = $('.countdown').find('.button-highscore');
    TheColourGame.highShootdown = $('.shootdownHighScore');
    TheColourGame.shootdownPlay = $('.shootdown').find('.button-highscore');
    TheColourGame.$loginOverlay = $('.login-overlay');
    TheColourGame.$leaderboardOverlay = $('.leaderboard-overlay');
    TheColourGame.$clickPreventor = $('.clickPreventor');
    function hideOverlays() {
        TheColourGame.$loginOverlay.addClass('fade-in-overlay');
        TheColourGame.$leaderboardOverlay.addClass('fade-in-overlay');
        setTimeout(function () {
            TheColourGame.$loginOverlay.addClass('hidden');
            TheColourGame.$leaderboardOverlay.addClass('hidden');
        }, 700);
    }
    TheColourGame.hideOverlays = hideOverlays;
    function showOverlays() {
        TheColourGame.$loginOverlay.removeClass('hidden');
        TheColourGame.$leaderboardOverlay.removeClass('hidden');
        setTimeout(function () {
            TheColourGame.$loginOverlay.removeClass('fade-in-overlay');
            TheColourGame.$leaderboardOverlay.removeClass('fade-in-overlay');
        }, 150);
    }
    TheColourGame.showOverlays = showOverlays;
    TheColourGame.moreInfoButton.on("click", function () {
        TheColourGame.hideOverlays();
        setHighScoresInfo();
        TheColourGame.$clickPreventor.addClass("preventOn");
        TheColourGame.moreInfo.addClass("blocked");
        setTimeout(function () {
            TheColourGame.moreInfo.addClass("slideToRight");
            TheColourGame.mainMenuScreen.addClass("moveToRight");
            TheColourGame.$clickPreventor.removeClass("preventOn");
        }, 200);
    });
    TheColourGame.backButton.on("click", function () {
        TheColourGame.showOverlays();
        TheColourGame.$clickPreventor.addClass("preventOn");
        TheColourGame.moreInfo.removeClass("slideToRight");
        TheColourGame.mainMenuScreen.removeClass("moveToRight");
        setTimeout(function () {
            TheColourGame.moreInfo.removeClass("blocked");
            TheColourGame.$clickPreventor.removeClass("preventOn");
        }, 1000);
    });
    function setHighScoresInfo() {
        if (localStorage.getItem("highScoreSurvival") !== null) {
            TheColourGame.highSurvival.html(localStorage.getItem("highScoreSurvival"));
        }
        else {
            TheColourGame.highSurvival.html("0");
        }
        if (localStorage.getItem("highScoreTarget") !== null) {
            TheColourGame.highTarget.html(localStorage.getItem("highScoreTarget"));
        }
        else {
            TheColourGame.highTarget.html("0");
        }
        if (localStorage.getItem("highScoreCountdown") !== null) {
            TheColourGame.highCountdown.html(localStorage.getItem("highScoreCountdown"));
        }
        else {
            TheColourGame.highCountdown.html("0");
        }
        if (localStorage.getItem("highScoreShootdown") !== null) {
            TheColourGame.highShootdown.html(localStorage.getItem("highScoreShootdown"));
        }
        else {
            TheColourGame.highShootdown.html("0");
        }
    }
    TheColourGame.setHighScoresInfo = setHighScoresInfo;
    function setHighScoresPlay() {
        if (localStorage.getItem("highScoreSurvival") !== null) {
            TheColourGame.survivalPlay.html(localStorage.getItem("highScoreSurvival"));
        }
        else {
            TheColourGame.survivalPlay.html("0");
        }
        if (localStorage.getItem("highScoreTarget") !== null) {
            TheColourGame.targetPlay.html(localStorage.getItem("highScoreTarget"));
        }
        else {
            TheColourGame.targetPlay.html("0");
        }
        if (localStorage.getItem("highScoreCountdown") !== null) {
            TheColourGame.countdownPlay.html(localStorage.getItem("highScoreCountdown"));
        }
        else {
            TheColourGame.countdownPlay.html("0");
        }
        if (localStorage.getItem("highScoreShootdown") !== null) {
            TheColourGame.shootdownPlay.html(localStorage.getItem("highScoreShootdown"));
        }
        else {
            TheColourGame.shootdownPlay.html("0");
        }
    }
    TheColourGame.setHighScoresPlay = setHighScoresPlay;
})(TheColourGame || (TheColourGame = {}));
