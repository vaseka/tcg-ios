///<reference path="../../ColourGame.ts"/>
///<reference path="mainReady.ts"/>
var TheColourGame;
(function (TheColourGame) {
    TheColourGame.playButtons = $('.mode-play');
    TheColourGame.chooseShootdown = $('.mode-play-shootdown');
    TheColourGame.chooseTarget = $('.mode-play-target');
    TheColourGame.chooseCountdown = $('.mode-play-countdown');
    TheColourGame.chooseSurvival = $('.mode-play-survival');
    TheColourGame.lockButton = $('.mode-locked');
    TheColourGame.buyExtra = $('.buy-extra-mods-wrapper');
    var width;
    var height;
    if (height > 1280) {
        height = 1280;
        width = 1280 / (height / width);
    }
    TheColourGame._$mainMenuChose = $('.mainMenu');
    TheColourGame._$animatedButton = $('.button-animated');
    TheColourGame._$clickPreventor = $(".clickPreventor");
    TheColourGame._$chooseMode = $('.chooseMode');
    $('.backMenuButton').on("click", function () {
        TheColourGame._$clickPreventor.addClass("preventOn");
        TheColourGame._$mainMenuChose.addClass("blocked");
        setTimeout(function () {
            TheColourGame._$mainMenuChose.removeClass("moveToLeft");
            TheColourGame._$chooseMode.removeClass("slideToLeft");
            setTimeout(function () {
                TheColourGame._$animatedButton.removeClass("slideDown");
                TheColourGame._$clickPreventor.removeClass("preventOn");
            }, 900);
        }, 0);
    });
    TheColourGame.playButtons.on("click", function (e) {
        var modeName = $.grep(this.className.split(" "), function (v, i) {
            return v.indexOf('mode-play-') === 0;
        }).join();
        modeName = modeName.replace('mode-play-', '');
        if (!TheColourGame.storeCG.products[0].checkBought() && modeName === 'shootdown' ||
            !TheColourGame.storeCG.products[0].checkBought() && modeName === 'target') {
            TheColourGame.buyExtra.addClass('show-buy');
        }
        else {
            Fractal.Helpers.rotateUntilCallback(function () {
                TheColourGame.hideOverlays();
                width = window.innerWidth * ((devicePixelRatio / 2) + 0.5);
                height = window.innerHeight * ((devicePixelRatio / 2) + 0.5);
                if (height > 1280) {
                    height = 1280;
                    width = 1280 / (window.innerHeight / window.innerWidth);
                }
                TheColourGame.colourGame = new TheColourGame.ColourGame(width, height, modeName);
            });
        }
    });
    TheColourGame.lockButton.on("click", function () {
        TheColourGame.buyExtra.addClass('show-buy');
    });
    TheColourGame.buyExtra.find('.buy-extra-mods-button-buy').on('click', function () {
        if (navigator.connection.type !== 'none') {
            if (!TheColourGame.storeReady) {
                navigator.notification.alert('In-app purchases are not ready yet. Please try again after 5 seconds.', null, 'Buy Extra Modes and Remove Ads.', 'OK');
                TheColourGame.storeCG.load();
            }
            else {
                TheColourGame.storeCG.getProduct('com.fractalgames.thecolourgame.noads.extramodes').buy();
            }
        }
        else {
            navigator.notification.alert('Please check your internet connection and try again.', null, 'Buy Extra Modes and Remove Ads.', 'OK');
        }
    });
    TheColourGame.buyExtra.find('.buy-extra-mods-button-close').on('click', function () {
        TheColourGame.buyExtra.removeClass('show-buy');
    });
})(TheColourGame || (TheColourGame = {}));
