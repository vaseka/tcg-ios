///<reference path="../Typings/jquery/jquery.d.ts"/>
///<reference path="menuScreen.ts"/>
///<reference path="../../Util/accountHandler.ts"/>
///<reference path="../../Util/Store/Store.ts"/>
///<reference path="../../Util/HeyZap.ts"/>
///<reference path="splashScripts.ts"/>
///<reference path="chooseMode.ts"/>
var TheColourGame;
(function (TheColourGame) {
    // import PRODUCTS = Fractal.PRODUCTS;
    document.addEventListener("deviceready", onDeviceReady, false);
    TheColourGame.storeCG = null;
    TheColourGame.storeReady = false;
    TheColourGame.accountHandler = null;
    TheColourGame.adCounter = 0;
    TheColourGame.randomAdShow = Math.floor(Math.random() * 2) + 2;
    var _$mainMenu = $('.mainMenu');
    var _$moreInfo = $('.moreInfo');
    var _$backInfoButton = $('.backInfoButton');
    var _$chooseMode = $('.chooseMode');
    var _$backMenuButton = $('.backMenuButton');
    var _$gameOver = $('.gameOver');
    var _$loginWrapperModal = $('.login-wrapper-modal');
    var _$accountWrapperModal = $('.account-wrapper-modal');
    var _$leaderboardWrapperModal = $('.leaderboard-wrapper-modal');
    var _backButtonFloodPrevention = true;
    var _$gameOverBackButton = $('.gameOverButtons').find('.back');
    function onBackButtonPressed() {
        if (_backButtonFloodPrevention) {
            _backButtonFloodPrevention = false;
            setTimeout(function () {
                _backButtonFloodPrevention = true;
            }, 1500);
            if (!_$loginWrapperModal.hasClass('not-visible')) {
                _$loginWrapperModal.find('.close-modal').click();
                return;
            }
            if (!_$accountWrapperModal.hasClass('not-visible')) {
                _$accountWrapperModal.find('.close-modal').click();
                return;
            }
            if (!_$leaderboardWrapperModal.hasClass('not-visible')) {
                _$leaderboardWrapperModal.find('.close-modal').click();
                return;
            }
            if (!_$mainMenu.hasClass('moveToLeft') && !_$mainMenu.hasClass('moveToRight')) {
                navigator.app.exitApp();
                return;
            }
            if (_$moreInfo.hasClass('slideToRight')) {
                _$backInfoButton.click();
                return;
            }
            if (_$chooseMode.hasClass('slideToLeft') && !_$chooseMode.hasClass('slideAway')) {
                _$backMenuButton.click();
                return;
            }
            if (_$chooseMode.hasClass('slideAway') && !_$gameOver.hasClass('blocked')) {
                TheColourGame.colourGame.state.getCurrentState().quitGame();
                return;
            }
            if (_$gameOver.hasClass('blocked')) {
                _$gameOverBackButton.click();
                return;
            }
        }
    }
    function onDeviceReady() {
        // window.onload= ()=>{
        document.addEventListener("backbutton", onBackButtonPressed, false);
        TheColourGame.accountHandler = new Fractal.accountHandler();
        TheColourGame.heyzap = new Fractal.HeyzapController("523bd65c5584786ae70204da410980be");
        TheColourGame.storeCG = new Fractal.Store();
        for (var i = 0; i < Fractal.PRODUCTS.length; i++) {
            //On store ready (after load)
            TheColourGame.storeCG.addProduct(Fractal.PRODUCTS[i].id, Fractal.PRODUCTS[i].alias, window.store.NON_CONSUMABLE);
        }
        TheColourGame.storeCG.load();
        navigator.splashScreen.hide();
        TheColourGame.splashScreen(function () {
            //Splash screen ended
            TheColourGame.startLogo(function () {
                setTimeout(function () {
                    var readyCheck = setInterval(function () {
                        if (document.readyState === 'complete') {
                            TheColourGame.startMainMenu(function () {
                                if (!TheColourGame.storeCG.products[0].checkBought()) {
                                    TheColourGame.heyzap.showInterstitialAd();
                                }
                                TheColourGame.accountHandler.user.showLoginOverlay();
                                TheColourGame.accountHandler.user.showLeaderboardOverlay();
                            });
                            clearInterval(readyCheck);
                        }
                    }, 100);
                }, 1000);
            });
        });
    }
})(TheColourGame || (TheColourGame = {}));
