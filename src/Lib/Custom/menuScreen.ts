///<reference path="../Typings/jquery/jquery.d.ts"/>
///<reference path="../../Util/constants.ts"/>
///<reference path="moreInfo.ts"/>
///<reference path="mainReady.ts"/>
module TheColourGame {
    // import logoTransitionTime = Fractal.logoTransitionTime;
    export let mainMenu = $('.mainMenu');
    export let buttonOne = $('.survival');
    export let buttonTwo = $('.countdown');
    export let buttonThree = $('.shootdown');
    export let buttonFour = $('.target');
    export let logoScreen = $('.logoScreen');
    export let mql = window.matchMedia("screen and (orientation: landscape)");

    export function startMainMenu(callback) {
        setTimeout(function () {
            mainMenu.addClass("blocked");
            logoScreen.removeClass("blocked");
            callback();
        }, Fractal.logoTransitionTime)
    }

    export let playButton = $('.menuPlayButton');
    export let chooseMode = $('.chooseMode');

    playButton.on("click", function () {
        if (TheColourGame.storeCG.products[0].checkBought() && (<any>navigator).connection.type === 'none') {
            $('.mode-buttons').addClass('unlocked');
        }
        setHighScoresPlay();
        $clickPreventor.addClass("preventOn");
        mainMenu.addClass("moveToLeft");
        setTimeout(function () {
            mainMenu.removeClass("blocked");
            $clickPreventor.removeClass("preventOn");
        }, 1200);


        //chooseMode.addClass("blocked");
        chooseMode.addClass("slideToLeft");

        if (mql.matches === true) {
            setTimeout(function () {
                buttonFour.addClass("slideDown");
                buttonThree.addClass("slideDown");
                buttonTwo.addClass("slideDown");
                buttonOne.addClass("slideDown");
            }, 800);
        }
        else {
            setTimeout(function () {
                buttonFour.addClass("slideDown");
                buttonThree.addClass("slideDown");
                buttonTwo.addClass("slideDown");
                buttonOne.addClass("slideDown");
            }, 800);
        }
        //console.log(mql)
    });
}