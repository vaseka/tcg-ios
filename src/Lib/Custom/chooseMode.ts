///<reference path="../../ColourGame.ts"/>
///<reference path="mainReady.ts"/>

module TheColourGame {
    export let playButtons = $('.mode-play');
    export let chooseShootdown = $('.mode-play-shootdown');
    export let chooseTarget = $('.mode-play-target');
    export let chooseCountdown = $('.mode-play-countdown');
    export let chooseSurvival = $('.mode-play-survival');
    export let lockButton = $('.mode-locked');
    export let buyExtra = $('.buy-extra-mods-wrapper');
    export let colourGame;

    let width;
    let height;

    if (height > 1280) {
        height = 1280;
        width = 1280 / (height / width);
    }

    export let _$mainMenuChose = $('.mainMenu');
    export let _$animatedButton = $('.button-animated');
    export let _$clickPreventor = $(".clickPreventor");
    export let _$chooseMode = $('.chooseMode');


    $('.backMenuButton').on("click", () => {
        _$clickPreventor.addClass("preventOn");
        _$mainMenuChose.addClass("blocked");
        setTimeout(function () {
            _$mainMenuChose.removeClass("moveToLeft");
            _$chooseMode.removeClass("slideToLeft");
            setTimeout(function () {
                _$animatedButton.removeClass("slideDown");
                _$clickPreventor.removeClass("preventOn");
            }, 900);
        }, 0);
    });

    playButtons.on("click", function (e) {
        let modeName = $.grep(this.className.split(" "), function (v, i) {
            return (<String>v).indexOf('mode-play-') === 0;
        }).join();
        modeName = modeName.replace('mode-play-', '');
        if (!TheColourGame.storeCG.products[0].checkBought() && modeName === 'shootdown' ||
            !TheColourGame.storeCG.products[0].checkBought() && modeName === 'target') {
            buyExtra.addClass('show-buy');
        }
        else {
            Fractal.Helpers.rotateUntilCallback(()=> {TheColourGame.hideOverlays();
                width = window.innerWidth * ((devicePixelRatio / 2) + 0.5);
                height = window.innerHeight * ((devicePixelRatio / 2) + 0.5);
                if (height > 1280) {
                    height = 1280;
                    width = 1280 / (window.innerHeight / window.innerWidth);
                }
                colourGame = new TheColourGame.ColourGame(width, height, modeName);
            });
        }

    });


    lockButton.on("click", function () {
        buyExtra.addClass('show-buy');
    });

    buyExtra.find('.buy-extra-mods-button-buy').on('click', ()=> {
        if ((<any>navigator).connection.type !== 'none') {
            if (!TheColourGame.storeReady) {
                (<any>navigator).notification.alert('In-app purchases are not ready yet. Please try again after 5 seconds.', null, 'Buy Extra Modes and Remove Ads.', 'OK');
                TheColourGame.storeCG.load();
            }
            else {
                TheColourGame.storeCG.getProduct('com.fractalgames.thecolourgame.noads.extramodes').buy();
            }
        }
        else {
            (<any>navigator).notification.alert('Please check your internet connection and try again.', null, 'Buy Extra Modes and Remove Ads.', 'OK');
        }
    });

    buyExtra.find('.buy-extra-mods-button-close').on('click', ()=> {
        buyExtra.removeClass('show-buy');
    });
}