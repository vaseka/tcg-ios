///<reference path="../Typings/jquery/jquery.d.ts"/>
///<reference path="../../Util/constants.ts"/>
///<reference path="moreInfo.ts"/>
///<reference path="mainReady.ts"/>
var TheColourGame;
(function (TheColourGame) {
    // import logoTransitionTime = Fractal.logoTransitionTime;
    TheColourGame.mainMenu = $('.mainMenu');
    TheColourGame.buttonOne = $('.survival');
    TheColourGame.buttonTwo = $('.countdown');
    TheColourGame.buttonThree = $('.shootdown');
    TheColourGame.buttonFour = $('.target');
    TheColourGame.logoScreen = $('.logoScreen');
    TheColourGame.mql = window.matchMedia("screen and (orientation: landscape)");
    function startMainMenu(callback) {
        setTimeout(function () {
            TheColourGame.mainMenu.addClass("blocked");
            TheColourGame.logoScreen.removeClass("blocked");
            callback();
        }, Fractal.logoTransitionTime);
    }
    TheColourGame.startMainMenu = startMainMenu;
    TheColourGame.playButton = $('.menuPlayButton');
    TheColourGame.chooseMode = $('.chooseMode');
    TheColourGame.playButton.on("click", function () {
        if (TheColourGame.storeCG.products[0].checkBought() && navigator.connection.type === 'none') {
            $('.mode-buttons').addClass('unlocked');
        }
        TheColourGame.setHighScoresPlay();
        TheColourGame.$clickPreventor.addClass("preventOn");
        TheColourGame.mainMenu.addClass("moveToLeft");
        setTimeout(function () {
            TheColourGame.mainMenu.removeClass("blocked");
            TheColourGame.$clickPreventor.removeClass("preventOn");
        }, 1200);
        //chooseMode.addClass("blocked");
        TheColourGame.chooseMode.addClass("slideToLeft");
        if (TheColourGame.mql.matches === true) {
            setTimeout(function () {
                TheColourGame.buttonFour.addClass("slideDown");
                TheColourGame.buttonThree.addClass("slideDown");
                TheColourGame.buttonTwo.addClass("slideDown");
                TheColourGame.buttonOne.addClass("slideDown");
            }, 800);
        }
        else {
            setTimeout(function () {
                TheColourGame.buttonFour.addClass("slideDown");
                TheColourGame.buttonThree.addClass("slideDown");
                TheColourGame.buttonTwo.addClass("slideDown");
                TheColourGame.buttonOne.addClass("slideDown");
            }, 800);
        }
        //console.log(mql)
    });
})(TheColourGame || (TheColourGame = {}));
