/**
 * Created by Ivaylo Hristov on 1.6.2016 г..
 */
cordova.define("com.fractalgames.utils.RequestPermissions", function(require, exports, module) {
    var exec = require('cordova/exec');
    var requestPermissions = {
        requestCamera: function (successCallback, errorCallback, action) {
            exec(successCallback, errorCallback, "RequestPermissions", "cameraPermissions", [action]);
        },

    };

    module.exports = requestPermissions;
});