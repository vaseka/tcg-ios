cordova.define('cordova/plugin_list', function (require, exports, module) {
    module.exports = [
        {
            "file": "plugins/store.js",
            "id": "cc.fovea.cordova.purchase",
            "clobbers": [
                "window.store"
            ]
        },
        {
            "file": "plugins/facebookConnectPlugin.js",
            "id": "com.phonegap.plugins.facebookconnect",
            "clobbers": [
                "window.facebookConnectPlugin"
            ]
        },
        {
            "file": "plugins/googlePlusPlugin.js",
            "id": "cordova-plugin-googleplus.GooglePlus",
            "clobbers": [
                "window.plugins.googleplus"
            ]
        },
        {
            "file": "plugins/SocialSharing.js",
            "id": "nl.x-services.plugins.socialsharing.SocialSharing",
            "clobbers": [
                "window.plugins.socialsharing"
            ]
        },
        {
            "file": "plugins/splashScreen.js",
            "id": "org.apache.cordova.splashscreen.SplashScreen",
            "clobbers": [
                "navigator.splashScreen"
            ]
        },
        {
            "file": "plugins/notification.js",
            "id": "org.apache.cordova.dialogs.Notification",
            "clobbers": [
                "navigator.notification"
            ]
        },
        {
            "file": "plugins/heyzap/ads/HeyzapAds.js",
            "id": "heyzap-cordova.ads.HeyzapAds",
            "clobbers": [
                "window.HeyzapAds"
            ]
        },
        {
            "file": "plugins/heyzap/ads/InterstitialAd.js",
            "id": "heyzap-cordova.ads.InterstitialAd"
        },
        {
            "file": "plugins/heyzap/ads/VideoAd.js",
            "id": "heyzap-cordova.ads.VideoAd"
        },
        {
            "file": "plugins/heyzap/ads/IncentivizedAd.js",
            "id": "heyzap-cordova.ads.IncentivizedAd"
        },
        {
            "file": "plugins/heyzap/ads/BannerAd.js",
            "id": "heyzap-cordova.ads.BannerAd"
        },
        {
            "file": "plugins/heyzap/Common.js",
            "id": "heyzap-cordova.Common"
        },
        {
            "file": "plugins/heyzap/Promise.js",
            "id": "heyzap-cordova.Promise"
        },
        {
            "file": "plugins/heyzap/vendor/es6-promise.min.js",
            "id": "heyzap-cordova.vendor.PromisePolyfill"
        },
        {
            "file": "plugins/network.js",
            "id": "cordova-plugin-network-information.network",
            "clobbers": [
                "navigator.connection",
                "navigator.network.connection"
            ]
        },
        {
            "file": "plugins/Connection.js",
            "id": "cordova-plugin-network-information.Connection",
            "clobbers": [
                "Connection"
            ]
        },
        {
            "file": "plugins/requestPermission.js",
            "id": "com.fractalgames.utils.RequestPermissions",
            "clobbers": [
                "window.requestPermissions"
            ]
        }
    ];
    module.exports.metadata =
// TOP OF METADATA
    {
        "com.phonegap.plugins.facebookconnect": "0.5.1",
        "cordova-plugin-googleplus": "4.0.0",
        "cordova-plugin-googleplayservices": "19.0.1",
        "nl.x-services.plugins.socialsharing": "4.3.19",
        "cc.fovea.cordova.purchase": "3.10.1",
        "org.apache.cordova.splashscreen.SplashScreen": "1.0.0",
        "org.apache.cordova.dialogs.Notification": "1.0.0",
        "heyzap-cordova": "1.0.0",
        "cordova-plugin-network-information": "1.2.0",
        "com.fractalgames.utils.RequestPermissions": "0.0.1"
    };
// BOTTOM OF METADATA
});