var fs = require('fs');
var path = require('path');
var gulp = require('gulp');
var less = require('gulp-less-sourcemap');
var livereload = require('gulp-livereload');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var typescript = require('gulp-tsc');
var del = require('del');
var uglify = require('gulp-uglify');
var js_obfuscator = require('gulp-js-obfuscator');

gulp.task('cleanBuildFolder', function () {
    // Deleting everything in the build folder
    return del([
        './build/**/*'
    ])
});

gulp.task('copyAssets', function () {
    return gulp.src(['./src/Assets/**/*', './src/Css/**/*.css',
            './src/Lib/Vendor/**/*.min.js', './src/index.html',
            './src/Lib/Cordova/**/*.js'], {"base": "./src"})
        .pipe(gulp.dest('./build'));
});

gulp.task('buildTypeScript', function () {
    //All folders with typescripts files needs to be added here
    return gulp.src(['./src/ColourGame.ts', './src/Util/**/*.ts',
            './src/Game/**/*.ts', './src/Lib/Custom/**/*.ts'])
        .pipe(typescript({
            module: 'commonjs',
            target: 'ES5',
            out: 'ColourGame.js',
            outDir: './src',
            sourceMap: true,
            removeComments: true,
            emitError: false
        }))
        .pipe(gulp.dest('./src/build'))
});

// cube.js
// constants.js
// main.js
// logo.js
// menuScreen.js
// chooseMode.js
// moreInfo.js

gulp.task('combineAndCopyJS', function () {
    //All javascript we need to combine here
    return gulp.src(['./src/build/ColourGame.js'])
        .pipe(concat('game.js'))
        .pipe(uglify())
        .pipe(js_obfuscator())
        .pipe(gulp.dest('./build/'));
});

gulp.task('buildGame', ['cleanBuildFolder', 'buildTypeScript', 'combineAndCopyJS', 'copyAssets',], function () {

})

gulp.task('less', function () {
    gulp.src('./src/Less/site.less')
        .pipe(less({
            sourceMap: {
                sourceMapRootpath: './src/Less'
            }
        }))
        .pipe(gulp.dest('./src/Css'))
        .pipe(livereload());
});


gulp.task('serve', ['less'], function () {
    browserSync.init({
        server: './src'
    });
    gulp.watch('./src/Less/**/*.less', ['less']);
    gulp.watch('./src/Css/site.css').on('change', browserSync.reload);
    gulp.watch('./src/*.html').on('change', browserSync.reload);
    gulp.watch('./src/Lib/Custom/*.js').on('change', browserSync.reload);
});
